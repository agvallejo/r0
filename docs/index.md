# Project r0

This project is an attempt to resurrect the spirit of the
[Coyotos](https://web.archive.org/web/20160902045419/http://www.coyotos.org/)
project, bringing the Rust ecosystem into the equation and producing a
credible framework for building safe, dynamic systems, based on the core
principles of capability-based security, transparent persistence and
minimality.

!!! warning

    The codebase is... slim, and has little to do with the contents of this
    docs. Because the project stands on the shoulders of its predecessors
    there's a great deal to be written in both code and prose. Eventually
    both will touch together, but until then these docs will hold the
    _hopes for what the future should hold_, while the reference manuals
    for the kernel and the OS personalities will provide insights into the
    state of the actual implementations
