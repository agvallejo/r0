# Memory Map

## x86_64

The higher 2 entries of PML4 are strictly reserved for kernel use.
`PML4[510]` is a global PTE, present on every PML4 in the system, while
`PML4[511]` is CPU-local. The region with common code & data is common,
while everything else is truly CPU-local. This means the stacks on every
CPU are on the same location, but different address spaces.

The transmap region is 256KiB worth of virtual space. The first page is the
only one actually backed by memory and it's wired to be the very page table
backing it. (i.e: `PT[0] = &PT`). This means the kernel can use the
transmap region (by manipulating entries `[1:63]` of that page) in order to
transiently gain access to unmapped pages. There is *no* directmap of all
physical memory at all in a (vain) attempt of secret freedom in the kernel.

```kroki-ditaa
|                        ...                        |
+---------------------------------------------------+ -1 TiB
|          Object vectors. Globaly visible          |
+---------------------------------------------------+ -512 GiB
|cBLK                 Unmapped                      |
+---------------------------------------------------+ -2 GiB
|        Common code & data. Globally visible       |
+---------------------------------------------------+ -1 GiB
|cBLK                 Unmapped                      |
+---------------------------------------------------+ -4 MiB
|cGRE              Per CPU transmap                 |
+---------------------------------------------------+ -4 MiB + 256 KiB
|cBLK                 Unmapped                      |
+---------------------------------------------------+ -2 MiB - 8 KiB
|cGRE               Per CPU stack                   |
+---------------------------------------------------+ -2 MiB - 4 KiB
|cGRE               Per CPU area                    |
+---------------------------------------------------+ -2 MiB
|cBLK                 Unmapped                      |
+---------------------------------------------------+ 0
```
