# Capabilities

Chip Morningstar wrote a fantastic introductory article on the concept of
capabilities. If you haven't heard the term in the context of access control
mechanisms, go and read [What are
Capabilities?](http://habitatchronicles.com/2017/05/what-are-capabilities/)
(and come back later!).

## Introduction

Capabilities are the means of communication with the kernel. _Possession_ of a
capability tells the kernel its holder has (at least temporarily) the authority
to perform an action. Their use is pervasive and everything is subject to
capability discipline by design. The following figure schematically shows this
relationship with a specific example:

```kroki-ditaa
                                           +-----------------------------------------------------+
+-------------+            +------------+  |         +-------------+            +-------------+  |
|   proc0     |    +------>|    gpt0    |<-+ +------>|     gpt1    |  +-------->|    proc1    |  |
+-------------+    |       +------------+    |       +-------------+  |         +-------------+  |
|+-----------+|    |       |+----------+|    |       |+-----------+|  |         |+-----------+|  |
||   cap0    ||    |       ||   cap0   ||    |       ||    cap0   ||  |         ||    cap0   ++--+
|+-----------+|    |       |+----------+|    |       |+-----------+|  |         |+-----------+|
||   cap1    ||    |       ||   cap1   ||    |       ||    cap1   ||  |         ||    cap1   ||
|+-----------+|    |       |+----------+|    |       |+-----------+|  |         |+-----------+|
||   cap2    ||    |       ||   cap2   ||    |       ||    cap2   ++--+         ||    cap2   ||
|+-----------+|    |       |+----------+|    |       |+-----------+|            |+-----------+|
||   cap3    ||    |       ||   cap3   ||    |       ||    cap3   ||            ||    cap3   ||
|+-----------+|    |       |+----------+|    |       |+-----------+|            |+-----------+|
||   cap4    ++----+       ||   cap4   ++----+       ||    cap4   ||            ||    cap4   ||
|+-----------+|            |+----------+|            |+-----------+|            |+-----------+|
|   ...       |            |    ...     |            |     ...     |            |     ...     |
|+-----------+|            |+----------+|            |+-----------+|            |+-----------+|
||   cap13   ||            ||   cap13  ||            ||    cap13  ||            ||    cap13  ||
|+-----------+|            |+----------+|            |+-----------+|            |+-----------+|
||   cap14   ++---------+  ||   cap14  ||            ||    cap14  ||            ||    cap14  ||
|+-----------+|         |  |+----------+|            |+-----------+|            |+-----------+|
||   cap15   ||         |  ||   cap15  ++--+         ||    cap15  ++--+         ||    cap15  ||
|+-----------+|         |  |+----------+|  |         |+-----------+|  |         |+-----------+|
+-------------+         |  +------------+  |         +-------------+  |         +-------------+
                        |                  |                          |
                        |  +------------+  |         +-------------+  |
                        +->|    page0   |  +-------->|   page1     |<-+
                           +------------+            +-------------+
```
There are two processes, `proc0` and `proc1`, two GPTs (fixed-size arrays of
capabilities), `gpt0` and `gpt1`, and two pages, `page0` and `page1`.

Each process holds a fixed amount of capabilities in their own capability
registers (`cap0` through `cap15`). _Invoking_ them grants the invoking process
access to more capabilities (and critically, the objects they point to).
`proc0` can access the capabilities in `gpt0` through its capability in
`proc0[cap4]`. It also has access to `page1` through the sequence
`proc0[cap4].gpt0[cap15]`. By a similar argument, `proc1` has access to `page1`
through `proc1[cap0].gpt0.cap[15]`, but _not_ to `page0`, because that object
is unreachable from `proc1`. The only way it can acquire it is for `proc0` to
explicitly _send_ it  to `proc1` through
`proc0[cap4].gpt0[cap4].gpt1[cap2].proc1`. When a process invokes another
process, an IPC operation happens in which capabilities and data may be
exchanged.

As was mentioned before, capabilities are the _only_ means of communication and
interaction with the system. address spaces are defined by creating trees of
capabilities with page capabilties at the leaves, mirroring the page table
structures of the MMU.

## Representation

Every capability is 16 octets long, and their layout depends on the capability type.

### Memory traversals

```kroki-packetdiag
packetdiag {
colwidth = 32;
node_height = 32;

0-5: Type;
6: P
7: RO [rotate = 270];
8: NX [rotate = 270];
9: WK [rotate = 270];
10: OP [rotate = 270];
11: NC [rotate = 270];
12-31: Allocation Count
32-38: Bitsize
39-63: 0
64-127: Object Identifier
}
```

On `Page` capabilities, bitsize is `log2(page_size)`, while on GPTs it is `log2(slot_size)`

### Process interaction

```kroki-packetdiag
packetdiag {
colwidth = 32;
node_height = 32;

0-5: Type;
6: P
7-11: 0
12-31: Allocation Count
32-63: Payload
64-127: Object Identifier
}
```

The `payload` field has 2 interpretations. On `Start` capabilities it means
_protected payload_ (this is delivered to the destination process alongside the
message sent). while on `Resume` capabilities it means _call count_ and is the
mechanism by which stale `Resume` capabilities are revoked.

On `Process` capabilities, the `payload` field has no meaning and is reserved.
