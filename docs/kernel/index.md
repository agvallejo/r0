# Summary

r0 is a microkernel at heart and at the center of the its design there are
three core principles. There are, of course, other desirable properties of
well-designed systems r0 must conform to.

## Capability-driven security
After an r0 system has booted some programs will automatically start
running, and all of them will be subject to to a certain ecosystem of
interconnections with other components. We call each such typical ecosystem
an _OS personality_. And each of those interconnections a _capability_.

!!! note

    The formal definition is more complicated, but crucially the key
    takeway is that a program's ability to directly interact with another
    program is directly tied to the former holding a capability to the
    latter. Conversely, if a program has no way of acquiring such a
    capability (via requests through existing capabilities) then those 2
    programs won't be able to communicate in an overt fashion.

Capability-based security offers a well-tested bedrock of plausible
security and many decades of literature and real systems proving its
soundness.

## Transparent persistence

This principle forces an r0 system to make an impossible promise to the
programs running under it.

> _The machine you're running on will run forever and it shall never reboot_

This is of course, fiction. And it shows through the cracks in certain
pieces of the TCB (think databases or very low-level drivers). By and large
though, the vast majority of software live in ignorant bliss about such
insignificant details. They are, for all intents and purposes, programs
running on an eternally available machine that never shuts down.

In order to maintain this fiction, the kernel and a collection of trusted
components collude to keep the full state of the machine synchronized
between some form of persistent store (like a hard drive or some remote
database) and RAM. This is done at regular intervals through an efficient
mechanism that ensures the system never stops.

## Minimality

The importance of this principle has been discussed
[over (KeyKOS)](https://en.wikipedia.org/wiki/KeyKOS), and
[over (Mach)](https://en.wikipedia.org/wiki/Mach_%28kernel%29), and
[over (L4)](https://en.wikipedia.org/wiki/L4_microkernel_family).

The intent in this project is to aim for overall minimality practical
systems (OS personalities) being with it. Each component must be as small
as common sense will allow in order to reduce attack surfaces and create
a common architectural framework.

In short, this definition of minimality requires each component to avoid
reducing themselves in a way that the overall system suffers. The kernel is
included as a component here for the purposes of minimality.

To make it clearer. A functionality _may_ be kernel material if:

1. Its addition causes a significant reduction of complexity of the system as a whole.
2. Its addition improves performance by a non-negligible amount (after
   benchmark) without compromising security
