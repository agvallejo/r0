#!/usr/bin/env sh

set -eux

REGISTRY=registry.gitlab.com
TAG=`date +%Y%m%d`-`git rev-parse --short HEAD`

podman build -t "$REGISTRY/agvallejo/r0:$TAG" .
podman login "$REGISTRY"
podman push "$REGISTRY/agvallejo/r0:$TAG"
