//! tx side of the rxtx test
#![no_std]
#![no_main]

use libsys::{
    base::{call, console_write, openwait, qemu_exit},
    CapSlot,
};

/// Expected capability slot for the kernel console
const CAP_CONSOLE: CapSlot = CapSlot(1);

/// Expected capability slot for the QEMU exit device
const CAP_QEMU: CapSlot = CapSlot(2);

/// Expected capability slot for the `rx` start capability
const CAP_START_RX: CapSlot = CapSlot(3);

/// Fixed spots for incoming caps
const INCAPS_TMP: [CapSlot; 4] = [CapSlot(8), CapSlot(9), CapSlot(10), CapSlot(11)];

/// Fixed spots for outgoing caps
const OUTCAPS_TMP: [CapSlot; 4] = [CapSlot(12), CapSlot(13), CapSlot(14), CapSlot(15)];

/// Rust's entry point
#[no_mangle]
fn main() {
    console_write(CAP_CONSOLE, "[tx] call rx\n");
    let _ = call(CAP_START_RX, 0xcafecafe, &INCAPS_TMP, &OUTCAPS_TMP);

    /* Yield by going on an open wait */
    openwait(&[CapSlot(0), CapSlot(0), CapSlot(0), CapSlot(0)]);
    unreachable!();
}

/// Panic handler
#[panic_handler]
fn panic(info: &core::panic::PanicInfo) -> ! {
    console_write(CAP_CONSOLE, "Panic!");
    qemu_exit(CAP_QEMU, false);
    libsys::hal::entry::panic(info);
}
