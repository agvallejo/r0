//! rx side of the rxtx kernel test
#![no_std]
#![no_main]

use libsys::{
    base::{console_write, openwait, qemu_exit, InvType, WordCtl},
    hal::syscall::cap_invoke,
    CapSlot,
};

/// Expected capability slot for the kernel console
const CAP_CONSOLE: CapSlot = CapSlot(1);

/// Expected capability slot for the QEMU exit device
const CAP_QEMU: CapSlot = CapSlot(2);

/// Expected capability slot for the resume capability passed over IPC
const CAP_RESUME: CapSlot = CapSlot(3);

/// Rust entry point
#[no_mangle]
fn main() {
    let mut resume = WordCtl::new(InvType::Return, CAP_RESUME, 0);
    resume = *resume.with_incap(0, CAP_RESUME);

    console_write(CAP_CONSOLE, "[rx] Hello World!\n");

    openwait(&[CAP_RESUME, CapSlot(0), CapSlot(0), CapSlot(0)]);
    console_write(CAP_CONSOLE, "[rx] Received something!\n");

    unsafe { cap_invoke(&mut resume.clone(), &mut [0; 4]) };
    console_write(CAP_CONSOLE, "[rx] Received something!\n");

    qemu_exit(CAP_QEMU, true);
}

/// Panic handler
#[panic_handler]
fn panic(info: &core::panic::PanicInfo) -> ! {
    console_write(CAP_CONSOLE, "Panic!");
    qemu_exit(CAP_QEMU, false);
    libsys::hal::entry::panic(info);
}
