//! High(er) level bindings for userspace capability invocations

use num_derive::FromPrimitive;
use num_traits::FromPrimitive;

use crate::{hal::syscall::cap_invoke, CapSlot};

/// Writes a character to the kernel console
pub fn console_write(cap: CapSlot, s: &str) {
    for octet in s.as_bytes() {
        let mut wctl = WordCtl::new(InvType::Call, cap, 0);
        let mut args: [u64; 4] = [(*octet).into(), 0, 0, 0];
        unsafe { cap_invoke(&mut wctl, &mut args) }
    }
}

/// Output octets into a PIO port
pub fn outb(cap: CapSlot, port: u16, s: &str) {
    for octet in s.as_bytes() {
        let mut wctl = WordCtl::new(InvType::Call, cap, 1);
        let mut args: [u64; 4] = [port.into(), (*octet).into(), 0, 0];
        unsafe { cap_invoke(&mut wctl, &mut args) }
    }
}

/// Writes a character to the kernel console
pub fn qemu_exit(cap: CapSlot, success: bool) {
    let cmd = if success { 1 } else { 0 };
    let mut wctl = WordCtl::new(InvType::Call, cap, cmd);
    let mut args: [u64; 4] = [0, 0, 0, 0];
    unsafe { cap_invoke(&mut wctl, &mut args) }
}

/// Go on an openwait
pub fn openwait(caps_out: &[CapSlot; 4]) -> [u64; 4] {
    let mut wctl = WordCtl::new(InvType::Return, CapSlot(0), 0);
    wctl.with_outcap(0, caps_out[0])
        .with_outcap(1, caps_out[1])
        .with_outcap(2, caps_out[2])
        .with_outcap(3, caps_out[3]);
    let mut args: [u64; 4] = [0, 0, 0, 0];
    unsafe { cap_invoke(&mut wctl, &mut args) }
    args
}

/// Go on an openwait
#[inline(always)]
pub fn call(cap: CapSlot, cmd: u32, caps_in: &[CapSlot; 4], caps_out: &[CapSlot; 4]) -> [u64; 4] {
    let mut wctl = WordCtl::new(InvType::Call, cap, cmd);
    wctl.with_outcap(0, caps_out[0])
        .with_outcap(1, caps_out[1])
        .with_outcap(2, caps_out[2])
        .with_outcap(3, caps_out[3])
        .with_incap(0, caps_in[0])
        .with_incap(1, caps_in[1])
        .with_incap(2, caps_in[2])
        .with_incap(3, caps_in[3]);
    let mut args: [u64; 4] = [0, 0, 0, 0];
    unsafe { cap_invoke(&mut wctl, &mut args) }
    args
}

/// Invocation type.
///
/// Changes the syscall semantics in the kernel. See each variant for details.
#[derive(Debug, Copy, Clone, Eq, PartialEq, FromPrimitive)]
pub enum InvType {
    /// Invokes a capability, ignoring its result. This has the effect on
    /// Start/Resume caps of continuing execution and marking the target as
    /// schedulable. Effectively creating an extra execution thread.
    Fork = 1,
    /// Invokes a capability, blocking until a result is sent back.
    ///
    /// The 0-th cap sent should be `CapSlot(0)` so a Resume cap can be synthesised
    /// in its place. The invoker is left in a paused state until the invokee
    /// invokes the synthesised Resume capability (at which point THAT message
    /// becomes the outputs seen by the invoker of the initial Call cap.
    Call = 2,
    /// Invokes a capability, ignoring its results and going on an open wait.
    Return = 3,
}

/// Control word in a syscall
///
/// Passed in a well-defined register; tells the kernel the specifics of the
/// syscall.
#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct WordCtl(pub u64);

impl WordCtl {
    /// Base bit of the opcode in the control word.
    const CMD_SHIFT: usize = 0;
    /// Baseband mask of the opcode in the control word.
    const CMD_MASK: u64 = (1 << 24) - 1;

    /// Base bit of the invocation type in the control word.
    const ITYP_SHIFT: usize = 24;
    /// Baseband mask of the invocation type in the control word.
    const ITYP_MASK: u64 = 3;

    /// Base bit of the invoked capability in the control word.
    const CAP_SHIFT: usize = 28;
    /// Baseband mask of the invoked capability in the control word.
    const CAP_MASK: u64 = 0xf;

    /// Base bit of the array of input capabilities in the control word.
    const INCAP_SHIFT: usize = 32;
    /// Baseband mask of the array of input capabilities in the control word.
    const INCAP_MASK: u64 = 0xffff;

    /// Base bit of the array of output capabilities in the control word.
    const OUTCAP_SHIFT: usize = 48;
    /// Baseband mask of the array of output capabilities in the control word.
    const OUTCAP_MASK: u64 = 0xffff;

    /// Create a brand-new invocation control word.
    pub fn new(inv: InvType, cap: CapSlot, cmd: u32) -> Self {
        Self(
            ((inv as u64) << Self::ITYP_SHIFT)
                | ((cap.0 as u64) << Self::CAP_SHIFT)
                | (((cmd as u64) & Self::CMD_MASK) << Self::CMD_SHIFT),
        )
    }

    /// Retrieve the cap invocation opcode.
    pub fn cmd(&self) -> u32 {
        ((self.0 >> Self::CMD_SHIFT) & Self::CMD_MASK)
            .try_into()
            .unwrap()
    }

    /// Retrieve the invocation type. See [`InvType`].
    pub fn invtype(&self) -> Option<InvType> {
        FromPrimitive::from_u64((self.0 >> Self::ITYP_SHIFT) & Self::ITYP_MASK)
    }

    /// Retrieve the invoked capability.
    pub fn invcap(&self) -> CapSlot {
        let slot = (self.0 >> Self::CAP_SHIFT) & Self::CAP_MASK;
        CapSlot(slot.try_into().unwrap())
    }

    /// Prevents transmitting capabilities.
    ///
    /// NOTE: A resume capability is always created on the 0-th outcap of the
    /// destination of a Start/Resume cap when the invtype is [`InvType::Call`]
    pub fn without_incaps(&mut self) -> &mut Self {
        self.0 &= !Self::INCAP_MASK;
        self
    }

    /// Prevents receiving capabilities.
    ///
    /// NOTE: This _includes_ resume capabilities. Receiving like this means
    /// resume capabilities from processes calling will be dropped.
    pub fn without_outcaps(&mut self) -> &mut Self {
        self.0 &= !Self::OUTCAP_MASK;
        self
    }

    /// Retrieve the cap reg where the `idx`-th received cap should go.
    pub fn cap_in(&self, idx: usize) -> Option<CapSlot> {
        let slot = (self.0 >> (Self::INCAP_SHIFT + 4 * idx)) & Self::CAP_MASK;
        if slot == 0 {
            return None;
        }
        Some(CapSlot(slot.try_into().unwrap()))
    }

    /// Define a cap reg as the `idx`-th transmitted cap.
    pub fn with_incap(&mut self, idx: usize, cap: CapSlot) -> &mut Self {
        self.0 |= u64::from(cap.0) << (Self::INCAP_SHIFT + 4 * idx);
        self
    }

    /// Retrieve the cap reg of the `idx`-th transmitted cap.
    pub fn cap_out(&self, idx: usize) -> Option<CapSlot> {
        let slot = (self.0 >> (Self::OUTCAP_SHIFT + 4 * idx)) & Self::CAP_MASK;
        if slot == 0 {
            return None;
        }
        Some(CapSlot(slot.try_into().unwrap()))
    }

    /// Define the cap reg for the `idx`-th received cap to land on.
    pub fn with_outcap(&mut self, idx: usize, cap: CapSlot) -> &mut Self {
        self.0 |= u64::from(cap.0) << (Self::OUTCAP_SHIFT + 4 * idx);
        self
    }
}
