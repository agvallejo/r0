//! Definitions used by both the kernel and userspace
//!
//! Specifies the ABI between the kernel, mksys and system userspace
#![no_std]

#[cfg(all(feature = "kernel", feature = "usr"))]
compile_error!("libsys can't be compiled for `kernel` and `usr` at the same time");

pub mod base;
pub mod hal;
pub mod ocap;
pub mod sysimg;

/// ABI version
///
/// Every incompatible update must bump this number. It's meant to be added to every initial system
/// image and checked by the kernel on boot with the number it was itself compiled with. In order
/// to keep it simple it's just the date and two more digits for a weird several ABI breakages
/// happened at the same time.
///
/// Hopefully this will avoid embarrasing ABI-related bugs
#[allow(clippy::inconsistent_digit_grouping)]
pub const VERSION: u32 = 20231005_00;

/// `log2(GPT_SIZE)`
pub const GPT_BITS: usize = 4;

/// Number of slots in a GPT
pub const GPT_SIZE: usize = 1 << GPT_BITS;

/// Number of octets in a page
///
/// NOTE: It's possible for this number to be bigger (i.e: on some ARM configurations)
pub const PAGE_SIZE: usize = 4096;

/// Slot index into a GPT
#[repr(transparent)]
#[derive(Copy, Clone)]
pub struct CapSlot(pub u8);

/// Capability location
///
/// Defines the location of a capability reachable from a process slot
#[repr(C)]
pub enum CapLoc {
    /// A capability register
    Reg(CapSlot),
    /// The successful target of walking the address space cap
    MemAddr(u64),
    /// The successful target of walking a cap register `.0` with an address `.1`
    Addr(CapSlot, u64),
}
