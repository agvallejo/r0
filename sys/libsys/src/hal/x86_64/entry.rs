//! Common stubs for primordial userspace programs
//!
//! Typical stubs needed for userspace programs. Normally these would live
//! in a sort of crt0.o, but we have nothing of the sort.

core::arch::global_asm!(
    ".section .text",
    ".global _start",
    "_start:",
    "    lea rsp, dword ptr [1f]",
    "    mov rbp, rsp",
    "    jmp main",
    ".section .bss",
    "  .balign 16",
    "  .byte 0",
    "  .balign 0x10000",
    "1:",
);

/// Common panic handler for primordial userspace programs
pub fn panic(_info: &core::panic::PanicInfo) -> ! {
    loop {
        unsafe { core::arch::asm!("mov eax, 0xdeaddead; hlt") }
    }
}
