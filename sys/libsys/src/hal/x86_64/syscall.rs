//! Low-level implementation of capability invocations in userspace

use crate::base::WordCtl;

/// Invokes the capability in slot `cap` with the given message
///
/// # Safety
/// Capability invocations may cause memory to shift under the process' feet so
/// they are always unsafe. The caller must provide a safe wrapper for them.
#[inline(always)]
pub unsafe fn cap_invoke(wctl: &mut WordCtl, arg: &mut [u64; 4]) {
    core::arch::asm!(
        "int 32",
        inout("rax") wctl.0,
        inout("rdi") arg[0],
        inout("rsi") arg[1],
        inout("rcx") arg[2],
        inout("rdx") arg[3],
        options(nomem)
    );
}
