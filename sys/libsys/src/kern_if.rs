//! Set of user-kernel interfaces used during capability invocations

#[repr(C)]
pub enum OpcodeId {
    GetType = 0,

    StartSendAndBlock = 0x100,
    StartSendAndContinue = 0x101,
    ResumeCall = 0x102,
    ResumeRet = 0x103,

    SysCtlPowerOff = 0x200,
    SysCtlRestart = 0x201,

    PioCtlRead = 0x300,
    PioCtlWrite = 0x301,

    IrqCtlWait = 0x400,
    IrqCtl = 0x401,
}

#[repr(C)]
pub enum PioPort(pub u16);

pub fn pio_outb(cap: CapSlot, port: PioPort, val: u8) {
    cap_invoke(cap, );
    
}
