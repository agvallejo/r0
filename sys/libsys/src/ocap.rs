//! Capability definitions
//!
//! For use in kernel, sysimg generation and keybits handling.

use core::mem::size_of;

use num_derive::FromPrimitive;
use num_traits::FromPrimitive;

/// Number of 64bit words involved in a capability invocation message
///
/// More words mean more expressivity at the cost of register spills
///
/// Changing this means also changing the syscall stub to consume more
/// words. There's a limit given by the size of the CPU register bank.
pub const CAPINV_WORD64_COUNT: usize = 5;

/// Identifier for the kind of interface this capability designates
#[repr(C)]
#[derive(Debug, Copy, Clone, Eq, PartialEq, FromPrimitive)]
pub enum CapType {
    /// Invalid capability
    Null = 0,
    /// Designates a data page
    Page = 1,
    /// Designates a process object and allows invoking it if it's in an
    /// open-wait state
    Start = 2,
    /// Designates a process object and allows invoking it if it's blocked
    /// awaiting the return path of calling a [`Self::Start`] cap
    Resume = 3,
    /// Designates a GPT object
    Gpt = 4,
    /// Designates an abstract output serial interface
    Console = 5,
    /// Designates an abstract QEMU exit interface
    Qemu = 6,
    /// Designates a process object and allows signalling to it asynchronously
    AsyncSignal = 7,
    /// (x86-only) Designates authority over a range of PIO ports
    #[cfg(target_arch = "x86_64")]
    IoCtl = 8,
}

/// Restriction 'Read-Only'
///
/// Prevents logical mutations to the target object. This restrictions
/// makes data pages be mapped without write permissions and prevents
/// modification of in-kernel objects.
///
/// # Note
/// _Some_ modifications are unavoidable (i.e: preparing a capability),
/// but they are not _logical_ mutations. In particular, this restriction
/// prevents irreversible mutations that would end up stored in the
/// persistent store.
pub const CAP_RESTR_RO: u32 = 1 << 0;

/// Restriction 'No-eXecute'
///
/// Transitively prevents code execution through capabilities marked
/// with this bit on architectures that support it. i.e: If this bit is
/// set on a GPT walk to a page on x86_64, that page must be marked NX
pub const CAP_RESTR_NX: u32 = 1 << 1;

/// Restriction 'WeaK'
///
/// Any capability fetched through a capability with this restriction
/// is implicitly weak+readonly. The restriction is transitive, so
/// applies at any depth level in a capability walk, including walks
/// done during page faults.
pub const CAP_RESTR_WK: u32 = 1 << 2;

/// Restriction 'OPaque'
///
/// Restrict the usage of a GPT capability to that of walks. Prevents
/// them being directly invoked.
pub const CAP_RESTR_OP: u32 = 1 << 3;

/// Restriction 'No-Call'
/// When set prevents invoking an address space keeper within its
/// subtree in cases of fault. Leaving the process keeper deal with it.
pub const CAP_RESTR_NC: u32 = 1 << 4;

/// Bit offset of the capability type field.
pub const CAP_CAPTYPE_SHIFT: u32 = 0;

/// Bit width of the capability type field.
pub const CAP_CAPTYPE_BITS: u32 = 6;

/// Baseband mask of the capability type field.
pub const CAP_CAPTYPE_MASK: u32 = (1 << CAP_CAPTYPE_BITS) - 1;

/// Bit mask of the prepared bit.
pub const CAP_PREPARED_SHIFT: u32 = 6;

/// Bit offset of the capability restrictions field.
pub const CAP_RESTR_SHIFT: u32 = 7;

/// Bit width of the capability restrictions field.
pub const CAP_RESTR_BITS: u32 = 5;

/// Baseband mask of the capability restrictions field.
pub const CAP_RESTR_MASK: u32 = (1 << CAP_RESTR_BITS) - 1;

/// Bit offset of the allocation count field.
pub const CAP_ALLOC_COUNT_SHIFT: u32 = 12;

/// Bit width of the allocation count field.
pub const CAP_ALLOC_COUNT_BITS: u32 = 20;

/// Baseband mask of the allocation count field.
pub const CAP_ALLOC_COUNT_MASK: u32 = (1 << CAP_ALLOC_COUNT_BITS) - 1;

/// Layout used for the capabilities contained inside any objects in the global object vectors
#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct Cap {
    /// Collection of bit fields. Accesible through functions like
    /// [`Self::set_alloc_count`], or [`Self::set_prepared`].
    pub bitfield: u32,

    /// Content depends on captype. For [`CapType::Start`] capabilities, this is
    /// the protected payload. For [`CapType::Gpt`] and [`CapType::Page`] this
    /// is the slot_bitsz of the target memory object.
    pub payload: u32,

    /// High half of the OID. Doubles as _OteId_ when the P bit is set.
    pub oid_lo: u32,

    /// High half of the OID. Doubles as the captype-specific _ObjId_ when the P
    /// bit is set.
    pub oid_hi: u32,
}

impl Cap {
    /// Bitfield of restrictions applied to this capability
    ///
    /// See [`CAP_RESTR_RO`], for example.
    pub fn restr(&self) -> u8 {
        u8::try_from((self.bitfield >> CAP_RESTR_SHIFT) & CAP_RESTR_MASK).unwrap()
    }

    /// Parses the captype field and returns a Rust-native enum
    pub fn typ(&self) -> CapType {
        FromPrimitive::from_u32((self.bitfield >> CAP_CAPTYPE_SHIFT) & CAP_CAPTYPE_MASK).unwrap()
    }

    /// Checks the prepared bit in the capability header
    ///
    /// When false: oid_lo=oid[31..0] and oid_hi=[oid[63..32]
    /// When true: oid_lo holds an OteId and oid_hi some captype-specific ObjId
    pub fn prepared(&self) -> bool {
        (self.bitfield >> CAP_PREPARED_SHIFT) & 1 == 1
    }

    /// Sets the prepared bit in the capability header
    pub fn set_prepared(&mut self) {
        self.bitfield |= 1 << CAP_PREPARED_SHIFT;
    }

    /// Clears the prepared bit in the capability header.
    pub fn clear_prepared(&mut self) {
        self.bitfield &= !(1 << CAP_PREPARED_SHIFT);
    }

    /// Queries the allocation count of a capability.
    pub fn alloc_count(&self) -> u32 {
        (self.bitfield >> CAP_ALLOC_COUNT_SHIFT) & CAP_ALLOC_COUNT_MASK
    }

    /// Sets the allocation count in in a capability.
    pub fn set_alloc_count(&mut self, val: u32) {
        assert!(val <= CAP_ALLOC_COUNT_MASK);
        self.bitfield &= !(CAP_ALLOC_COUNT_MASK << CAP_ALLOC_COUNT_SHIFT);
        self.bitfield |= (val << CAP_ALLOC_COUNT_SHIFT) & CAP_ALLOC_COUNT_MASK;
    }
}

#[doc(hidden)]
const _: [u8; 16] = [0; size_of::<Cap>()];
