//! Common definitions for the intial system image
//!
//! These are definitions shared between mksys and the kernel in order to simplify serializing and
//! deserializing objects. The layout of the initial system image (sysimg) is.
//!
//! A few requirements for different components so everything fits together:
//!  * The bootloader _must_ ensure sysimg is loaded at a page boundary.
//!    * So that vector alignment is preserved.
//!  * mksys _must_ ensure the offset of the page vector is page-aligned.
//!    * So that page headers may bind directly to the sysimg vector.
//!  * mksys _must_ provide images tagged with [`crate::VERSION`].
//!  * The kernel _must_ check its own [`crate::VERSION`] matches that of sysimg.

use crate::{ocap::Cap, GPT_SIZE};

/// Magic tag to detect obvious _"This ain't a sysimg!"_ cases
pub const SYSIMG_MAGIC: u64 = 0x1992041319920413;

/// Header of of the initial system image
///
/// Must be page-aligned when mapped on the kernel
#[repr(C)]
pub struct SysImgHeader {
    /// Magic tag
    pub magic: u64,
    /// ABI version
    pub version: u64,
    /// Size of the image in octets
    pub total_octets: u64,
    /// Number of processes in the image
    pub proc_len: u64,
    /// Offset of the array of processes into the image
    pub proc_offset: u64,
    /// Number of GPTs in the image
    pub gpt_len: u64,
    /// Offset of the array of GPTs into the image
    pub gpt_offset: u64,
    /// Number of pages in the image
    pub page_len: u64,
    /// Offset of the array of pages into the image
    pub page_offset: u64,
    /// Number of capabilities per GPT
    pub caps_per_gpt: u64,
    /// Number of capability registers per process
    pub capregs_per_proc: u64,
    /// Size of each capability
    pub octets_per_cap: u64,
}

/// A serialised GPT in the GPT array of a sysimg
#[repr(C)]
#[derive(Debug)]
pub struct SerialisedGpt {
    /// Virtual span of each slot of the GPT
    pub slot_bitsz: u64,
    /// Fixed collection of capabilities for the GPT
    pub cap: [Cap; GPT_SIZE],
}

/// A serialised process in the process array of a sysimg
#[repr(C)]
#[derive(Debug)]
pub struct SerialisedProcess {
    /// Entry point of this process
    pub entry: u64,
    /// Capability used to identify the address space of this process
    pub addr_space: Cap,
    /// Fixed collection of capabilities for the process
    pub slot: [Cap; GPT_SIZE],
}
