//----------------------------------------------------------------
// Constants
//----------------------------------------------------------------
/// Virtually addressable space
pub const ARCH_BITS: usize = 47;

/// log2(PAGE_SIZE)
pub const PAGE_BITS: usize = 12;
/// Size of a page in octets
pub const PAGE_SIZE: usize = 1 << PAGE_BITS;
/// Low-mask of PAGE_BITS
pub const PAGE_MASK: usize = PAGE_SIZE - 1;

/// log2(GPT_SIZE)
pub const GPT_BITS: usize = 4;
/// Number of capabilities in a GPT
pub const GPT_SIZE: usize = 1 << GPT_BITS;

//----------------------------------------------------------------
// Types
//----------------------------------------------------------------
/// An entry point into a process
#[derive(Copy, Clone, Debug)]
pub struct VirtAddr(pub u64);

/// An Object Identifier (OID)
#[derive(Copy, Clone, Debug)]
pub struct Oid(pub usize);

/// Protected Payload inside an Entry Capability
#[derive(Copy, Clone, Debug)]
pub struct ProtPayload(pub u64);

/// log2(size) of each slot of a GPT
#[derive(Copy, Clone, Debug)]
pub struct SlotSz(pub u8);

//----------------------------------------------------------------
// Helpers
//----------------------------------------------------------------
/// A user friendly(-er) way to exit mksys
pub fn mistake(msg: &str) -> ! {
    eprintln!("[ERROR] {}", msg);
    std::process::exit(-1);
}

/// Align a number up to the nearest page boundary
pub fn page_align_up(n: usize) -> usize {
    PAGE_SIZE * ((n + PAGE_SIZE - 1) / PAGE_SIZE)
}

/// Finds the right slot size for a GPT that covers the range 0..end
pub fn find_slotsz(end: u64) -> usize {
    for bits in ((GPT_BITS + PAGE_BITS)..ARCH_BITS).step_by(GPT_BITS) {
        if (1 << bits) >= end {
            return bits - GPT_BITS;
        }
    }
    unreachable!();
}
