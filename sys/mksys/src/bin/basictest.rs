//! test - basictest
//!
//! This sysimg contains just one idle process. Meant for bootstrap only.

use env_logger::Env;
use mksys::ocap::Cap;
use mksys::sysimg::SysImg;

fn main() {
    env_logger::Builder::from_env(Env::default().default_filter_or("info")).init();

    let args: Vec<String> = std::env::args().collect();
    let builddir = &args[1];
    let mut img = SysImg::new(builddir);

    // --------------------------------------------------------------------
    let rx_oid = img.add_proc(1, "src/usr/basictest/basictest.elf");
    let rx = img.fetch_proc(rx_oid);
    rx[0].set_reg(1, Cap::Console);
    rx[0].set_reg(2, Cap::Qemu);
    // --------------------------------------------------------------------
    img.write_bin("sysimg.bin");
}
