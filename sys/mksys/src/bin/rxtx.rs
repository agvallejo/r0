//! test - rxtx
//!
//! This test consists of 3 processes: 1 consumer and 2 producers. It's meant to provide a basic
//! level of confidence while testing IPC

use env_logger::Env;
use mksys::ocap::Cap;
use mksys::sysimg::SysImg;

fn main() {
    env_logger::Builder::from_env(Env::default().default_filter_or("info")).init();

    let args: Vec<String> = std::env::args().collect();
    let builddir = &args[1];
    let usrpath = &args[2];
    let path = |progname| format!("{}/{}", usrpath, progname);

    // --------------------------------------------------------------------
    let mut img = SysImg::new(builddir);

    let rx_cap = {
        let rx_oid = img.add_proc(1, &path("rx"));
        let rx = img.fetch_proc(rx_oid);
        rx[0].set_reg(1, Cap::Console);
        rx[0].set_reg(2, Cap::Qemu);
        rx[0].cap()
    };

    let tx_oid = img.add_proc(2, &path("tx"));
    let tx = img.fetch_proc(tx_oid);
    for each_tx in tx {
        each_tx.set_reg(1, Cap::Console);
        each_tx.set_reg(2, Cap::Qemu);
        each_tx.set_reg(3, rx_cap);
    }

    img.write_bin("sysimg.bin");
    // --------------------------------------------------------------------
}
