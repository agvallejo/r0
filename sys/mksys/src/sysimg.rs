use elf_rs::{Elf, ElfFile, ProgramHeaderFlags, ProgramType};
use log::{error, info};
use std::{fs, path::PathBuf, slice};

use crate::basic::*;
use crate::ocap::{Cap, Gpt, Page, Process};
use crate::range::{Range, RangeSet};

/// A structured system image
pub struct SysImg {
    /// Path to the build directory of userland programs
    builddir: PathBuf,
    /// Collection of al the pages in the system
    pages: Vec<Page>,
    /// Collection of al the GPTs in the system
    gpts: Vec<Gpt>,
    /// Collection of al the processes in the system
    procs: Vec<Process>,
}

/// Size of a capability
const CAP_SIZE: usize = core::mem::size_of::<libsys::ocap::Cap>();

impl SysImg {
    /// Query the total size of the intial system image
    fn initrd_size(&self) -> u64 {
        self.proc_pages() + self.gpt_pages() + ((1 + self.pages.len()) * PAGE_SIZE) as u64
    }

    /// Initialises an initial system image with empty contents
    pub fn new(builddir: &str) -> Self {
        info!("sysimg created");
        Self {
            builddir: PathBuf::from(builddir),
            pages: Vec::new(),
            gpts: Vec::new(),
            procs: Vec::new(),
        }
    }

    /// Query the number of pages used for holding processes
    fn proc_pages(&self) -> u64 {
        let proc_size = 8 + CAP_SIZE * (GPT_SIZE + 1);
        page_align_up(self.procs.len() * proc_size) as u64
    }

    /// Query the number of pages used for holding GPTs
    fn gpt_pages(&self) -> u64 {
        let gpt_size = 8 + GPT_SIZE * CAP_SIZE;
        page_align_up(self.gpts.len() * gpt_size) as u64
    }

    /// Store the initial system image at a certain path
    pub fn write_bin(&mut self, path: &str) {
        info!("sysimg start");

        let path = self.builddir.as_path().join(path);
        info!("path={}", path.display());
        let mut bin = Vec::<u64>::new();

        // ------- header start -------

        let mut offset: u64 = PAGE_SIZE.try_into().unwrap();

        bin.push(libsys::sysimg::SYSIMG_MAGIC); // magic
        bin.push(0); // version
        bin.push(self.initrd_size());

        bin.push(self.procs.len().try_into().unwrap()); // proc len
        bin.push(offset); // proc offset
        info!("proc_offset={:#x}", offset);
        info!("proc_len={}", self.procs.len());
        offset += self.proc_pages();

        bin.push(self.gpts.len().try_into().unwrap()); // gpt len
        bin.push(offset); // gpt offset
        info!("gpt_offset={:#x}", offset);
        info!("gpt_len={}", self.gpts.len());
        offset += self.gpt_pages();

        bin.push(self.pages.len().try_into().unwrap()); // page len
        bin.push(offset); // page offset
        info!("page_offset={:#x}", offset);
        info!("page_len={}", self.pages.len());

        let caps_per_gpt: u64 = GPT_SIZE.try_into().unwrap();
        bin.push(caps_per_gpt);
        info!("caps_per_gpt={}", caps_per_gpt);

        let capregs_per_process: u64 = GPT_SIZE.try_into().unwrap();
        bin.push(capregs_per_process);
        info!("capregs_per_process={}", capregs_per_process);

        bin.push(CAP_SIZE.try_into().unwrap());
        info!("octets_per_cap={}", CAP_SIZE);

        // And now pad the rest of the page with zeroes
        while (8 * bin.len()) % PAGE_SIZE != 0 {
            bin.push(0); // padding
        }

        // ------- header end -------

        for proc in self.procs.iter() {
            proc.write_bin(&mut bin);
        }
        while (8 * bin.len()) % PAGE_SIZE != 0 {
            bin.push(0); // padding
        }
        for gpt in self.gpts.iter() {
            gpt.write_bin(&mut bin);
        }
        while (8 * bin.len()) % PAGE_SIZE != 0 {
            bin.push(0); // padding
        }
        for page in self.pages.iter() {
            page.write_bin(&mut bin);
        }

        let slice = unsafe { slice::from_raw_parts(bin.as_ptr() as *const u8, 8 * bin.len()) };
        fs::write(path, slice).unwrap();
    }

    /// Fetches processes from the process vector. Intended to be used with
    /// the same tuple of (BaseOid, NumOids) returned by add_proc
    pub fn fetch_proc(&mut self, oid_range: (Oid, usize)) -> &mut [Process] {
        let (Oid(base_oid), num_oids) = oid_range;

        let oid_end = base_oid + num_oids;
        &mut self.procs[base_oid - 1..oid_end - 1]
    }

    /// Adds N processes to the initial system image using an ELF file
    /// as the input. This procedure is smart enough to share immutable
    /// pages and GPTs across all processes and ensure all paths leading
    /// to mutable pages are process-exclusive
    ///
    /// Returns the range of OIDs allocated for these processes
    pub fn add_proc(&mut self, nprocs: usize, path: &str) -> (Oid, usize) {
        let path: PathBuf = path.into();
        let elf_buf = match std::fs::read(&path) {
            Ok(buf) => buf,
            Err(err) => {
                error!("bad file: {} | {}", path.display(), err);
                std::process::exit(-1);
            }
        };
        let elf = Elf::from_bytes(&elf_buf).unwrap();

        // Collection of all ranges
        let mut rangeset = RangeSet::default();
        for phdr in elf.program_header_iter() {
            if phdr.ph_type() != ProgramType::LOAD {
                /* Skip non-loadable segments */
                continue;
            }

            let can_write = phdr.flags().contains(ProgramHeaderFlags::WRITE);
            let can_exec = phdr.flags().contains(ProgramHeaderFlags::EXECUTE);
            assert_le!(phdr.filesz(), phdr.memsz());
            if can_write && can_exec {
                mistake("Unsafe ELF. broken R^W");
            }

            let memsz = PAGE_SIZE * ((phdr.memsz() as usize + PAGE_SIZE - 1) / PAGE_SIZE);

            rangeset.push(Range {
                vaddr: phdr.vaddr() & !(PAGE_MASK as u64),
                can_write,
                can_exec,
                memsz: page_align_up(memsz),
                content: phdr.content().unwrap(),
            });
        }

        // Generate the address space capabilities. 1 per process
        let gpt_caps = self.gen_tree(rangeset, nprocs, 0);
        assert!(gpt_caps.len() == nprocs);

        for cap in gpt_caps {
            self.procs.push(Process::new(
                Oid(self.procs.len() + 1),
                cap,
                VirtAddr(elf.entry_point()),
            ));
        }

        // We've allocated the last `nprocs` OIDs
        (Oid(self.procs.len() - nprocs + 1), nprocs)
    }

    /// Generate a set of N capabilities intended for each of N processes.
    /// They are memory capabilities (or the Invalid capability) covering the
    /// the span of the set of memory ranges passed as a parameter. If all
    /// the ranges are immutable then all the capabilities returned are
    /// equal and point to the same memory object.
    fn gen_tree(&mut self, rset: RangeSet, nprocs: usize, lvl: usize) -> Vec<Cap> {
        assert_ne!(lvl, (ARCH_BITS - PAGE_BITS) / GPT_BITS);
        // Case 1: Empty range. Nothing to do.
        if rset.is_empty() {
            return vec![Cap::Invalid; nprocs];
        }

        // Case 2: Single page range. We can return the page already.
        if rset.end() == PAGE_SIZE as u64 {
            assert_eq!(rset.len(), 1);
            let mut caps = vec![Cap::Invalid; nprocs];
            for cap_i in &mut caps {
                let page = Page::new(Oid(self.pages.len() + 1), &rset.page());
                let cap = page.cap(rset.mutable(), rset.executable());
                self.pages.push(page);
                if !rset.mutable() {
                    // Immutable pages can just be shared. Use the same page
                    return vec![cap; nprocs];
                }
                *cap_i = cap;
            }
            return caps;
        }

        // Case 3: More than a page in the rangeset. Need a new GPT.

        // Find the size of the smallest GPT that spans at least until
        // the end of the rangeset. By substracting GPT_BITS we have
        // the size of each slot of the GPT.
        let slotsz = find_slotsz(rset.end());

        // Allocate the GPTs. Just 1 if the rangeset is fully immutable
        // because it can be shared
        let mut gpt_caps = Vec::new();
        for _ in 0..nprocs {
            let gpt = Gpt::new(Oid(self.gpts.len() + 1), SlotSz(slotsz as u8));
            let cap = gpt.cap(rset.mutable(), rset.executable());
            gpt_caps.push(cap);
            self.gpts.push(gpt);
            if !rset.mutable() {
                for _ in 1..nprocs {
                    gpt_caps.push(cap);
                }
                break;
            }
        }

        // Base index for the GPTs we'll be modifying
        let gpts_base = if rset.mutable() {
            self.gpts.len() - nprocs
        } else {
            self.gpts.len() - 1
        };

        // For each GPT we'll go slot by slot and fill in the required
        // capabilities. We need to be careful because in the immutable
        // case there's only a single GPT rather than 1 per process.
        for slot in 0..GPT_SIZE {
            // This slot spans the following parts of the address space
            let start = slot as u64 * (1 << slotsz);
            let end = (slot as u64 + 1) * (1 << slotsz);

            // The caps for all the processes. All caps are equal if the sliced
            // rangeset is immutable, otherwise they point to different GPTs
            let caps = self.gen_tree(rset.slice(start, end), nprocs, lvl + 1);

            // Clippy is right, but it's far from obvious what's going on if we follow the advice
            #[allow(clippy::needless_range_loop)]
            for proc in 0..nprocs {
                self.gpts[gpts_base + proc].slots[slot] = caps[proc];

                if !rset.mutable() {
                    break;
                }
            }
        }
        gpt_caps
    }
}
