//! mksys - The r0 initial system image generator
//!
//! This is a library to ease generating initial system images for the r0 microkernel.

#[macro_use]
extern crate more_asserts;

/// Trivial helpers to ease writing everything
pub mod basic;

/// Object capability model
pub mod ocap;

/// Range manipulation
///
/// Used for parsing ELF files
pub mod range;

/// Controls the generation of the output binary
pub mod sysimg;
