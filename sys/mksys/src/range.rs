use crate::basic::PAGE_SIZE;

/// A consecutive range of pages with the same permissions
#[derive(Debug)]
pub struct Range<'a> {
    /// Base address of the range (is page aligned)
    pub vaddr: u64,
    /// The range can be written to
    pub can_write: bool,
    /// The range can be executed
    pub can_exec: bool,
    /// Total range size in octets
    pub memsz: usize,
    /// Contents of each individual page
    ///
    /// May be _less_ than memsz. If so, the remainder of the range is bss
    pub content: &'a [u8],
}

impl<'a> Range<'a> {
    /// Determines if the range is (at least partially) writable
    fn mutable(&self) -> bool {
        self.can_write
    }

    /// Determines if the range is (at least partially) executable
    fn executable(&self) -> bool {
        self.can_exec
    }

    /// Retrieve the base address of the range
    fn start(&self) -> u64 {
        self.vaddr
    }

    /// Retrieve the (non-inclusive) last address of the range.
    ///
    /// NOTE: Can be zero for the case where the range is the last in the address space
    fn end(&self) -> u64 {
        self.start() + self.memsz as u64
    }

    /// Retrieve the (non-inclusive) last address of the range that is backed by actual data.
    /// This maps to filesz in ELF files.
    fn end_content(&self) -> u64 {
        self.start() + self.content.len() as u64
    }

    /// Determine if this range overlaps with the provided range
    fn disjoint(&self, start: u64, end: u64) -> bool {
        end <= self.start() || start >= self.end()
    }

    /// Determine the intersection between this range and the provided range
    fn slice(&self, start: u64, end: u64) -> Self {
        let offset = if start <= self.start() {
            0
        } else if start <= self.end_content() {
            (start - self.start()) as usize
        } else {
            self.content.len()
        };

        let limit = if end <= self.end_content() {
            (end - self.start()) as usize
        } else {
            self.content.len()
        };

        let slice_start = std::cmp::max(start, self.start());
        let slice_end = std::cmp::min(end, self.end());

        Range {
            vaddr: slice_start - start,
            can_write: self.can_write,
            can_exec: self.can_exec,
            memsz: (slice_end - slice_start) as usize,
            content: &self.content[offset..limit],
        }
    }
}

/// Set of address ranges
#[derive(Default, Debug)]
pub struct RangeSet<'a> {
    /// Vector containing the set
    vec: Vec<Range<'a>>,
}

impl<'a> RangeSet<'a> {
    /// Pushes a range into the set
    pub fn push(&mut self, item: Range<'a>) {
        //println!("pushed item_vaddr={}, item_memsz={}", item.vaddr, item.memsz);
        if !self.vec.is_empty() {
            assert_le!(self.vec.last().unwrap().end(), item.start());
        }
        assert_ge!(item.memsz, item.content.len());
        self.vec.push(item);
    }

    /// If the range consists of a single page, returns a vector to the contents of said page
    ///
    /// Panics if there's more than one range or more than one page in the range
    pub fn page(&self) -> Vec<u8> {
        assert_eq!(self.vec.len(), 1);
        assert_eq!(self.vec[0].memsz, PAGE_SIZE);

        let mut content = vec![0u8; PAGE_SIZE];
        content[..self.vec[0].content.len()].copy_from_slice(self.vec[0].content);
        content
    }

    /// Returns the number of ranges in the set
    pub fn len(&self) -> usize {
        self.vec.len()
    }

    /// True iff the set is empty
    pub fn is_empty(&self) -> bool {
        self.vec.is_empty()
    }

    /// Returns the page with the smallest address from the set
    pub fn start(&self) -> u64 {
        assert!(!self.vec.is_empty());
        self.vec[0].start()
    }

    /// Returns the page with the highest address from the set
    ///   (non-inclusive. So the page is _not_ in the set and may be zero)
    pub fn end(&self) -> u64 {
        assert!(!self.vec.is_empty());
        self.vec.last().unwrap().end()
    }

    /// True iff there's at least one mutable range in the set
    pub fn mutable(&self) -> bool {
        for range in self.vec.iter() {
            if range.mutable() {
                return true;
            }
        }
        false
    }

    /// True iff there's at least one executable range in the set
    pub fn executable(&self) -> bool {
        for range in self.vec.iter() {
            if range.executable() {
                return true;
            }
        }
        false
    }

    /// Create a subset of this set, between the provided boundaries
    ///
    /// e.g: [0x1000, 0x3000].slice(0x2000, 0x4000) = [0x2000, 0x3000]
    pub fn slice(&self, start: u64, end: u64) -> Self {
        let mut set = RangeSet::default();
        for range in self.vec.iter() {
            if !range.disjoint(start, end) {
                set.push(range.slice(start, end));
            }
        }
        set
    }
}
