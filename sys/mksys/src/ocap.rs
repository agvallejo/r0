use crate::basic::*;

use libsys::ocap::{CapType, CAP_RESTR_NX, CAP_RESTR_RO, CAP_RESTR_SHIFT, CAP_RESTR_WK};
use log::debug;

//----------------------------------------------------------------
// Objects
//----------------------------------------------------------------

/// A Page object
pub struct Page {
    /// Object Identifier
    oid: Oid,
    /// Octets contained in the page
    content: [u8; PAGE_SIZE],
}

impl Page {
    /// Create a new page object
    pub fn new(oid: Oid, content: &[u8]) -> Self {
        let mut page = [0u8; PAGE_SIZE];
        page.copy_from_slice(content);
        Page { oid, content: page }
    }

    /// Create a new capability for this page
    pub fn cap(&self, can_write: bool, can_exec: bool) -> Cap {
        Cap::Page(self.oid, Restr::new(can_write, can_exec))
    }

    /// Marshall this object into its binary form
    pub fn write_bin(&self, bin: &mut Vec<u64>) {
        for i in (0..PAGE_SIZE).step_by(8) {
            let word: u64 = (u64::from(self.content[i]))
                | (u64::from(self.content[i + 1]) << 8)
                | (u64::from(self.content[i + 2]) << 16)
                | (u64::from(self.content[i + 3]) << 24)
                | (u64::from(self.content[i + 4]) << 32)
                | (u64::from(self.content[i + 5]) << 40)
                | (u64::from(self.content[i + 6]) << 48)
                | (u64::from(self.content[i + 7]) << 56);
            bin.push(word);
        }
    }
}

/// A Guarded Page Table object
#[derive(Debug)]
pub struct Gpt {
    /// Object Identifier
    pub oid: Oid,
    /// Virtual span of each slot of the GPT in bits
    ///
    /// eg: slotsz=21 means each slot is 2MiB (1 << 21) in size, which means the
    ///     whole GPT spans GPT_SIZE * 2MiB.
    pub slotsz: SlotSz,
    /// Array containing each slot of the GPT
    pub slots: [Cap; GPT_SIZE],
}
impl Gpt {
    /// Create a new GPT object
    pub fn new(oid: Oid, slotsz: SlotSz) -> Self {
        assert!((slotsz.0 as usize - PAGE_BITS) % GPT_BITS == 0);

        Gpt {
            oid,
            slotsz,
            slots: [Cap::Invalid; GPT_SIZE],
        }
    }

    /// Create a new capability to this GPT object
    pub fn cap(&self, can_write: bool, can_exec: bool) -> Cap {
        Cap::Gpt(self.oid, Restr::new(can_write, can_exec), self.slotsz)
    }

    /// Marshall this object into its binary form
    pub fn write_bin(&self, bin: &mut Vec<u64>) {
        debug!("gpt write {self:?}");
        bin.push(self.slotsz.0.into());
        for cap in self.slots {
            cap.write_bin(bin);
        }
    }
}

/// A Process object
#[derive(Debug)]
pub struct Process {
    /// Object Identifier
    oid: Oid,
    /// Capability to the virtual address space
    addr_space: Cap,
    /// Entry point
    entry: VirtAddr,
    /// Capability registers
    caps: [Cap; GPT_SIZE],
}

impl Process {
    /// Create a new process
    pub fn new(oid: Oid, addr_space: Cap, entry: VirtAddr) -> Self {
        Process {
            oid,
            addr_space,
            entry,
            caps: [Cap::Invalid; GPT_SIZE],
        }
    }

    /// Set the content of one of its capability registers
    pub fn set_reg(&mut self, slot: usize, cap: Cap) {
        self.caps[slot] = cap;
    }

    /// Get a start capability to this process
    pub fn cap(&self) -> Cap {
        Cap::Start(self.oid, ProtPayload(0))
    }

    /// Marshall the process into binary form
    pub fn write_bin(&self, bin: &mut Vec<u64>) {
        debug!("proc write {self:?}");
        bin.push(self.entry.0);
        self.addr_space.write_bin(bin);
        for cap in self.caps.iter() {
            cap.write_bin(bin);
        }
    }
}

//----------------------------------------------------------------
// Capabilities
//----------------------------------------------------------------

/// Capability restrictions
///
/// A subset of the restrictions a capability can have
#[derive(Copy, Clone, Debug)]
pub struct Restr {
    /// The target object of this capability cannot be written to
    pub readonly: bool,
    /// The target object of this capability cannot be executed
    ///
    /// NOTE: This has implications for page table management mostly
    pub noexec: bool,
}

impl Restr {
    /// Creates a new restriction set
    fn new(can_write: bool, can_exec: bool) -> Self {
        Restr {
            readonly: !can_write,
            noexec: !can_exec,
        }
    }

    /// Serialises these restrictions into the value used in capabilities
    fn write(&self) -> u64 {
        let mut ret = 0;
        if self.readonly {
            ret |= CAP_RESTR_RO | CAP_RESTR_WK; // Weak|ReadOnly
        }
        if self.noexec {
            ret |= CAP_RESTR_NX; // NoExec
        }
        ret.into()
    }
}

/// A capability, as written in the initial system image
///
/// This is a much simplified form compared to the kernel
#[derive(Copy, Clone, Debug)]
pub enum Cap {
    /// An invalid (or null) capability
    Invalid,
    /// A page capability
    Page(Oid, Restr),
    /// A Guarded Page Table capability
    Gpt(Oid, Restr, SlotSz),
    /// A Start capability to some process
    Start(Oid, ProtPayload),
    /// A Console capability
    Console,
    /// A QEMU debug capability
    Qemu,
    /// A PIO handling capability. Range is inclusive.
    IoCtl {
        /// First PIO port allowed by this cap.
        start: u16,
        /// Last PIO port allowed by this cap.
        end: u16,
    },
}

impl Cap {
    /// Marshall this capability into its binary form
    pub fn write_bin(&self, bin: &mut Vec<u64>) {
        match self {
            Cap::Invalid => {
                bin.push(0); // Cap header
                bin.push(0); // Cap OID
            }
            Cap::Page(oid, restr) => {
                let mut hdr: u64 = CapType::Page as u64;
                hdr |= restr.write() << CAP_RESTR_SHIFT;
                hdr |= (PAGE_BITS as u64) << 32; // bitsz
                bin.push(hdr);
                bin.push(oid.0.try_into().unwrap());
            }
            Cap::Gpt(oid, restr, slotsz) => {
                let mut hdr: u64 = CapType::Gpt as u64;
                hdr |= restr.write() << CAP_RESTR_SHIFT;
                hdr |= u64::from(slotsz.0 + u8::try_from(GPT_BITS).unwrap()) << 32; // bitsz
                bin.push(hdr);
                bin.push(oid.0.try_into().unwrap());
            }
            Cap::Start(oid, pp) => {
                let mut hdr = CapType::Start as u64;
                hdr |= pp.0 << 32; // pp
                bin.push(hdr);
                bin.push(oid.0.try_into().unwrap());
            }
            Cap::Console => {
                let hdr = CapType::Console as u64;
                bin.push(hdr);
                bin.push(0); // No OID
            }
            Cap::Qemu => {
                let hdr = CapType::Qemu as u64;
                bin.push(hdr);
                bin.push(0); // No OID
            }
            Cap::IoCtl { start, end } => {
                let mut hdr = CapType::IoCtl as u64;
                hdr |= u64::from(*start) << 32;
                hdr |= u64::from(*end) << 48;
                bin.push(hdr);
                bin.push(0); // No OID
            }
        }
    }
}
