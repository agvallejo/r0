//! This is a kernel reference manual. Go [here](/r0) instead if you're looking for the top-level
//! documentation.
//!
//! The kernel is an event-driven machine that reacts to external stimuli. The most essential
//! events to have anything running at all are page faults and syscalls.
//!
//! In either case, kernel entries and exits are (amongs other things) handled in [`hal`]. Page
//! faults are the main mechanism for memory management in userspace, with the logic codified in
//! [`mapping`]. As for syscalls, they (almost) always involve capability invocations, which follow
//! the [`ocap`] module. The [`log`] module is a wrapper to ease emitting events in the kernel.
#![cfg_attr(not(feature = "std"), no_std)]
#![cfg_attr(not(feature = "std"), no_main)]
#![cfg_attr(feature = "std", feature(thread_local))]
#![feature(panic_info_message)]
#![feature(sync_unsafe_cell)]
// FIXME: The code is too immature and we need to build the data structures before we can build the
// implementations to bridge them
#![allow(dead_code)]

mod fb;
mod global;
mod hal;
mod list;
mod log;
mod mapping;
mod mutex;
mod ocap;
mod percpu;
mod phys;
mod sched;
mod syscall;
mod sysimg;

/// Kernel panic handler
///
/// Automatically invoked on assertion failures,
/// bounds-check failures and panic!() invocations.
#[cfg_attr(not(feature = "std"), panic_handler)]
pub fn panic(info: &core::panic::PanicInfo) -> ! {
    log::Panic(info).panic();
    hal::exit::failure();
    loop {}
}

#[cfg(kani)]
#[doc(hidden)]
fn main() {}
