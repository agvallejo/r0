//! Syscall handling
//!
//! The overwhelming majority of syscalls are capability invocations, but as a
//! matter of convenience capability copy and yielding support are provided as
//! first-class syscalls.
use crate::ocap::cap;

/// Performs a syscall
///
/// Parameters are taken from the register state of the currently running
/// process
pub fn syscall() {
    cap::invoke();
}
