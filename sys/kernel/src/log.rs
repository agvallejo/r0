//! Logging support
//!
//! This module adds the logging infrastructure for the kernel. It's
//! purpose is to generate strict `event + (key,value)` strings that can be
//! externally parsed. This allows external parsers to plot logs and do all
//! sorts of cool stuff.
//!
//! # Development note
//!
//! For true development hacks you can just do `writeln!(crate::log::Logger, ...)`
//! and use it as a regular `println` replacement. Everything commited must go
//! through regular events though.
use core::fmt;
use core::fmt::Write;

use crate::ocap::gpt::GptId;
use crate::ocap::proc::ProcId;
use crate::phys::PhysRange;
use crate::{fb, hal};

/// A generic event relevant for the kernel
///
/// Each event might have payload associated with it
#[must_use]
pub enum Event<'a> {
    /// Event emitted after each initilization step is done
    Init(&'a str),
    /// Event emitted when a memory range is added to the physical memory manager
    PhysRangeAdded(&'a PhysRange),
    /// Event emitted when a memory range is ignored by the physical memory manager
    PhysRangeDropped(&'a PhysRange),
    /// Event emitted during SMP bootstrap
    Smp(&'a str, u32),
    /// Event emitted when a heap vector gets initialised
    HeapVectorAdded {
        /// Identifier for the vector this event refers to
        name: &'a str,
        /// Number of objects allocated in the vector
        nitems: usize,
        /// Virtual start address of the vector
        start: usize,
        /// Virtual end address of the vector (non-inclusive)
        end: usize,
        /// Maximum value of `end`. Used for growing/shrinking
        max: usize,
    },
    /// Event emitted when a sysimg vector is loaded.
    SysImgLoad(&'a str, usize),
    /// Event emitted when a page fault hits.
    PageFault(ProcId, usize),
    /// Event emitted at each step of a capability walk.
    StepInCapWalk {
        /// GPT traversed in a capability walk.
        gpt: GptId,
        /// Slot of the GPT traversed in a capability walk.
        slot: usize,
        /// A copy of the capability traversed during the walk.
        cap: Cap,
    },
    /// Event emitted in case of a panic. Must not itself panic
    Panic(&'a core::panic::PanicInfo<'a>),
}
use libsys::ocap::Cap;
pub use Event::*;

impl<'a> fmt::Display for Event<'a> {
    /// Formatter for an event
    ///
    /// Each event has hard-coded strings for the events and the keys. The
    /// values are filled by the event's payload.
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Init(s) => write!(f, "init stage={}", s),
            PhysRangeAdded(r) => write!(
                f,
                "physmem_add  start={:#018x} end={:#018x} usage={:?} type={}",
                r.start, r.end, r.usage, r.typ
            ),
            PhysRangeDropped(r) => write!(
                f,
                "physmem_drop start={:#018x} end={:#018x} usage={:?} type={}",
                r.start, r.end, r.usage, r.typ
            ),
            Smp(s, id) => write!(f, "smp stage={} x2apic_id={:#08x}", s, id),
            HeapVectorAdded {
                name,
                nitems,
                start,
                end,
                max,
            } => write!(
                f,
                "heapvec_add id={} start={:#018x} end={:#018x} max={:#018x} nitems={}",
                name, start, end, max, nitems
            ),
            PageFault(procid, addr) => write!(f, "pagefault proc={} addr={:#08x}", procid.0, addr),
            StepInCapWalk { gpt, slot, cap } => write!(
                f,
                "capwalk_step gpt={} slot={} captype={:#x}",
                gpt.0, slot, cap.bitfield
            ),
            SysImgLoad(s, c) => write!(f, "sysimg_load id={} count={}", s, c),
            Panic(i) => {
                write!(f, "panic")?;
                if let Some(loc) = i.location() {
                    write!(f, " line={} file={}", loc.line(), loc.file())?;
                }
                if let Some(msg) = i.message() {
                    write!(f, " {}", msg)?;
                }
                Ok(())
            }
        }
    }
}

impl<'a> Event<'a> {
    /// General logger
    ///
    /// Prints an event at the specified level
    #[inline(always)]
    fn emit(&self, lvl: &str) -> fmt::Result {
        writeln!(Logger, "{} - {}", lvl, self)
    }

    /// Info logger
    ///
    /// Prints an event at the INFO level
    #[inline(always)]
    pub fn info(&self) {
        self.emit("INFO ").unwrap();
    }

    /// Debug logger
    ///
    /// Prints an event at the DEBUG level
    #[inline(always)]
    pub fn debug(&self) {
        self.emit("DEBUG").unwrap();
    }

    /// Warning logger
    ///
    /// Prints an event at the WARN level
    #[inline(always)]
    pub fn warn(&self) {
        self.emit("WARN ").unwrap();
    }

    /// Prints an event during a panic
    ///
    /// It must _not_ unwrap!
    pub fn panic(&self) {
        let _ = self.emit("PANIC");
    }
}

/// An unary struct representing the logger
///
/// It's meant to implement the Write trait only and have the
/// backends (serial, gfx...) attached.
pub struct Logger;

impl Write for Logger {
    /// Writes a formatted string to the logger backends
    fn write_str(&mut self, s: &str) -> fmt::Result {
        for c in s.as_bytes() {
            hal::serial::put(*c);
        }
        fb::print(s);
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn emit_event() {
        Init("some_stage_with_a_name").info();
        let range = PhysRange {
            start: 0,
            end: 0x1000,
            usage: crate::phys::PhysUsage::Rsvd,
            typ: crate::phys::PhysType::BootloaderReclaimable,
        };
        PhysRangeAdded(&range).warn();
        PhysRangeDropped(&range).panic();
        PhysRangeDropped(&range).debug();
    }
}
