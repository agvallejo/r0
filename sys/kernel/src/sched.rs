//! Scheudler
//!
//! Simple global scheduler following round-robin.

use core::mem::offset_of;

use spin::mutex::SpinMutex;

use crate::hal::mm::this_cpu;
use crate::hal::percpu::update_isr_sp;
use crate::list::{Dhead, Dlist};
use crate::ocap::proc::{ProcId, ProcRef, ProcState, Process};
use crate::{global::HeapRef, hal};

/// Global ready queue. Every CPU pops a next CPU after each tick (or IO block) from here
static READY_QUEUE: SpinMutex<Dhead> = SpinMutex::new(unsafe { core::mem::zeroed() });

/// Get the process associated with a `q_cur` link.
unsafe fn link2proc(link: &Dlist) -> &Process {
    let link_addr = core::ptr::addr_of!(*link) as *const u8;
    &*(link_addr.wrapping_sub(offset_of!(Process, q_cur)) as *const Process)
}

/// Pushes the currently running process to the back of the ready
/// queue and runs the one at the front (may be the same).
pub fn yield_() -> ! {
    let cur = this_cpu().cur.take().unwrap();
    // Safe because the linked list is covered by a mutex
    unsafe { READY_QUEUE.lock().pushback(&cur.get().q_cur) };
    run();
}

/// Exits the kernel running the next ready process
pub fn run() -> ! {
    assert!(this_cpu().cur.get().is_none());
    this_cpu().cur.set({
        let rq = READY_QUEUE.lock();
        if let Some(dlist) = rq.peekfront() {
            let proc = unsafe { link2proc(dlist) };
            unsafe { proc.q_cur.unbind() };
            update_isr_sp(proc);
            Some(ProcRef::new(ProcId(proc.hdr.objidx)))
        } else {
            todo!("Nothing to run");
        }
    });

    hal::asm::ret_to_userspace();
}

/// Executed when coming from idle. Pops the next to-be-run
/// process and sets it up for running on exit.
pub fn block(to: &Process, from: &Process) -> ! {
    assert_ne!(to as *const _, from as *const _);

    assert_eq!(from.state.get(), ProcState::Running);
    assert_ne!(to.state.get(), ProcState::Available);
    assert!(from.q_cur.is_empty());

    to.state.set(ProcState::Ready);

    from.arch.dec_ip();
    unsafe {
        to.q_blocked.pushback(&from.q_cur);
    }
    this_cpu().cur.set(None);

    run();
}

/// Have `from` _call_ `to`, blocking on its response via resume capability
pub fn call(to: &Process, from: &Process) -> ! {
    assert_ne!(to as *const _, from as *const _);

    assert_eq!(from.state.get(), ProcState::Running);
    assert_ne!(to.state.get(), ProcState::Running);
    assert_ne!(to.state.get(), ProcState::Ready);
    assert!(from.q_cur.is_empty());

    to.state.set(ProcState::Running);
    from.state.set(ProcState::Paused);

    this_cpu()
        .cur
        .set(Some(ProcRef::new(ProcId(to.hdr.objidx))));
    update_isr_sp(to);
    hal::asm::ret_to_userspace();
}

/// Have `from` _return_ to `to`, going on an open wait after
pub fn ret(to: Option<&Process>, from: &Process) -> ! {
    assert_eq!(from.state.get(), ProcState::Running);
    assert!(from.q_cur.is_empty());

    from.state.set(ProcState::Available);

    unsafe {
        // Unblock everyone previously blocked on `from`
        let rq = READY_QUEUE.lock();
        rq.mergeback(&from.q_blocked);
    }

    if let Some(to) = to {
        assert_ne!(to as *const _, from as *const _);
        assert_ne!(to.state.get(), ProcState::Running);
        assert_ne!(to.state.get(), ProcState::Ready);
        to.state.set(ProcState::Running);
        this_cpu()
            .cur
            .set(Some(ProcRef::new(ProcId(to.hdr.objidx))));
        update_isr_sp(to);
        hal::asm::ret_to_userspace();
    } else {
        this_cpu().cur.set(None);
        run();
    }
}

/// Have `from` _fork_ into `to`, going continuing running after
pub fn fork(to: &Process, from: &Process) -> ! {
    assert_ne!(to as *const _, from as *const _);

    assert_eq!(from.state.get(), ProcState::Running);
    assert_ne!(to.state.get(), ProcState::Running);
    assert_ne!(to.state.get(), ProcState::Ready);
    assert!(from.q_cur.is_empty());

    to.state.set(ProcState::Ready);
    from.state.set(ProcState::Available);

    unsafe {
        let rq = READY_QUEUE.lock();
        rq.pushback(&to.q_cur);
    }

    hal::asm::ret_to_userspace();
}

/// Pushes a process into the ready queue. Only
/// meant for initial system bootstrap
pub fn init_push(proc: &Process) {
    let rq = READY_QUEUE.lock();

    unsafe {
        // Initialise on first push. Otherwise the head won't point to itself.
        rq.init();

        // Safe because linked list is covered by a lock.
        rq.pushback(&proc.q_cur);
    }
}
