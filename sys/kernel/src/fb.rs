//! Kernel framebuffer
//!
//! VERY primitive framebuffer meant to assist while operating on real hardware.
//! Kernel messages, and console capability invocations land here.
use font8x8::BASIC_UNICODE;
use spin::mutex::SpinMutex;

use crate::hal::mm::DIRECTMAP_START;

/// r0-specific structure describing a framebuffer. A singleton of this is used
/// in [`FB_CONTEXT]` and it describes the primary framebuffer used for kernel
/// messages.
#[derive(Debug)]
struct FbContext {
    /// Pointer to the base of the framebuffer.
    vaddr: *mut u8,
    /// Width of the framebuffer in pixels.
    px_width: usize,
    /// Height of the framebuffer in pixels.
    px_height: usize,
    /// (derived from bpp) Bytes per pixel (2, 3 or 4).
    bytes_per_px: usize,
    /// Size in bytes of each row of pixels.
    pitch: usize,
    /// Width of the framebuffer in characters.
    ///
    /// Note the in-kernel framebuffer is always used as a scrolling terminal
    txt_width: usize,
    /// Height of the framebuffer in characters.
    ///
    /// Note the in-kernel framebuffer is always used as a scrolling terminal
    txt_height: usize,
    /// Index of the next character in the current line.
    txt_cur_x: usize,
    /// Index of the next line in the framebuffer.
    txt_cur_y: usize,
}

impl FbContext {
    /// Scaling factor so letters aren't tiny.
    const FONT_SCALING: usize = 1;
    /// Width of a font character in pixels.
    const FONT_WIDTH: usize = 8 * Self::FONT_SCALING;
    /// Height of a font character in pixels.
    const FONT_HEIGHT: usize = 8 * Self::FONT_SCALING;

    /// Clears the framebuffer of all text.
    pub fn clear(&mut self) {
        self.txt_cur_y = 0;
        self.txt_cur_x = 0;
        unsafe { self.vaddr.write_bytes(0, self.pitch * self.px_height) };
    }

    /// Scrolls down the terminal, creating a blank line at the bottom
    pub fn shiftline(&mut self) {
        unsafe {
            let sz = self.pitch * (self.px_height - Self::FONT_HEIGHT);
            core::ptr::copy(
                self.vaddr.add(self.pitch * Self::FONT_HEIGHT),
                self.vaddr,
                sz,
            );
        }
    }

    /// Clears the framebuffer of all text.
    pub fn putc(&mut self, c: char) {
        if c == '\n' || self.txt_cur_x == self.txt_width {
            self.txt_cur_x = 0;
            if self.txt_cur_y == self.txt_height - 1 {
                self.shiftline();
            } else {
                self.txt_cur_y += 1;
            }
            if c == '\n' {
                return;
            }
        }

        unsafe {
            let mut base = self.vaddr.add(
                Self::FONT_HEIGHT * self.pitch * self.txt_cur_y
                    + Self::FONT_WIDTH * self.bytes_per_px * self.txt_cur_x,
            );

            for y in 0..Self::FONT_HEIGHT {
                let bm = BASIC_UNICODE[c as usize].byte_array()[y / Self::FONT_SCALING];
                for x in 0..(Self::FONT_WIDTH / Self::FONT_SCALING) {
                    let val = if (bm >> x) & 1 == 1 { u8::MAX } else { 0 };

                    let bound = Self::FONT_SCALING * self.bytes_per_px;
                    base.add(x * bound).write_bytes(val, bound);
                }
                base = base.add(self.pitch);
            }
        }
        self.txt_cur_x += 1;
    }
}

unsafe impl Send for FbContext {}

/// Singleton with the kernel framebuffer, if any. Otherwise the framebuffer
/// subsystem acts as a no-op.
static FB_CONTEXT: SpinMutex<Option<FbContext>> = SpinMutex::new(None);

/// Response from the bootloader about the framebuffer. If there are some,
/// there's a response with their characteristics and the first one is chosen as
/// the kernel framebuffer and set up in [`FB_CONTEXT`].
static FB_LIMINE: limine::FramebufferRequest = limine::FramebufferRequest::new(0);

/// Reinitianisation of the framebuffer after non-boot kernel PTs are in working order
pub fn stage2_init() {
    let mut ctx = FB_CONTEXT.lock();
    let Some(fb) = ctx.as_mut() else {
        return;
    };

    let new_addr = fb.vaddr as u32 as *mut u8;
    fb.vaddr = unsafe { new_addr.add(DIRECTMAP_START) };
}

/// Early initialisation of the framebuffer. Meant to provide some early output.
pub fn init() {
    let Some(rsp) = FB_LIMINE.get_response().get() else {
        // No graphics card
        return;
    };

    let px_width = rsp.framebuffers.width as usize;
    let px_height = rsp.framebuffers.height as usize;

    let mut ctx = FB_CONTEXT.lock();
    *ctx = Some(FbContext {
        vaddr: rsp.framebuffers.address.as_ptr().unwrap(),
        px_width,
        px_height,
        bytes_per_px: rsp.framebuffers.bpp as usize / 8,
        pitch: rsp.framebuffers.pitch as usize,
        txt_cur_x: 0,
        txt_cur_y: 0,
        txt_width: px_width / FbContext::FONT_WIDTH,
        txt_height: px_height / FbContext::FONT_HEIGHT,
    });

    let fb = ctx.as_mut().unwrap();
    fb.clear();
}

/// Prints a string onto the framebuffer if any. No-op otherwise
pub fn print(s: &str) {
    let mut ctx = FB_CONTEXT.lock();
    let Some(fb) = ctx.as_mut() else {
        return;
    };

    for c in s.chars() {
        fb.putc(c);
    }
}
