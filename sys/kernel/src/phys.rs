//! Physical Memory Management
//!
//! Parses the memory map given by the bootloader and provides page allocation facilities.

use core::ops::Deref;

use crate::hal::{Paddr, PageSize, KERN_END, KERN_START};
use crate::log;

use spin::mutex::SpinMutex;

/// Instantiation of the Limine request for the memory map
///
/// The bootloader places its answer in this data structure
static MEMMAP: limine::MemmapRequest = limine::MemmapRequest::new(0);

/// Instantiation of the Limine request for the memory map
///
/// The bootloader places its answer in this data structure
static KERN_ADDR: limine::KernelAddressRequest = limine::KernelAddressRequest::new(0);

/// Transforms a reference to an item contained in the kernel image.
/// Usable on addresses contained in `.rodata`, `.text`, `.data` and `.bss`.
pub unsafe fn kpa<T>(t: &T) -> Paddr {
    let va = t as *const T as usize;
    let kern_start = (&KERN_START) as *const _ as usize;
    let kern_end = (&KERN_END) as *const _ as usize;
    assert!(va >= kern_start);
    assert!(va <= kern_end);
    let offset = (va - kern_start) as u64;

    Paddr(KERN_ADDR.get_response().get().unwrap().physical_base + offset)
}

#[cfg(feature = "std")]
#[thread_local]
static MOCK_MEMMAP: SpinMutex<Vec<limine::MemmapEntry>> = SpinMutex::new(Vec::new());

/// Instantiation of the global physical memory manager
#[cfg_attr(feature = "std", thread_local)]
static GLOBAL_PHYSMEM: SpinMutex<PhysMem<1024>> = SpinMutex::new(PhysMem::new());

/// Initialise [`GLOBAL_PHYSMEM`]
///
/// Read the machine memory map and create a local copy we can modify
/// # Safety
///   Unsafe because the caller must ensure this function is called only once
pub unsafe fn init() {
    // When in unit tests, initialise the memory allocator with "a bunch" of pages.
    #[cfg(feature = "std")]
    {
        use libsys::PAGE_SIZE;

        let len = 128 * PAGE_SIZE;
        let layout = std::alloc::Layout::from_size_align(len, PAGE_SIZE).unwrap();
        let mem = std::alloc::alloc(layout);
        assert!(!mem.is_null());
        MOCK_MEMMAP.lock().push(limine::MemmapEntry {
            base: mem as u64,
            len: len as u64,
            typ: limine::MemoryMapEntryType::Usable,
        });
    }
    GLOBAL_PHYSMEM.lock().init();
}

/// Allocate a suitably aligned page from the global page allocator
pub fn alloc(ps: PageSize) -> Option<Paddr> {
    GLOBAL_PHYSMEM.lock().alloc(ps)
}

/// Memory type of a physical range
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
#[cfg_attr(kani, derive(kani::Arbitrary))]
#[repr(C)]
pub enum PhysType {
    /// Conventional main memory we can use as-is
    Usable,
    /// Reserved memory. Could be emulated IO or just Firmware-controlled. Don't touch unless
    /// something (say, ACPI) tells us that there's something of interest inside.
    Reserved,
    /// Essentially ACPI tables. While it's technically possible to reclaim them after parsing it's
    /// a terrible idea because (a) they're not that big and (b) buggy firmware implementation just
    /// assume they are always there.
    AcpiReclaimable,
    /// ACPI Non-volatile-storagey
    AcpiNvs,
    /// Memory identified to misbehave by firmware. We warn and leave it alone
    BadMemory,
    /// Memory we can collect back after setting up our own page tables.
    BootloaderReclaimable,
    /// As the name implies, kernel and modules without the sustaining page tables (those are in
    /// [`Self::BootloaderReclaimable`]
    KernelAndModules,
    /// Range with the framebuffer VRAM
    Framebuffer,
}

impl core::fmt::Display for PhysType {
    /// A formatter so that the printed memory map is easier to digest
    fn fmt(&self, f: &mut core::fmt::Formatter) -> core::fmt::Result {
        match self {
            Self::Usable => write!(f, "U------ {:?}", self),
            Self::Reserved => write!(f, "-R----- {:?}", self),
            Self::AcpiReclaimable | Self::AcpiNvs => write!(f, "--A---- {:?}", self),
            Self::BadMemory => write!(f, "---B--- {:?}", self),
            Self::BootloaderReclaimable => write!(f, "----X-- {:?}", self),
            Self::KernelAndModules => write!(f, "-----K- {:?}", self),
            Self::Framebuffer => write!(f, "------F {:?}", self),
        }
    }
}

/// The use we're giving to a particular memory region
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
#[cfg_attr(kani, derive(kani::Arbitrary))]
#[repr(C)]
pub enum PhysUsage {
    /// Used in the kernel heap as either metadata, page tables or raw frame
    Used,
    /// Up for grabs
    Free,
    /// Not meant for allocation
    Rsvd,
}

/// Definition of a physical range of memory
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
#[repr(C)]
pub struct PhysRange {
    /// Base address of the range. Must be page-aligned
    pub start: u64,
    /// End address of the range. Must be page-aligned
    ///
    /// Note this is _not_ the last page in the range, but one page after that, so `end - start` is
    /// the range length.
    pub end: u64,
    /// Memory type of this range
    pub typ: PhysType,
    /// Use of this range. Basically its allocation state
    pub usage: PhysUsage,
}

#[cfg(kani)]
impl kani::Arbitrary for PhysRange {
    fn any() -> Self {
        let start = kani::any();
        let end = kani::any();
        kani::assume(start & PageSize::Normal.mask() == 0);
        kani::assume(end & PageSize::Normal.mask() == 0);
        kani::assume(start < end);

        let usage = kani::any();
        let typ = kani::any();
        kani::assume(
            (typ == PhysType::Usable && usage != PhysUsage::Rsvd)
                || (typ != PhysType::Usable && usage == PhysUsage::Rsvd),
        );

        Self {
            start,
            end,
            typ,
            usage,
        }
    }
}

impl PhysRange {
    /// Returns true iff a page of the provided size is present in the range. Takes into account
    /// the alignment restrictions of the page
    pub fn can_alloc(&self, ps: PageSize) -> bool {
        if self.typ != PhysType::Usable || self.usage != PhysUsage::Free {
            return false;
        }
        let start = ps.octets() * ((self.start + ps.octets() - 1) / ps.octets());
        let end = start + ps.octets();
        end <= self.end
    }
}

impl From<&limine::MemmapEntry> for PhysRange {
    fn from(entry: &limine::MemmapEntry) -> Self {
        let start = entry.base;
        let end = start + entry.len;
        #[doc(hidden)]
        type T = limine::MemoryMapEntryType;
        let (typ, usage) = match entry.typ {
            T::Usable => (PhysType::Usable, PhysUsage::Free),
            T::Reserved => (PhysType::Reserved, PhysUsage::Rsvd),
            T::AcpiReclaimable => (PhysType::AcpiReclaimable, PhysUsage::Rsvd),
            T::AcpiNvs => (PhysType::AcpiNvs, PhysUsage::Rsvd),
            T::BadMemory => (PhysType::BadMemory, PhysUsage::Rsvd),
            T::BootloaderReclaimable => (PhysType::BootloaderReclaimable, PhysUsage::Rsvd),
            T::KernelAndModules => (PhysType::KernelAndModules, PhysUsage::Rsvd),
            T::Framebuffer => (PhysType::Framebuffer, PhysUsage::Rsvd),
        };
        Self {
            start,
            end,
            typ,
            usage,
        }
    }
}

/// A physical memory manager
///
/// The kernel has a singleton of this type (See [`GLOBAL_PHYSMEM`]) which it keeps around for page
/// allocation and heap management. Note that this is unrelated to the memory management required
/// for userspace. That operates fully within the heap bounds and uses [`crate::mapping`] instead.
#[repr(C)]
pub struct PhysMem<const N: usize>(heapless::Vec<PhysRange, N>);

impl<const N: usize> PhysMem<N> {
    /// Creates a new empty [`PhysMem`] object
    pub const fn new() -> Self {
        Self(heapless::Vec::new())
    }

    /// Creates a new empty [`PhysMem`] object
    pub fn kern_base(&self) -> u64 {
        self.0
            .iter()
            .find(|x| x.typ == PhysType::KernelAndModules)
            .unwrap()
            .start
    }

    /// Populates an _empty_ [`PhysMem`] object with the memory map given by the bootloader. In a
    /// pathological case where the memory map doesn't fit, the last ranges are logged as warnings
    /// (with [`crate::log::PhysRangeDropped`]) and ignored.
    ///
    /// # Panics
    /// Panics if the object is not empty or the bootloader didn't provide a memory map
    pub fn init(&mut self) {
        assert!(self.0.is_empty());
        #[cfg(feature = "std")]
        let memmap = MOCK_MEMMAP.lock();
        #[cfg(not(feature = "std"))]
        let memmap = MEMMAP.get_response().get().unwrap().memmap();

        let mut last_end = 0;
        for region in memmap.iter() {
            let physrange = PhysRange::from(region.deref());

            assert!(last_end <= physrange.start);

            if self.push(physrange).is_ok() {
                if physrange.typ == PhysType::BadMemory {
                    log::PhysRangeAdded(&physrange).warn();
                }
                log::PhysRangeAdded(&physrange).info();
            } else {
                log::PhysRangeDropped(&physrange).warn();
            }
            last_end = physrange.end;
        }
    }

    /// Push a regin into the physical memory allocator
    ///
    /// Ensures that internally adjacent regions with the same type and
    /// usage are merged. It also ensures they are sorted by start address.
    pub fn push(&mut self, range: PhysRange) -> Result<(), PhysRange> {
        // We _may_ be able to merge it, but one entry won't make a
        // whole difference and it's just simpler to give up as it's
        // not expected to happen in practice (and even if it does it's
        // not fatal during boot, where it matters).
        //
        // Also, wellformedness checks
        if self.0.is_full()
            || range.start >= range.end
            || (range.usage == PhysUsage::Rsvd && range.typ == PhysType::Usable)
            || (range.usage != PhysUsage::Rsvd && range.typ != PhysType::Usable)
        {
            return Err(range);
        }

        if let Some(idx) = self.0.iter().position(|x| x.start >= range.end) {
            self.0.insert(idx, range).unwrap();
        } else {
            self.0.push(range).unwrap();
        }
        self.compress();
        Ok(())
    }

    /// Allocate a page
    ///
    /// First-fit algorithm that takes page alignment into account
    pub fn alloc(&mut self, ps: PageSize) -> Option<Paddr> {
        let idx = self.0.iter().enumerate().position(|x| x.1.can_alloc(ps))?;

        // After filtering we got one region we can allocate from. Now it's a matter of being
        // consistent

        // Case 1: Perfect fit. Convert the entry's usage
        if self.0[idx].start == self.0[idx].end - ps.octets() {
            self.0[idx].usage = PhysUsage::Used;
            let ret = Some(Paddr(self.0[idx].start));
            self.compress();
            return ret;
        }

        // Case 2: Bigger region and start address is page aligned.
        //         Resize region and push newly allocated region
        if self.0[idx].start & ps.mask() == 0 {
            let newregion = PhysRange {
                start: self.0[idx].start,
                end: self.0[idx].start + ps.octets(),
                usage: PhysUsage::Used,
                typ: self.0[idx].typ,
            };

            self.0[idx].start += ps.octets();
            self.0.insert(idx, newregion).unwrap();
            self.compress();
            return Some(Paddr(newregion.start));
        }

        // Case 3: Bigger region and end address is page aligned.
        //         Resize region and push newly allocated region
        if self.0[idx].end & ps.mask() == 0 {
            let newregion = PhysRange {
                start: self.0[idx].end - ps.octets(),
                end: self.0[idx].end,
                usage: PhysUsage::Used,
                typ: self.0[idx].typ,
            };

            self.0[idx].end -= ps.octets();
            self.0.insert(idx + 1, newregion).unwrap();
            self.compress();
            return Some(Paddr(newregion.start));
        }

        // Case 4: Bigger region and the page is in the middle (ugh...)
        //         Split region in 3, and push 2
        let head = PhysRange {
            start: self.0[idx].start,
            end: ps.octets() * ((self.0[idx].start + ps.octets() - 1) / ps.octets()),
            usage: self.0[idx].usage,
            typ: self.0[idx].typ,
        };
        let tail = PhysRange {
            start: head.end + ps.octets(),
            end: self.0[idx].end,
            usage: self.0[idx].usage,
            typ: self.0[idx].typ,
        };

        self.0[idx].start = head.end;
        self.0[idx].end = tail.start;
        self.0[idx].usage = PhysUsage::Used;
        self.0.insert(idx, head).unwrap();
        self.0.insert(idx + 2, tail).unwrap();
        self.compress();
        Some(Paddr(head.end))
    }

    /// Sort entries and merge them, compacting the array
    ///
    /// This is meant to be invoked after every allocation, in order to avoid fragmentation
    fn compress(&mut self) {
        for i in 1..self.0.len() {
            if self.0[i - 1].typ == self.0[i].typ
                && self.0[i - 1].usage == self.0[i].usage
                && self.0[i - 1].end == self.0[i].start
            {
                self.0[i].start = self.0[i - 1].start;
                self.0[i - 1].end = self.0[i - 1].start;
            }
        }
        self.0.retain(|x| x.start != x.end);
    }
}

#[cfg(kani)]
mod proofs {
    use super::*;

    #[kani::proof]
    #[kani::unwind(3)]
    fn pushes_merge() {
        let mut physmem = PhysMem::<2>::new();
        assert!(physmem.0.is_empty());

        // Make 2 non-overlapping ranges
        let range1: PhysRange = kani::any();
        let range2: PhysRange = kani::any();
        kani::assume(range1.end <= range2.start);

        if kani::any() {
            physmem.push(range1).unwrap();
            physmem.push(range2).unwrap();
        } else {
            physmem.push(range2).unwrap();
            physmem.push(range1).unwrap();
        }

        if range1.end == range2.start && range1.typ == range2.typ && range1.usage == range2.usage {
            assert_eq!(physmem.0.len(), 1);
            assert_eq!(physmem.0[0].start, range1.start);
            assert_eq!(physmem.0[0].end, range2.end);
        } else {
            assert_eq!(physmem.0.len(), 2);
            assert_eq!(physmem.0[0], range1);
            assert_eq!(physmem.0[1], range2);
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const PAGE_SIZE: u64 = libsys::PAGE_SIZE as u64;

    fn region(start: u64, end: u64) -> PhysRange {
        PhysRange {
            start,
            end,
            typ: PhysType::Usable,
            usage: PhysUsage::Free,
        }
    }

    #[test]
    fn cant_alloc_on_empty() {
        assert!(GLOBAL_PHYSMEM.lock().alloc(PageSize::Normal).is_none());
    }

    #[test]
    fn can_allocate_two_but_not_more() {
        GLOBAL_PHYSMEM
            .lock()
            .push(region(PAGE_SIZE, 3 * PAGE_SIZE))
            .unwrap();
        assert_eq!(GLOBAL_PHYSMEM.lock().0.len(), 1);
        assert_eq!(alloc(PageSize::Normal).unwrap().0, PAGE_SIZE);
        assert_eq!(GLOBAL_PHYSMEM.lock().0.len(), 2); // Region split due to usages
        assert_eq!(alloc(PageSize::Normal).unwrap().0, 2 * PAGE_SIZE);
        assert_eq!(GLOBAL_PHYSMEM.lock().0.len(), 1); // Merging
        assert!(alloc(PageSize::Normal).is_none());
    }

    #[test]
    fn can_allocate_superpage_in_middle() {
        GLOBAL_PHYSMEM
            .lock()
            .push(region(
                PageSize::Super.octets() - PAGE_SIZE,
                2 * PageSize::Super.octets() + PAGE_SIZE,
            ))
            .unwrap();
        assert_eq!(GLOBAL_PHYSMEM.lock().0.len(), 1);
        assert_eq!(alloc(PageSize::Super).unwrap().0, PageSize::Super.octets());
        assert_eq!(GLOBAL_PHYSMEM.lock().0.len(), 3); // Region split in 3 due to alignment
        assert_eq!(
            alloc(PageSize::Normal).unwrap().0,
            PageSize::Super.octets() - PAGE_SIZE
        );
        assert_eq!(GLOBAL_PHYSMEM.lock().0.len(), 2); // Merging
        assert_eq!(
            alloc(PageSize::Normal).unwrap().0,
            2 * PageSize::Super.octets()
        );
        assert_eq!(GLOBAL_PHYSMEM.lock().0.len(), 1); // Merging
        assert!(alloc(PageSize::Normal).is_none());
    }

    #[test]
    fn can_allocate_superpage_on_top() {
        GLOBAL_PHYSMEM
            .lock()
            .push(region(
                PageSize::Super.octets() - PAGE_SIZE,
                2 * PageSize::Super.octets(),
            ))
            .unwrap();
        assert_eq!(GLOBAL_PHYSMEM.lock().0.len(), 1);
        assert_eq!(alloc(PageSize::Super).unwrap().0, PageSize::Super.octets());
        assert_eq!(GLOBAL_PHYSMEM.lock().0.len(), 2); // Region split due to alignment
        assert_eq!(
            alloc(PageSize::Normal).unwrap().0,
            PageSize::Super.octets() - PAGE_SIZE
        );
        assert_eq!(GLOBAL_PHYSMEM.lock().0.len(), 1); // Merging
    }

    #[test]
    fn can_allocate_superpage_on_bottom() {
        GLOBAL_PHYSMEM
            .lock()
            .push(region(
                PageSize::Super.octets(),
                2 * PageSize::Super.octets() + PAGE_SIZE,
            ))
            .unwrap();
        assert_eq!(GLOBAL_PHYSMEM.lock().0.len(), 1);
        assert_eq!(alloc(PageSize::Super).unwrap().0, PageSize::Super.octets());
        assert_eq!(GLOBAL_PHYSMEM.lock().0.len(), 2); // Region split due to alignment
        assert_eq!(
            alloc(PageSize::Normal).unwrap().0,
            2 * PageSize::Super.octets()
        );
        assert_eq!(GLOBAL_PHYSMEM.lock().0.len(), 1); // Merging
    }

    #[test]
    fn can_merge_in_the_middle_on_push() {
        GLOBAL_PHYSMEM
            .lock()
            .push(region(
                PageSize::Normal.octets(),
                2 * PageSize::Normal.octets(),
            ))
            .unwrap();
        GLOBAL_PHYSMEM
            .lock()
            .push(region(
                3 * PageSize::Normal.octets(),
                4 * PageSize::Normal.octets(),
            ))
            .unwrap();
        GLOBAL_PHYSMEM
            .lock()
            .push(region(
                2 * PageSize::Normal.octets(),
                3 * PageSize::Normal.octets(),
            ))
            .unwrap();
        assert_eq!(GLOBAL_PHYSMEM.lock().0.len(), 1);
    }

    #[test]
    fn cant_push_after_full() {
        let mut physmem = PhysMem::<1>::new();
        physmem
            .push(region(
                2 * PageSize::Normal.octets(),
                3 * PageSize::Normal.octets(),
            ))
            .unwrap();
        let _ = physmem
            .push(region(0, PageSize::Normal.octets()))
            .unwrap_err();
    }

    #[test]
    fn tests_init_with_single_good_region() {
        MOCK_MEMMAP.lock().push(limine::MemmapEntry {
            base: PageSize::Normal.octets(),
            len: PageSize::Normal.octets(),
            typ: limine::MemoryMapEntryType::Usable,
        });
        GLOBAL_PHYSMEM.lock().init();
        assert_eq!(GLOBAL_PHYSMEM.lock().0.len(), 1);
    }

    #[test]
    fn tests_init_with_bad_memory() {
        MOCK_MEMMAP.lock().push(limine::MemmapEntry {
            base: PageSize::Normal.octets(),
            len: PageSize::Normal.octets(),
            typ: limine::MemoryMapEntryType::BadMemory,
        });
        GLOBAL_PHYSMEM.lock().init();
        assert_eq!(GLOBAL_PHYSMEM.lock().0.len(), 1);
    }

    #[test]
    fn tests_init_with_malformed_region() {
        MOCK_MEMMAP.lock().push(limine::MemmapEntry {
            base: PageSize::Normal.octets(),
            len: 0,
            typ: limine::MemoryMapEntryType::Framebuffer,
        });
        GLOBAL_PHYSMEM.lock().init();
        assert_eq!(GLOBAL_PHYSMEM.lock().0.len(), 0);
    }

    #[test]
    #[should_panic]
    fn tests_init_with_regions_out_of_order() {
        MOCK_MEMMAP.lock().push(limine::MemmapEntry {
            base: 2 * PageSize::Normal.octets(),
            len: PageSize::Normal.octets(),
            typ: limine::MemoryMapEntryType::KernelAndModules,
        });
        MOCK_MEMMAP.lock().push(limine::MemmapEntry {
            base: PageSize::Normal.octets(),
            len: PageSize::Normal.octets(),
            typ: limine::MemoryMapEntryType::BootloaderReclaimable,
        });
        GLOBAL_PHYSMEM.lock().init();
    }
}
