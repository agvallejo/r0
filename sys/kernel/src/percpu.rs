//! SMP and concurrency handling

use core::cell::Cell;
use core::mem::size_of;
use core::sync::atomic::{AtomicU64, Ordering};

use crate::hal::{mm::get_cpu, percpu::PerCpuArch};
use crate::mutex::LockWord;
use crate::ocap::proc::ProcRef;

use libsys::PAGE_SIZE;

/// See [`limine::SmpRequest`].
static SMP_REQUEST: limine::SmpRequest = limine::SmpRequest::new(0);

/// Runtime check on the number of CPUs in the system
pub fn nr_cpus() -> usize {
    SMP_REQUEST.get_response().get().unwrap().cpu_count as usize
}

/// Arch-independent identifier of each CPU
#[derive(Copy, Clone)]
pub struct CpuId(pub u16);

/// per-CPU page. It's physically located after the per-CPU stack
#[repr(C)]
pub struct PerCpu {
    /// Current lockword of this CPU
    ///
    /// Encodes:
    ///   * Transaction ID: `[0:32]`
    ///   * CPU ID: `[48:64]`
    pub lockword: AtomicU64,

    /// Set by _other_ CPUs when they want this CPU to abort its current
    /// transaction. It's a deadlock avoidance mechanism. This CPU is meant
    /// to compare this field with its own lockword upon failing to acquire
    /// a lockword-based mutex.
    pub defer_lockword: AtomicU64,

    /// Arch-specific portion of the per-CPU area
    pub arch: PerCpuArch,

    /// Domain currently scheduled on this CPU. When `None` the
    /// scheduler is meant to find a new one of the way out.
    pub cur: Cell<Option<ProcRef>>,

    /// Padding to make the whole area page-sized
    _pad: [u8; PAGE_SIZE - size_of::<PerCpuArch>() - 40],
}

#[doc(hidden)]
const _: [u8; PAGE_SIZE] = [0; core::mem::size_of::<PerCpu>()];

impl PerCpu {
    /// Initialises the per-CPU area of a single CPU. Padding is ignored.
    pub unsafe fn init(&self, cpu_id: CpuId, info: &limine::SmpInfo) {
        self.lockword
            .store(LockWord::new(cpu_id).0, Ordering::Relaxed);
        self.cur.set(None);
        self.arch.init(info);
    }
}

/// Initialises the per-CPU structures
pub unsafe fn init() {
    let rsp = SMP_REQUEST.get_response().get_mut().unwrap();
    for (idx, cpu) in rsp.cpus().iter_mut().enumerate() {
        let cpu_id = CpuId(idx.try_into().unwrap());
        assert_eq!(idx, cpu.processor_id as usize);
        get_cpu(cpu_id).init(cpu_id, cpu);
    }
}
