//! Capability manipulation
//!
//! Module in charge of capability manipulation. Preparations, walks, etc.

use core::cell::Cell;

use libsys::base::InvType;
use libsys::ocap::{Cap, CapType, CAP_RESTR_RO, CAP_RESTR_WK};
use libsys::GPT_SIZE;

use crate::global::HeapRef;
use crate::hal::mm::this_cpu;
use crate::hal::pio::invoke_io;
use crate::ocap::gpt::{GptId, GptRef};
use crate::ocap::ote::OteId;
use crate::ocap::page::{PageId, PageRef};
use crate::ocap::proc::{ProcId, ProcRef};
use crate::ocap::{console, qemu};
use crate::{log, sched};

use super::{asyncsig, proc};

/// A single step in a capability walk.
#[derive(Debug)]
pub struct CapWalkStep {
    /// The GPT traversed.
    pub target: GptRef,
    /// Traversed slot of `target`.
    pub slot: u32,
    /// Restrictions so far in the walk.
    pub restr: u32,
}

/// A (heavy) representation of a capability walk. Used during page faults to
/// determine whether a memory access was valid or not.
#[derive(Debug)]
pub struct CapWalk {
    /// List of steps taken during the walk.
    pub step: heapless::Vec<CapWalkStep, 32>,
    /// Leaf capability reached at the end of the walk.
    pub leaf: *const Cell<Cap>,
    /// Overall restrictions involving the leaf.
    pub restr: u32,
}

impl CapWalk {
    /// Perform a capability walk starting at `cellcap` for the address `addr`.
    pub fn new(mut cellcap: &Cell<Cap>, mut addr: u64) -> Option<Self> {
        let mut walk = CapWalk {
            step: heapless::Vec::new(),
            leaf: core::ptr::null(),
            restr: 0,
        };

        // TODO: Calculate producers here. It's cheap and simplifies the page
        //       fault handler, which is complicated enough as-is.

        for _ in 0..walk.step.capacity() {
            cellcap.prepare().ok()?;
            let cap = cellcap.get();
            walk.restr |= u32::from(cap.restr()) & CAP_RESTR_WK;

            if cap.typ() == CapType::Gpt {
                let target = GptRef::new(GptId(cap.oid_hi));
                let target_slot_bitsz = target.get().mem.slot_bitsz.get();

                let slot = addr >> target_slot_bitsz;
                if slot > GPT_SIZE as u64 {
                    // Non-empty guard
                    return None;
                }
                log::StepInCapWalk {
                    gpt: target.objid,
                    slot: slot as usize,
                    cap,
                }
                .debug();

                walk.step
                    .push(CapWalkStep {
                        target,
                        restr: cap.restr().into(),
                        slot: slot as u32,
                    })
                    .ok()?; // Can't fail

                addr &= (1 << target_slot_bitsz) - 1;
                cellcap = &target.get().cap[slot as usize];

                continue;
            }

            // The leaf!
            walk.leaf = cellcap;
            if walk.restr & CAP_RESTR_WK != 0 {
                walk.restr |= CAP_RESTR_RO;
            }

            return Some(walk);
        }
        None
    }
}

/// Augmented kernel-only interface to the capability layout exposed in `libsys`
pub trait KernCap {
    /// Prepares a capability. Leaves target object locked.
    fn prepare(&self) -> Result<(), ()>;

    /// Deprepares a capability
    fn deprepare(&self);

    /// Turn this cap into the null capability
    fn nullify(&self);
}

impl KernCap for Cell<Cap> {
    fn nullify(&self) {
        self.set(Cap {
            bitfield: 0,
            payload: 0,
            oid_lo: 0,
            oid_hi: 0,
        });
        debug_assert_eq!(self.get().typ(), CapType::Null);
    }

    fn deprepare(&self) {
        let mut cap = self.get();

        if !cap.prepared() {
            return;
        }

        let ote = OteId(cap.oid_lo).into_ote();
        if ote.destroyed() {
            // Was stale. This object is dead, so this is actually a null
            // capability. This is logically a `success` case.
            self.nullify();
            return;
        }

        let oid = ote.oid();
        cap.oid_lo = oid as u32;
        cap.oid_hi = (oid >> 32) as u32;
        cap.clear_prepared();
        self.set(cap);
    }

    fn prepare(&self) -> Result<(), ()> {
        let mut cap = self.get();

        if cap.prepared() {
            let hdr = match cap.typ() {
                CapType::Start | CapType::Resume | CapType::AsyncSignal => {
                    &ProcRef::new(ProcId(cap.oid_hi)).get().hdr
                }
                CapType::Gpt => &GptRef::new(GptId(cap.oid_hi)).get().mem.hdr,
                CapType::Page => &PageRef::new(PageId(cap.oid_hi)).get().mem.hdr,
                CapType::Null | CapType::Console | CapType::Qemu | CapType::IoCtl => return Ok(()),
            };

            hdr.lock.try_lock()?;

            let oteid = OteId(cap.oid_lo);
            if hdr.ote.get() == oteid {
                return Ok(());
            }

            self.deprepare(); // Stale. Deprepare and try again
            cap = self.get();
        }

        let oid = (u64::from(cap.oid_hi) << 32) | u64::from(cap.oid_lo);
        let (objid, hdr) = match cap.typ() {
            CapType::Start | CapType::Resume | CapType::AsyncSignal => {
                let Ok(proc) = ProcRef::lookup(oid) else {
                    // FIXME: Go look in the SLS if not in the cache
                    self.nullify();
                    return Ok(());
                };
                (proc.objid.0, &proc.get().hdr)
            }
            CapType::Gpt => {
                let Ok(gpt) = GptRef::lookup(oid) else {
                    // FIXME: Go look in the SLS if not in the cache
                    self.nullify();
                    return Ok(());
                };
                (gpt.objid.0, &gpt.get().mem.hdr)
            }
            CapType::Page => {
                let Ok(page) = PageRef::lookup(oid) else {
                    // FIXME: Go look in the SLS if not in the cache
                    self.nullify();
                    return Ok(());
                };
                (page.objid.0, &page.get().mem.hdr)
            }
            CapType::Null | CapType::Console | CapType::Qemu | CapType::IoCtl => return Ok(()),
        };

        hdr.lock.try_lock()?;

        cap.oid_lo = hdr.ote.get().0;
        cap.oid_hi = objid;
        cap.set_prepared();
        self.set(cap);
        Ok(())
    }
}

/// Invokes a yet-to-be-seen capability
///
/// This function performs basic sanity checks and forwards the message to the
/// type-specific handler.
pub fn invoke() {
    // If we got here, there _IS_ a scheduled process
    let proc = this_cpu().cur.get().unwrap().get();

    // The syscall block is directly mapped over register state, so copy the
    // control word out to manipulate it more conveniently.
    let sysblk = proc.arch.syscall_block();
    let wctl = sysblk.wctl.get();

    let Some(invtype) = wctl.invtype() else {
        todo!("bad invtype. Fault to keeper");
    };

    let invcap = match wctl.invcap().0 {
        0 => {
            // TODO: This is a hack to allow IPC early. Allow invoking DK(0)
            assert_eq!(invtype, InvType::Return);
            sched::ret(None, proc);
        }
        x => &proc.cap[x as usize],
    };
    if invcap.prepare().is_err() {
        todo!("cap_prepare failure");
    }
    let invcap = invcap.get(); // Shadow the cell with its content

    let mut incaps: [Option<&Cell<Cap>>; 4] = [None; 4];
    for (i, incap) in incaps.iter_mut().enumerate() {
        if let Some(slot) = wctl.cap_in(i) {
            *incap = Some(&proc.cap[slot.0 as usize]);
        }
    }

    let mut outcaps: [Option<&Cell<Cap>>; 4] = [None; 4];
    for (i, outcap) in outcaps.iter_mut().enumerate() {
        if let Some(slot) = wctl.cap_out(i) {
            *outcap = Some(&proc.cap[slot.0 as usize]);
        }
    }

    match invcap.typ() {
        CapType::Start | CapType::Resume => proc::invoke(sysblk, invtype, invcap, &incaps),
        CapType::AsyncSignal => asyncsig::invoke(sysblk, invcap),
        CapType::Qemu => qemu::invoke(sysblk),
        CapType::Console => console::invoke(sysblk),
        CapType::Gpt => todo!("notdone gpt invocation"),
        CapType::Page => todo!("notdone page invocation"),
        CapType::Null => todo!("notdone null invocation {}", wctl.invcap().0),
        #[cfg(target_arch = "x86_64")]
        CapType::IoCtl => invoke_io(invcap, sysblk),
    }
}
