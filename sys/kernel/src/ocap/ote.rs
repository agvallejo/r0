//! Object Table
//!
//! This is a terrible misnomer inherited from Coyotos. The name will stay
//! for the time being, but I'll rename it as soon as I can think of better
//! one.
//!
//! The `Object Table` (OT) is neither a table of of [`super::Header`]s nor
//! an indirection table like those equally named of Smalltalk. This table
//! provides book-keeping between prepared [`libsys::ocap::Cap`] and the objects
//! they point to. The way this is done is somewhat strange.
//!
//! TODO: Go on

use core::{
    mem::size_of,
    sync::atomic::{AtomicU32, Ordering},
};

use crate::hal::mm::{OTE_END, OTE_START};
use crate::hal::PageSize;
use crate::{global::OBJCACHE, log};

use spin::mutex::SpinMutex;

/// Index of an OTE in the Object Table
#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub struct OteId(pub u32);

/// Head of the linked list of empty OTEs
#[cfg_attr(feature = "std", thread_local)]
static OTE_FREELIST: SpinMutex<u32> = SpinMutex::new(u32::MAX);

impl OteId {
    /// Number of bits used for indexing inside a single bundle
    const SUBBUNDLE_BITS: usize = 18;

    /// Allocates a new OTE, driving the garbage collector to make some progress
    pub fn alloc(oid: u64) -> Self {
        let (oteid, ote) = {
            let mut freelist = OTE_FREELIST.lock();
            let oteid = match *freelist {
                u32::MAX => panic!("ote_starvation"),
                x => OteId(x),
            };
            let ote = oteid.into_ote();
            *freelist = ote.oid[0].load(Ordering::Relaxed);
            (oteid, ote)
        };

        ote.oid[0].store(oid as u32, Ordering::Relaxed);
        ote.oid[1].store((oid >> 32) as u32, Ordering::Relaxed);
        ote.flags
            .fetch_and(!(OTE_FLAG_FREE | OTE_FLAG_DESTROYED), Ordering::Relaxed);

        oteid
    }

    /// Synthesise a new [`Self`] from `bundle_id` + `subbundle_id`
    fn new(bundle_id: u32, subbundle_id: u32) -> Self {
        let ret = Self((bundle_id << Self::SUBBUNDLE_BITS) | subbundle_id);
        assert_eq!(ret.bundle_id(), bundle_id as usize);
        assert_eq!(ret.subbundle_id(), subbundle_id as usize);
        ret
    }

    /// Return this OTE to [`OTE_FREELIST`]
    pub fn free(self) {
        let ote = self.into_ote();

        let mut freelist = OTE_FREELIST.lock();
        ote.oid[0].store(*freelist, Ordering::Relaxed);
        *freelist = self.0;
    }

    /// Which bundle contains the page table of this index
    fn bundle_id(self) -> usize {
        self.0 as usize >> Self::SUBBUNDLE_BITS
    }

    /// Which item inside the bundle contains the page table of this index
    fn subbundle_id(self) -> usize {
        self.0 as usize & ((1 << Self::SUBBUNDLE_BITS) - 1)
    }

    /// "Dereferences" the index and fetches its associated OTE
    pub fn into_ote<'a>(self) -> &'a Ote {
        let cache = unsafe { &*OBJCACHE.get() };
        &cache.ote[self.bundle_id()].tab[self.subbundle_id()]
    }
}

/// An element of the object table. Represents a relationship between a
/// capability marked prepared and the object allegedly being pointed to
///
/// # Note
///   The OID is split in 2 so that the alignment requirements drop to 4
///   and the structures can be packed in 12 octets without padding.
#[repr(C)]
pub struct Ote {
    /// OID of the object (split to allow 4-octet alignment)
    oid: [AtomicU32; 2],
    /// Various flags. See [`OTE_FLAG_FREE`], [`OTE_FLAG_MARK`] and
    /// [`OTE_FLAG_DESTROYED`]. Other bits must be zero.
    flags: AtomicU32,
}

impl Ote {
    /// Extract the OID of the object from the OTE
    pub fn oid(&self) -> u64 {
        // No particular synchronization is needed because the only
        // concurrency happens during cap prepare and those are exlucisvely
        // read operations
        self.oid[0].load(Ordering::Relaxed) as u64
            | (self.oid[1].load(Ordering::Relaxed) as u64) << 32
    }

    /// Returns `true` if the object this OTE referred to has been destroyed
    pub fn destroyed(&self) -> bool {
        // No particular synchronization is needed because the only
        // concurrency happens during cap prepare and those are exlucisvely
        // read operations
        self.flags.load(Ordering::Relaxed) & OTE_FLAG_DESTROYED != 0
    }

    /// Marks the OTE as destroyed
    pub fn set_destroyed(&self) {
        // No particular synchronization is needed because the only
        // concurrency happens during cap prepare and those are exlucisvely
        // read operations
        self.flags.fetch_or(OTE_FLAG_DESTROYED, Ordering::Relaxed);
    }
}

/// A flag set by the OTE garbage collector. If the mark is still there
/// after a full pass, the OTE is guaranteed to be an orphan and can be
/// freed.
const OTE_FLAG_MARK: u32 = 1;

/// A flag set when an object is destroyed. This bit can be checked by
/// a failed capability prepare in order to determine whether the
/// capability should be rewritten as invalid or not. This is not required
/// for correctness (we could simply deprepare and look for an object with
/// a matching OID, and then fail on `alloc_count`), but it's helpful to
/// easily short-circuit a useless object lookup.
const OTE_FLAG_DESTROYED: u32 = 2;

/// A flag set when the OTE is not in use and is in the freelist
const OTE_FLAG_FREE: u32 = 2;

/// Number of OTE entries on each bundle
const ITEMS_PER_BUNDLE: usize = PageSize::Super.octets() as usize / size_of::<Ote>();

/// A _bundle_ of depend table entries. This is simply as many dep entries
/// as we can fit in a superpage
#[repr(C)]
pub struct OteBundle {
    /// Linear array of OTEs
    tab: [Ote; ITEMS_PER_BUNDLE],
    /// Unused
    _pad: u64,
}

#[doc(hidden)]
const _: [u8; PageSize::Super.octets() as usize] = [0; size_of::<OteBundle>()];

/// Initialises the Object Table
pub unsafe fn init() {
    assert!(OteId(u32::MAX).subbundle_id() >= ITEMS_PER_BUNDLE - 1);

    let cache = OBJCACHE.get();
    (*cache).ote = core::slice::from_raw_parts(OTE_START as *const OteBundle, 1);

    for (bundle_id, b) in (*cache).ote.iter().enumerate() {
        for (subbundle_id, _) in b.tab.iter().enumerate() {
            OteId::new(
                bundle_id.try_into().unwrap(),
                subbundle_id.try_into().unwrap(),
            )
            .free();
        }
    }

    log::HeapVectorAdded {
        name: "ote ",
        nitems: ITEMS_PER_BUNDLE,
        start: OTE_START,
        end: OTE_START + PageSize::Super.octets() as usize,
        max: OTE_END,
    }
    .info();
}
