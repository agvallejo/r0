//! GPT Objects
//!
//! This module handles the GPTs objects in the kernel heap.
use core::cell::Cell;
use core::iter::zip;
use core::mem::size_of;
use core::sync::atomic::Ordering;

use libsys::sysimg::SerialisedGpt;
use libsys::GPT_BITS;
use libsys::{ocap::Cap, GPT_SIZE};

use crate::global::{HeapId, HeapRef, ObjRef, OBJCACHE};
use crate::hal::mm::{GPT_END, GPT_START};
use crate::hal::PageSize;
use crate::{log, ocap};

use super::ObjType;

/// A generic type for indexing the array of GPTs in the kernel heap
///
/// The existence of such an index means the location exists. This is
/// effectively a 32bit shared reference, and it's the sort of pointer
/// embedded in capabilities and such.
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct GptId(pub u32);

impl HeapId for GptId {}

/// A [`Gpt`] specialization of [`ObjRef`]
pub type GptRef = ObjRef<GptId, Gpt>;

impl HeapRef for GptRef {
    type ObjId = GptId;
    type ObjType = Gpt;
    const BASE: *mut Self::ObjType = GPT_START as *mut _;

    fn new(objid: Self::ObjId) -> Self {
        Self {
            objid,
            objref: unsafe { &*Self::BASE.add(objid.0 as usize) },
        }
    }

    fn lookup(oid: u64) -> Result<Self, super::Error> {
        let nr_superpages = unsafe { (*OBJCACHE.get()).gpt_nr_superpages };
        let nr_items = (nr_superpages * PageSize::Super.octets() as u32) / size_of::<Gpt>() as u32;
        for i in 0..nr_items {
            let gpt = GptRef::new(GptId(i));
            let this_oid = gpt.get().mem.hdr.oid.load(Ordering::Relaxed);
            if this_oid == oid {
                if gpt.get().mem.hdr.lock.try_lock().is_err() {
                    return Err(super::Error::Deferred);
                }
                if oid == gpt.get().mem.hdr.oid.load(Ordering::Relaxed) {
                    // We must check again to avoid TOCTOU. This is ok
                    return Ok(gpt);
                }
            }
        }
        Err(super::Error::NotFound)
    }
}

/// Header for a process object type
#[repr(C)]
#[derive(Debug)]
pub struct Gpt {
    /// Object header common to every memory object
    pub mem: ocap::MemHeader,
    /// Array of slots of the GPT.
    pub cap: [Cell<Cap>; GPT_SIZE],
}

/// Process is sync, because access is synchronized thorugh it's built-in lock
unsafe impl Sync for Gpt {}

impl Gpt {
    /// Initialise this GPT as "unused". That is, oid = alloc_count = 0.
    pub unsafe fn init(&mut self, objidx: u32) {
        *self = core::mem::zeroed();
        self.mem.hdr.init(objidx, ObjType::Gpt);
    }

    /// Initialise this GPT from a serialised GPT in _sysimg_.
    pub unsafe fn init_from_serialised(&self, oid: u64, gpt: &SerialisedGpt) {
        assert!(gpt.slot_bitsz > 0);
        assert!(gpt.slot_bitsz < 64);
        self.mem.hdr.init_from_serialised(oid);
        self.mem.init();
        self.mem
            .slot_bitsz
            .set(u8::try_from(gpt.slot_bitsz).unwrap());
        self.mem
            .bitsz
            .set(self.mem.slot_bitsz.get() + GPT_BITS as u8);
        for (dst, src) in zip(self.cap.iter(), gpt.cap.iter()) {
            dst.set(*src);
        }
    }
}

/// Initialise the GPT vector in the global heap
pub unsafe fn init() {
    let nr_items = PageSize::Super.octets() as usize / size_of::<Gpt>();
    (*OBJCACHE.get()).gpt_nr_superpages = 1;
    (*OBJCACHE.get()).gpt = core::slice::from_raw_parts(GptRef::BASE, nr_items);

    for i in 0..nr_items {
        (*GptRef::BASE.add(i)).init(i as u32);
    }

    log::HeapVectorAdded {
        name: "gpt ",
        nitems: nr_items,
        start: GPT_START,
        end: GPT_START + PageSize::Super.octets() as usize,
        max: GPT_END,
    }
    .info();
}
