//! Asynchronous inter-process signalling
//!
//! For Xen these are event channels, for seL4 it's Notification Objects and for
//! Coyotos it was an AppNotice. It's all the same; something akin to an IPI but
//! between processes.

use libsys::ocap::Cap;

use crate::{
    global::HeapRef,
    hal::{mm::this_cpu, proc::SyscallBlock},
    ocap::proc::{ProcId, ProcRef},
    sched,
};

use super::proc::ProcState;

/// Handles the invocation of an AsyncSignal capability
pub fn invoke(sysblk: &SyscallBlock, invcap: Cap) {
    // invcap is already prepared, and the object locked
    let to = ProcRef::new(ProcId(invcap.oid_hi)).get();
    let signals = ((sysblk.data[0].get() as u32) & invcap.payload) | to.async_signals.get();

    to.async_signals.set(signals);

    if to.state.get() == ProcState::Available {
        let from = this_cpu().cur.get().unwrap().get();
        sched::fork(to, from);
    }
}
