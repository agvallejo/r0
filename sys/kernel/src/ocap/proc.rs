//! Process Objects
//!
//! This module handles the arch-agnostic side of processes in the kernel heap.
use core::cell::Cell;
use core::iter::zip;
use core::mem::size_of;
use core::sync::atomic::Ordering;

use crate::global::{HeapId, HeapRef, ObjRef, OBJCACHE};
use crate::hal::mm::{this_cpu, PROC_END};
use crate::hal::proc::{ArchProcess, SyscallBlock};
use crate::hal::{mm::PROC_START, PageSize};
use crate::list::{Dhead, Dlist};
use crate::ocap::cap::KernCap;
use crate::{log, sched};

use libsys::base::InvType;
use libsys::ocap::{Cap, CapType, CAP_ALLOC_COUNT_SHIFT, CAP_CAPTYPE_SHIFT, CAP_PREPARED_SHIFT};
use libsys::sysimg::SerialisedProcess;
use libsys::GPT_SIZE;

use super::ObjType;

/// A generic type for indexing the array of Processes in the kernel heap
///
/// The existence of such an index means the location exists. This is
/// effectively a 32bit shared reference, and it's the sort of pointer
/// embedded in capabilities and such.
#[derive(Copy, Clone, Eq, PartialEq)]
pub struct ProcId(pub u32);

impl HeapId for ProcId {}

/// A [`Process`] specialization of [`ObjRef`]
pub type ProcRef = ObjRef<ProcId, Process>;

impl HeapRef for ProcRef {
    type ObjId = ProcId;
    type ObjType = Process;
    const BASE: *mut Self::ObjType = PROC_START as *mut _;

    fn new(objid: Self::ObjId) -> Self {
        Self {
            objid,
            objref: unsafe { &*Self::BASE.add(objid.0 as usize) },
        }
    }

    fn lookup(oid: u64) -> Result<Self, super::Error> {
        let nr_superpages = unsafe { (*OBJCACHE.get()).proc_nr_superpages };
        let nr_items =
            (nr_superpages * PageSize::Super.octets() as u32) / size_of::<Process>() as u32;
        for i in 0..nr_items {
            let proc = ProcRef::new(ProcId(i));
            let this_oid = proc.get().hdr.oid.load(Ordering::Relaxed);
            if this_oid == oid {
                if proc.get().hdr.lock.try_lock().is_err() {
                    return Err(super::Error::Deferred);
                }
                if oid == proc.get().hdr.oid.load(Ordering::Relaxed) {
                    // We must check again to avoid TOCTOU. This is ok
                    return Ok(proc);
                }
            }
        }
        Err(super::Error::NotFound)
    }
}

/// Different states a process might be in.
#[repr(u8)]
#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub enum ProcState {
    /// In an open wait
    Available,
    /// Enqueued _somewhere_. That somewhere may be the ready queue OR any stall
    /// queue (IRQ queue, sleep queue, process blocking queue...)
    Ready,
    /// Physically executing on some CPU.
    Running,
    /// In a closed wait.
    Paused,
    /// Awaiting invocation of its resume capability.
    ///
    /// Note that a thread blocked on another after invoking its Start
    /// capability is in the Ready state instead.
    InClosedWait,
}

/// Header for a process object type
#[repr(C)]
pub struct Process {
    /// Object header common to every object type.
    pub hdr: super::Header,
    /// Anchor for the _curret queue_ this process is in. Could be none.
    pub q_cur: Dlist,
    /// Head of a linked list of processes blocked on this one.
    pub q_blocked: Dhead,
    /// A counter incremented after each `call` operation. Used as a match for
    /// the `alloc_count` field of resume capabilities so they are invalidated
    /// after each return.
    pub call_count: Cell<u32>,
    /// State the process is logically in
    pub state: Cell<ProcState>,
    /// Signals asynchronously delivered to processes
    pub async_signals: Cell<u32>,
    /// Arch-dependent fields.
    pub arch: ArchProcess,
    /// This capability memory tree represents the virtual address space of the process.
    pub addr_space: Cell<Cap>,
    /// Capability registers of the process.
    pub cap: [Cell<Cap>; GPT_SIZE],
}

/// Process is sync, because access is synchronized thorugh it's built-in lock
unsafe impl Sync for Process {}

impl Process {
    /// Initialise this process as "unused". That is, oid = alloc_count = 0.
    pub unsafe fn init(&mut self, objidx: u32) {
        self.hdr = core::mem::zeroed();
        self.hdr.init(objidx, ObjType::Process);
        self.q_cur.init();
        self.q_cur.init();
        self.q_blocked.init();
        self.arch.init();
    }

    /// Initialise this process from a serialised process in _sysimg_.
    pub unsafe fn init_from_serialised(&self, oid: u64, proc: &SerialisedProcess) {
        self.hdr.init_from_serialised(oid);
        self.arch.set_ip(proc.entry);
        self.addr_space.set(proc.addr_space);
        self.state.set(ProcState::Running);
        for (dst, src) in zip(self.cap.iter(), proc.slot.iter()) {
            dst.set(*src);
        }
    }

    /// Create a resume capability to this process on `capcell`
    pub fn create_resume(&self, capcell: &Cell<Cap>) {
        capcell.set(Cap {
            bitfield: ((CapType::Resume as u32) << CAP_CAPTYPE_SHIFT)
                | (self.hdr.alloc_count.get() << CAP_ALLOC_COUNT_SHIFT)
                | (1 << CAP_PREPARED_SHIFT),
            payload: self.call_count.get(),
            oid_lo: self.hdr.ote.get().0,
            oid_hi: self.hdr.objidx,
        });
    }
}

/// Initialise the processes vector in the global heap
pub unsafe fn init() {
    let nr_items = PageSize::Super.octets() as usize / size_of::<Process>();
    (*OBJCACHE.get()).proc_nr_superpages = 1;
    (*OBJCACHE.get()).proc = core::slice::from_raw_parts(ProcRef::BASE, nr_items);

    for i in 0..nr_items {
        (*ProcRef::BASE.add(i)).init(i as u32);
    }

    log::HeapVectorAdded {
        name: "proc",
        nitems: nr_items,
        start: PROC_START,
        end: PROC_START + PageSize::Super.octets() as usize,
        max: PROC_END,
    }
    .info();
}

/// [`CapType::Start`] and [`CapType::Resume`] cap invocation handler.
pub fn invoke(
    sysblk: &SyscallBlock,
    invtype: InvType,
    invcap: Cap,
    incaps: &[Option<&Cell<Cap>>; 4],
) -> ! {
    // invcap is already prepared, and the object locked
    let to = ProcRef::new(ProcId(invcap.oid_hi)).get();
    let from = this_cpu().cur.get().unwrap().get();

    // Block if the destination isn't in an adequate state. Note that this
    // start/resume capability is _valid_. Meaning the resume case is guaranteed
    // to succeed and the state check is only relevant on start caps.
    if invcap.typ() != CapType::Resume && to.state.get() != ProcState::Available {
        sched::block(to, from);
    }

    let to_sysblk = to.arch.syscall_block();
    for (dst, src) in zip(to_sysblk.data.iter(), sysblk.data.iter()) {
        dst.set(src.get());
    }

    let mut outcaps: [Option<&Cell<Cap>>; 4] = [None; 4];
    for (i, outcap) in outcaps.iter_mut().enumerate() {
        if let Some(slot) = to_sysblk.wctl.get().cap_out(i) {
            *outcap = Some(&to.cap[slot.0 as usize]);
        }
    }

    for (dst, src) in zip(outcaps.iter(), incaps.iter()) {
        if let Some(outcap) = dst {
            if let Some(incap) = src {
                outcap.set(incap.get());
            } else {
                outcap.nullify();
            };
        }
    }

    if invcap.typ() == CapType::Resume {
        // Invalidate all resume caps
        to.call_count.set(to.call_count.get() + 1);
        // TODO: deal with overflow sensibly
    }

    match invtype {
        InvType::Call => {
            if outcaps[0].is_some() {
                from.create_resume(outcaps[0].unwrap());
            }
            sched::call(to, from);
        }
        InvType::Return => sched::ret(Some(to), from),
        InvType::Fork => sched::fork(to, from),
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn resume_cap_is_valid() {
        let proc: Process = unsafe { core::mem::zeroed() };
        let cellcap: Cell<Cap> = unsafe { core::mem::zeroed() };
        proc.create_resume(&cellcap);
        assert_eq!(cellcap.get().typ(), CapType::Resume);
    }
}
