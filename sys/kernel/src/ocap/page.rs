//! Page Objects
//!
//! This module handles pages covered by capability discipline. That is, those that have page
//! headers assigned to them. These pages are used as a cache for Single-Level Store pages and
//! virtual address backbone.
use core::cell::Cell;
use core::mem::size_of;
use core::sync::atomic::Ordering;

use crate::global::{HeapId, HeapRef, ObjRef, OBJCACHE};
use crate::hal::mm::{DIRECTMAP_START, PAGE_END, PAGE_START};
use crate::hal::{Paddr, PageSize};
use crate::{log, phys};

use libsys::PAGE_SIZE;

use super::ObjType;

/// A generic type for indexing the array of Processes in the kernel heap
///
/// The existence of such an index means the location exists. This is
/// effectively a 32bit shared reference, and it's the sort of pointer
/// embedded in capabilities and such.
#[derive(Copy, Clone, Eq, PartialEq)]
pub struct PageId(pub u32);

impl HeapId for PageId {}

/// A [`Page`] specialization of [`ObjRef`]
pub type PageRef = ObjRef<PageId, Page>;

impl HeapRef for PageRef {
    type ObjId = PageId;
    type ObjType = Page;
    const BASE: *mut Self::ObjType = PAGE_START as *mut _;

    fn new(objid: Self::ObjId) -> Self {
        Self {
            objid,
            objref: unsafe { &*Self::BASE.add(objid.0 as usize) },
        }
    }

    fn lookup(oid: u64) -> Result<Self, super::Error> {
        let nr_superpages = unsafe { (*OBJCACHE.get()).page_nr_superpages };
        let nr_items = (nr_superpages * PageSize::Super.octets() as u32) / size_of::<Page>() as u32;
        for i in 0..nr_items {
            let page = PageRef::new(PageId(i));
            let this_oid = page.get().mem.hdr.oid.load(Ordering::Relaxed);
            if this_oid == oid {
                if page.get().mem.hdr.lock.try_lock().is_err() {
                    return Err(super::Error::Deferred);
                }
                if oid == page.get().mem.hdr.oid.load(Ordering::Relaxed) {
                    // We must check again to avoid TOCTOU. This is ok
                    return Ok(page);
                }
                // We _COULD_ release this lock... But it's going to be very
                // infrequent, so there's no need.
            }
        }
        Err(super::Error::NotFound)
    }
}

/// Header for a page object type
#[repr(C)]
pub struct Page {
    /// Object header common to every memory object
    pub mem: super::MemHeader,
    /// Physical address of the page frame referenced by this header.
    pub paddr: Cell<Paddr>,
}

/// Process is sync, because access is synchronized thorugh it's built-in lock
unsafe impl Sync for Page {}

impl Page {
    /// Initialise this page header as "unused". That is, oid = alloc_count = 0.
    pub unsafe fn init(&mut self, objidx: u32) {
        *self = core::mem::zeroed();
        self.mem.hdr.init(objidx, ObjType::Page);
        self.paddr.set(phys::alloc(PageSize::Normal).unwrap());
    }

    /// Initialise this page header's frame from a serialised page in _sysimg_.
    pub unsafe fn init_from_serialised(&self, oid: u64, frame: &[u8; PAGE_SIZE]) {
        self.mem.init();
        self.mem.hdr.init_from_serialised(oid);
        self.mem.slot_bitsz.set(0);
        self.mem.bitsz.set(PageSize::Normal.bits() as u8);
        let dst = DIRECTMAP_START + usize::try_from(self.paddr.get().0).unwrap();
        (*(dst as *mut [u8; PAGE_SIZE])).copy_from_slice(frame);
    }
}

/// Initialise the page headers vector in the global heap
pub unsafe fn init() {
    let nr_items = PageSize::Super.octets() as usize / size_of::<Page>();
    (*OBJCACHE.get()).page_nr_superpages = 1;
    (*OBJCACHE.get()).page = core::slice::from_raw_parts(PageRef::BASE, nr_items);

    for i in 0..nr_items {
        (*PageRef::BASE.add(i)).init(i as u32);
    }

    log::HeapVectorAdded {
        name: "page",
        nitems: nr_items,
        start: PAGE_START,
        end: PAGE_START + PageSize::Super.octets() as usize,
        max: PAGE_END,
    }
    .info();
}
