//! Object-Capability Model
//!
//! This models tries to abstract away the concepts of object capabilities so they can be
//! encapsulated by object type. See [`gpt`], [`page`] and [`proc`].
use core::cell::Cell;
use core::sync::atomic::{AtomicU64, Ordering};

use crate::global::SENTINEL;
use crate::hal::mm::map_kernel;
use crate::mapping::ptab::{PtabId, PtabRef, PTAB_LRU};
use crate::mapping::rmap::rm_forget_pt;
use crate::mutex::LwMutex;

use self::ote::OteId;

pub mod asyncsig;
pub mod console;
pub mod gpt;
pub mod page;
pub mod proc;
pub mod qemu;

pub mod cap;
pub mod ote;

/// Generalized error codes for capability manipulation code
pub enum Error {
    /// An object was looked up, and wasn't found
    NotFound,
    /// Some CPU is politely requesting this CPU to get out of the way
    Deferred,
}

/// Enumeration for Object Type
/// A numeric representation of the type of a heap object
#[repr(u8)]
#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub enum ObjType {
    /// See [`gpt::Gpt`]
    Gpt,
    /// See [`page::Page`]
    Page,
    /// See [`proc::Process`]
    Process,
}

/// Common object header. Used by processes, GPTs and page headers.
#[repr(C)]
#[derive(Debug)]
pub struct Header {
    /// Intrusive lock. Using something like `Mutex<T>` is more aggravating
    /// than it is helpful, as there are elements that we want inside to be
    /// modified without taking this lock. i.e: age or queue links, product
    /// chains...
    ///
    /// We could refactor it so those elements are out, but then the object
    /// composition becomes unwieldy and it becomes hard to ensure packing.
    ///
    /// This turns out to be alright. raw references to this header are
    /// only present through `ObjRef`, which can only be created via
    /// locking, so it's less insecure than it might seem.
    pub lock: LwMutex,
    /// 64bit number identifying this object in the Single-Level Store.
    ///
    /// This is atomic because OID lookups avoid taking the locks. This makes
    /// the case of a CPU constantly being deferred while preparing a capability
    /// much more unlikely.
    pub oid: AtomicU64,
    /// 20bit number (top 12 unused) used to revoke capabilities. A capability
    /// to this OID is only valid _iff_ their alloc_count field match too.
    pub alloc_count: Cell<u32>,
    /// Identifier of the OTE associated with this object.
    pub ote: Cell<OteId>,
    /// The upcasted type of this object
    pub objidx: u32,
    /// Set if this page is pinned in memory and cannot move. This is used for
    /// physical pages outside the SLS and pages used for DMA.
    pub pinned: Cell<bool>,
    /// Set if this object has changes wrt. the SLS.
    pub dirty: Cell<bool>,
    /// The upcasted type of this object
    pub objtype: ObjType,
}

#[doc(hidden)]
const _: [u8; 32] = [0; core::mem::size_of::<Header>()];

impl Header {
    /// Initialise a heap object for the first time.
    pub unsafe fn init(&mut self, objidx: u32, objtype: ObjType) {
        self.objtype = objtype;
        self.objidx = objidx;
    }

    /// First-time load of a real object into the object header
    ///
    /// This is used exclusively while loading `sysimg`
    pub unsafe fn init_from_serialised(&self, oid: u64) {
        self.oid.store(oid, Ordering::Relaxed);
        self.ote.set(OteId::alloc(oid));
        self.alloc_count.set(0);
        self.pinned.set(false);
        self.dirty.set(false);
    }
}

/// Common memory object header. Used by GPTs and page headers.
#[repr(C)]
#[derive(Debug)]
pub struct MemHeader {
    /// Header common to every object type
    pub hdr: Header,
    /// Singly linked list of _products_. A product is essentially a page
    /// table from the global pool of page tables.
    pub prod_slist: Cell<PtabId>,
    /// Offset in address decode for this memory object.
    pub bitsz: Cell<u8>,
    /// Hardcoded to `PAGE_BITS` for pages, and `GPT_BITS` for GPTs
    pub slot_bitsz: Cell<u8>,
    /// Unused.
    _pad: [u8; 2],
}

impl MemHeader {
    /// Initialises a new pristine memory header
    pub fn init(&self) {
        self.prod_slist.set(PtabId(SENTINEL));
    }

    /// Create a new product
    pub fn produce(&self, lvl: usize, prod_idx: u8) -> PtabRef {
        let mut ptabref = PtabRef::try_new(self.prod_slist.get());

        while let Some(ptab) = ptabref {
            if ptab.id.lvl() == lvl && ptab.get_header().prod_idx.get() == prod_idx {
                return ptab;
            }
            ptabref = ptab.next_product();
        }
        let ptab = PTAB_LRU.lock()[lvl].alloc();
        assert_ne!(
            ptab.get_header().lru_front.get(),
            ptab.get_header().lru_back.get(),
        );
        let ptab_hdr = ptab.get_header();

        // Add this product to the producer's product chain
        //
        // FIXME: Open-coded slist_t
        ptab_hdr.producer_id.set(self.hdr.objidx);
        ptab_hdr.prod_idx.set(prod_idx);
        ptab_hdr.producer_type.set(self.hdr.objtype);
        ptab_hdr.product_list.set(self.prod_slist.get());
        self.prod_slist.set(ptab.id);

        // We wiped the rmap entries while allocating, but it might itself still
        // have entries pointing somewhere else. We could traverse the depend
        // table to find out, but it's faster to just wipe them all here. Yes,
        // we're leaving orphaned entries, but that's benign.
        for pte in &ptab.get_frame().0 {
            pte.invalidate();
        }

        if lvl == 0 {
            map_kernel(ptab);
        }

        ptab
    }

    /// Destroy every product associated bound to this memory object
    pub fn destroy_products(&self) {
        let _lru = PTAB_LRU.lock();
        let mut ptabref = PtabRef::try_new(self.prod_slist.get());
        while let Some(ptab) = ptabref {
            rm_forget_pt(ptab.id);
            ptab.get_header().producer_id.set(SENTINEL);
            ptabref = ptab.next_product();
        }
    }
}

#[doc(hidden)]
const _: [u8; 40] = [0; core::mem::size_of::<MemHeader>()];
