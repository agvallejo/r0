//! Handling of capability invocations for the QEMU exit device

use crate::hal::{self, proc::SyscallBlock};

/// Handles the invocation of a QEMU capability
pub fn invoke(sysblk: &SyscallBlock) {
    match sysblk.wctl.get().cmd() {
        1 => hal::exit::success(),
        _ => hal::exit::failure(),
    }

    unreachable!();
}
