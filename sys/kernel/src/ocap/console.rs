//! Handling of capability invocations for the kernel console

use crate::{hal::proc::SyscallBlock, log};

/// Handles the invocation of a Console capability
pub fn invoke(sysblk: &SyscallBlock) {
    use core::fmt::Write;
    write!(log::Logger, "{}", sysblk.data[0].get() as u8 as char).unwrap();
}
