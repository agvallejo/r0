//! Interrupt Descriptor Table
//!
//! The IDT is x86's interrupt vector table. Each entry states what the
//! interrupt handler should be for that particular vector. In line with
//! the complexities we love about x86 this is _not_ an array of pointers
//! or instructions, but an array of _interrupt descriptors_. Each
//! descriptor describes how interrupts/exceptions on that vector ought to
//! be handled. i.e: Where the stack is, whether it's a
//! software-triggerable vector, whether interrupts are disabled on entry
//! or not, etc.
//!
//! r0 uses SYSCALL/SYSRET for syscall management, so only the exception
//! handlers need to be present.
use core::sync::atomic::{AtomicU64, Ordering};

use super::{asm, proc::FixRegs};
use crate::hal::pagefault::pagefault_handler;
use crate::hal::proc::FixRegId;
use crate::hal::x86_64::asm::ret_to_userspace;
use crate::{log, syscall::syscall};

/// IDT register
///
/// Describes location and size of the IDT so we can point the HW to it
#[repr(C)]
pub struct Idtr {
    /// Unused padding
    _pad: [u16; 3],
    /// Size of the IDT in octets minus 1
    pub limit: u16,
    /// Virtual address of the IDT
    offset: AtomicU64,
}

/// Instantiation of the IDTR
///
/// This is the register that gets installed in hardware. It's populated
/// after initialisation and immediately loaded in HW
#[cfg_attr(feature = "std", thread_local)]
static IDTR: Idtr = Idtr {
    _pad: [0; 3],
    limit: (16 * IDT_NUM_ENTRIES) as u16 - 1,
    offset: AtomicU64::new(0),
};

/// Interrupt Descriptor Table
///
/// Each entry is u128 in size, but it's more convenient to treat it as
/// separate u64 numbers.
#[cfg_attr(feature = "std", thread_local)]
static IDT: [[AtomicU64; 2]; IDT_NUM_ENTRIES] = unsafe { core::mem::zeroed() };

/// Number of entries in the IDT
const IDT_NUM_ENTRIES: usize = 33;

/// Fill an IDT entry
///
/// These are meant to populate the exception handlers only. ring3 is meant
/// to use SYSCALL to communicate with the kernel
fn set_vector(idte: &[AtomicU64; 2], handler: u64, dpl: u64) {
    #[allow(clippy::identity_op)] // for readability
    idte[0].store(
        (handler & 0xFFFF)
             | (0x8 << 16) // Code selector
             | (0 << 32) // IST
             | (0xE << 40) // Interrupt gate
             | (dpl << 45) // DPL
             | (1 << 47) // Present
             | (((handler >> 16) & 0xFFFF) << 48),
        Ordering::Relaxed,
    );
    idte[1].store(handler >> 32, Ordering::Relaxed);
}

/// Initialise the IDT
pub unsafe fn init() {
    #[cfg(feature = "std")]
    let isr_array = [0; 33];

    #[cfg(not(feature = "std"))]
    let isr_array = [
        &isr_0, &isr_1, &isr_2, &isr_3, &isr_4, &isr_5, &isr_6, &isr_7, &isr_8, &isr_9, &isr_10,
        &isr_11, &isr_12, &isr_13, &isr_14, &isr_15, &isr_16, &isr_17, &isr_18, &isr_19, &isr_20,
        &isr_21, &isr_22, &isr_23, &isr_24, &isr_25, &isr_26, &isr_27, &isr_28, &isr_29, &isr_30,
        &isr_31, &isr_32,
    ];

    let zip = core::iter::zip(IDT.iter(), isr_array.iter());
    for (entry, handler) in zip {
        set_vector(entry, *handler as *const u8 as u64, 0);
    }

    // Override syscall
    set_vector(&IDT[32], *isr_array.last().unwrap() as *const u8 as u64, 3);

    IDTR.offset.store(IDT.as_ptr() as u64, Ordering::Relaxed);

    asm::lidt(&IDTR);

    log::Init("idt_done").info();
}

/// Common ISR handler jumped at from each individual ISR entry stub
#[no_mangle]
pub extern "C" fn isr(regs: &FixRegs) -> ! {
    // The kernel can't be interrupted
    assert_eq!(regs[FixRegId::Cs].get(), 0x1B);

    let isrnum = regs[FixRegId::IsrNum].get();

    match isrnum {
        14 => pagefault_handler(),
        32 => syscall(),
        _ => {
            panic!(
                "exception rip={:#x} isrnum={} errcode={}",
                regs[FixRegId::Rip].get(),
                regs[FixRegId::IsrNum].get(),
                regs[FixRegId::ErrorCode].get()
            );
        }
    }
    ret_to_userspace();
}

#[cfg(test)]
mod idt_tests {
    use super::*;

    #[test]
    #[should_panic]
    fn isr_panics() {
        let f = unsafe { core::mem::zeroed() };
        isr(&f);
    }
}

#[cfg(not(feature = "std"))]
extern "C" {
    static isr_0: u8;
    static isr_1: u8;
    static isr_2: u8;
    static isr_3: u8;
    static isr_4: u8;
    static isr_5: u8;
    static isr_6: u8;
    static isr_7: u8;
    static isr_8: u8;
    static isr_9: u8;
    static isr_10: u8;
    static isr_11: u8;
    static isr_12: u8;
    static isr_13: u8;
    static isr_14: u8;
    static isr_15: u8;
    static isr_16: u8;
    static isr_17: u8;
    static isr_18: u8;
    static isr_19: u8;
    static isr_20: u8;
    static isr_21: u8;
    static isr_22: u8;
    static isr_23: u8;
    static isr_24: u8;
    static isr_25: u8;
    static isr_26: u8;
    static isr_27: u8;
    static isr_28: u8;
    static isr_29: u8;
    static isr_30: u8;
    static isr_31: u8;
    static isr_32: u8;
}

#[cfg(not(feature = "std"))]
core::arch::global_asm!(
    // Creates a stub for a exception handler without an error code
    ".asm__isr:",
    "    push qword ptr 0", // FIXME: rdgsbase
    "    push qword ptr 0", // FIXME: rdfsbase
    "    push rbx",
    "    push r15",
    "    push r14",
    "    push r13",
    "    push r12",
    "    push r11",
    "    push r10",
    "    push r9",
    "    push r8",
    "    push rdx",
    "    push rcx",
    "    push rsi",
    "    push rdi",
    "    push rax",
    "    mov rax, [rsp - 8]", // Restore per-CPU kernel stack
    "    push rbp",
    "    lea rdi, [rsp - 16]",
    "    mov rsp, rax",
    "    mov rbp, rsp",
    "    jmp isr",
    // Creates a stub for a exception handler with an error code
    ".macro ISR_ERR isrnum",
    "  .global isr_\\isrnum",
    "  isr_\\isrnum:",
    "    push qword ptr \\isrnum",
    "    jmp .asm__isr",
    ".endm",
    // Creates a stub for a exception handler without an error code
    ".macro ISR_NOERR isrnum",
    "  .global isr_\\isrnum",
    "  isr_\\isrnum:",
    "    push qword ptr 0",
    "    push qword ptr \\isrnum",
    "    jmp .asm__isr",
    ".endm",
    // Sea of stubs; each reachable through isr_#n
    "ISR_NOERR 0",
    "ISR_NOERR 1",
    "ISR_NOERR 2",
    "ISR_NOERR 3",
    "ISR_NOERR 4",
    "ISR_NOERR 5",
    "ISR_NOERR 6",
    "ISR_NOERR 7",
    "ISR_ERR   8",
    "ISR_NOERR 9",
    "ISR_ERR   10",
    "ISR_ERR   11",
    "ISR_ERR   12",
    "ISR_ERR   13",
    "ISR_ERR   14",
    "ISR_NOERR 15",
    "ISR_NOERR 16",
    "ISR_ERR   17",
    "ISR_NOERR 18",
    "ISR_NOERR 19",
    "ISR_NOERR 20",
    "ISR_ERR   21",
    "ISR_NOERR 22",
    "ISR_NOERR 23",
    "ISR_NOERR 24",
    "ISR_NOERR 25",
    "ISR_NOERR 26",
    "ISR_NOERR 27",
    "ISR_NOERR 28",
    "ISR_ERR   29",
    "ISR_ERR   30",
    "ISR_NOERR 31",
    // Syscall
    "ISR_NOERR 32",
);
