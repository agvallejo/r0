//! Exit QEMU using a debug ISA device
//!
//! When r0 runs in QEMU, it does so with a special ISA device attached.
//! This allows early code still unable to use ACPI to exit the emulator,
//! with the added benefit that it's possible to make QEMU report a special
//! return code. This is critical to make QEMU-based test runners.

use super::asm;

/// PIO port through which the QEMUExit device can be accessed
const PIO_QEMU_EXIT: u16 = 0x501;

/// The QEMU exit device can only return odd numbers
/// so "3", is as good as any to consider it "success"
///
/// Note that QEMU shifts _left_ by 1 and adds 1 before returning, so
/// we need take that into account by dividing by 2 and rounding down.
const SUCCESS_CODE: u8 = 1;

/// Exit QEMU with `SUCCESS_CODE`
pub fn success() {
    unsafe { asm::Pio(PIO_QEMU_EXIT).outb(SUCCESS_CODE) };
}

/// Exit QEMU with anything _except_ `SUCCESS_CODE`
pub fn failure() {
    unsafe { asm::Pio(PIO_QEMU_EXIT).outb(SUCCESS_CODE + 1) };
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn exits_work() {
        success();
        let (port, val) = asm::mock::PIO_QUEUE.borrow_mut().pop_front().unwrap();

        assert_eq!(port, 0x501);
        assert_eq!(val, SUCCESS_CODE);

        failure();
        let (port, val) = asm::mock::PIO_QUEUE.borrow_mut().pop_front().unwrap();

        assert_eq!(port, 0x501);
        assert_ne!(val, SUCCESS_CODE);
    }
}
