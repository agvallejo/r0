//! per-CPU management

use core::mem::size_of;
use core::sync::atomic::{AtomicU32, Ordering};

use crate::log;
use crate::ocap::proc::Process;

use super::{gdt::Gdt, mm::this_cpu, proc::FixRegId};

/// x86_64-specific data of the CPU
pub struct PerCpuArch {
    /// APIC ID of this CPU. If it's in xAPIC mode,
    /// the APIC ID is in the bottom octet.
    pub x2apic_id: AtomicU32,

    /// TSS associated with this CPU
    pub tss: Tss, // 104 octets

    /// TSS associated with this CPU
    pub gdt: Gdt, // 56 octes
}

/// Legacy definition required by hardware.
#[repr(C)]
pub struct Tss {
    /// Unused.
    _head: u32,
    /// Stack pointer to return to on interrupts or exceptions from userspace.
    rsp0: [AtomicU32; 2],
    /// Unused.
    _tail: [u32; 0x17],
}

#[doc(hidden)]
const _: [u8; 104] = [0; size_of::<Tss>()];

impl PerCpuArch {
    /// Initialises the arch-specific per-CPU data
    pub unsafe fn init(&self, info: &limine::SmpInfo) {
        self.x2apic_id.store(info.lapic_id, Ordering::Relaxed);
        log::Smp("init", info.lapic_id).info();
    }
}

/// Update RSP so that the next interrupt taken from userspace lands in the
/// right place (namely, the register bank of the currently running process)
pub fn update_isr_sp(proc: &Process) {
    let ss_loc = &proc.arch.fixreg[FixRegId::Ss];

    // Set the stack one word after the SS so the next
    // interrupt leaves the registers in their rightful
    // place
    let sp = core::ptr::addr_of!(*ss_loc) as usize + 8;
    let tss = &this_cpu().arch.tss;
    tss.rsp0[0].store(sp as u32, Ordering::Relaxed);
    tss.rsp0[1].store((sp >> 32) as u32, Ordering::Relaxed);
}
