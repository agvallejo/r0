//! Global Descriptor Table
//!
//! Segment registers are x86-specific registers with very different
//! meaning depending on the decade we're talking about. In modern machines
//! their use is vestigial, but nonetheless required. These days segment
//! registers are loaded with _segment selectors_, which are an index
//! into the GDT tagged with the _Requested Privilege Level_ (RPL)
//!
//! The GDT is a table of _segment descriptors_. While one would hope to
//! call it a _legacy_ table, truth is it's still needed in modern machines.
//! Fortunately, it's a matter of performing a one-off initialization
//! that's required.
//!
//! So, _segment registers_ are loaded with _segment selectors_, which
//! point to _segment descriptors_. Why should we set them up though?
//!
//! Assuming strict long-mode the unsatisfying answer is "because the CPU
//! said so and it will triple fault if you don't". The longer answer is that
//! this allows us to switch back and forward between long mode and
//! compatibility mode. Also, segments gs and fs _may_ be used for having
//! Thread Local Storage (TLS) support.

use core::mem::size_of_val;
use core::sync::atomic::{AtomicU64, Ordering};

use super::mm::this_cpu;

/// GDT Register
///
/// Describes location and size of the GDT so we can point the HW to it
#[repr(C)]
pub struct Gdtr {
    /// Unused padding fields
    _pad: [u16; 3],
    /// Size of the GDT in octets minus 1
    pub limit: u16,
    /// Virtual address of the GDT
    offset: u64,
}

/// Global Descriptor Table
///
/// A statically-defined table with all segments for kernel and user modes
/// They are laid out in the order required to use SYSCALL/SYSRET. It also
/// contains the descriptor for the TSS (required for IRQs/exceptions from
/// userspace)
#[repr(transparent)]
pub struct Gdt([AtomicU64; 7]);

impl Gdt {
    /// Update the TSS base in the GDT.
    ///
    /// This is absurd and a one time thing, so it's hardcoded.
    /// Sorry, but not sorry.
    pub fn init(&self) {
        self.0[0].store(
            // [0x00] Null
            0x0000000000000000,
            Ordering::Relaxed,
        );
        self.0[1].store(
            // [0x08] 64bit code kernel
            0x00209A0000000000,
            Ordering::Relaxed,
        );
        self.0[2].store(
            // [0x10] 64bit data kernel
            0x0000920000000000,
            Ordering::Relaxed,
        );
        self.0[3].store(
            // [0x18] 64bit code user
            0x0020FA0000000000,
            Ordering::Relaxed,
        );
        self.0[4].store(
            // [0x20] 64bit data user
            0x0000F20000000000,
            Ordering::Relaxed,
        );

        let tss_base = core::ptr::addr_of!(this_cpu().arch.tss) as u64;
        let tss_lo = tss_base & 0xFFFFFF;
        let tss_mid = (tss_base >> 24) & 0xFF;

        self.0[5].store(
            // [0x28] 64bit TSS (lo)
            0x0000890000000068 | (tss_mid << 56) | (tss_lo << 16),
            Ordering::Relaxed,
        );
        self.0[6].store(tss_base >> 32, Ordering::Relaxed);
    }
}

/// Initialises the per-CPU GDT. Must be called during stage2, after percpu
/// mappings are present, and must be done once per CPU.
pub unsafe fn init() {
    let gdt = &this_cpu().arch.gdt;
    gdt.init();

    let gdtr = Gdtr {
        _pad: [0; 3],
        limit: size_of_val(gdt) as u16 - 1,
        offset: gdt as *const _ as u64,
    };
    super::asm::lgdt(&gdtr);
}
