//! 64-bit PC
//!
//! This port covers both AMD and Intel 64-bit machine architectures

use core::sync::atomic::{AtomicU64, Ordering};

use crate::mapping::ptab::PteRestr;
use crate::percpu::CpuId;
use crate::{global, log, phys, sched};

pub mod asm;
pub mod exit;
pub mod mm;
pub mod pagefault;
pub mod percpu;
pub mod pio;
pub mod proc;
pub mod serial;

mod gdt;
mod idt;

#[cfg(not(feature = "std"))]
/// Defines an external linker symbol so different sections can be located
macro_rules! ld_sym {
    ($i:ident) => {
        extern "C" {
            pub static $i: u8;
        }
    };
}

#[cfg(feature = "std")]
/// Defines an mocked linker symbol for linking purposes
macro_rules! ld_sym {
    ($i:ident) => {
        pub static $i: u8 = 0;
    };
}

ld_sym!(KERN_START);
ld_sym!(KERN_END);
ld_sym!(__LD_RODATA_START);
ld_sym!(__LD_RODATA_END);
ld_sym!(__LD_TEXT_START);
ld_sym!(__LD_TEXT_END);
ld_sym!(__LD_DATA_START);
ld_sym!(__LD_DATA_END);

/// Kernel stage1 entry point
///
/// Limine's boot protocol ensures we already have
/// a stack by the time the bootloader jumps here
#[no_mangle]
pub unsafe extern "C" fn kern_main() -> ! {
    serial::init();
    idt::init();

    crate::fb::init();

    phys::init();
    mm::init();

    core::arch::asm!(
        "mov cr3, {0}",
        "mov rsp, {1}",
        "mov rbp, rsp",
        "jmp {2}",
        in(reg) mm::FULL_KERN_PML4_PADDR.load(Ordering::Relaxed),
        in(reg) mm::get_cpu(CpuId(0)),
        in(reg) kern_main_stage2,
        options(preserves_flags, nomem)
    );
    unreachable!();
}

/// Kernel stage2 entry point
///
/// The kernel reaches this point with a fresh stack
/// stacks. The memory
/// a stack by the time the bootloader jumps here
unsafe extern "C" fn kern_main_stage2() -> ! {
    // Ensure the framebuffer points at OUR directmap
    crate::fb::stage2_init();

    log::Init("stage2").info();

    crate::percpu::init();
    gdt::init();

    global::init();
    crate::sysimg::init();

    sched::run();
}

/// Physical Address
#[repr(transparent)]
#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub struct Paddr(pub u64);

/// Page Table Entry
#[derive(Debug)]
#[repr(transparent)]
pub struct Pte(AtomicU64);

impl Pte {
    /// Present
    pub const BIT_P: u64 = 1;
    /// Read-write
    pub const BIT_RW: u64 = 1 << 1;
    /// User-supervisor
    pub const BIT_US: u64 = 1 << 2;
    /// Page-Size (indicates this is a leaf of 2MiB or 1GiB)
    pub const BIT_PS: u64 = 1 << 7;
    /// No-eXecute
    pub const BIT_NX: u64 = 1 << 63;

    /// The following invariant is expected of every
    /// physical address usable for page tables.
    ///
    /// # Examples
    /// ```
    /// let addr = 0x12341234000;
    /// assert_eq!(addr & ADDR_MASK, addr);
    /// ```
    pub const ADDR_MASK: u64 = !(PageSize::Normal.mask() | Self::BIT_NX);

    /// Fetch the address from this PTE, excluding the flags. Returns None if entry is invalid
    pub fn addr(&self) -> Option<u64> {
        let val = self.0.load(Ordering::Relaxed);
        if val & Self::BIT_P == 0 {
            return None;
        }

        // Remove all non-address bits (including NX)
        let mask = PageSize::Normal.mask() | (u64::MAX << 48);
        Some(val & !mask)
    }

    /// Rewrite a PTE for a leaf frame with certain restrictions
    pub fn set_leaf(&self, pa: Paddr, ps: PageSize, restr: PteRestr) {
        let pteval = pa.0
            | Pte::BIT_P
            | match restr {
                PteRestr::ReadOnly => Pte::BIT_NX,
                PteRestr::ReadWrite => Pte::BIT_NX | Pte::BIT_RW,
                PteRestr::ExecOnly => 0,
            }
            | if ps == PageSize::Normal {
                0
            } else {
                Pte::BIT_PS
            };
        self.0.store(pteval, Ordering::Relaxed);
    }

    /// FIXME: Merge with set_leaf()
    pub fn set_user_leaf(&self, pa: Paddr, ps: PageSize, restr: PteRestr) {
        let pteval = pa.0
            | Pte::BIT_P
            | Pte::BIT_US
            | match restr {
                PteRestr::ReadOnly => Pte::BIT_NX,
                PteRestr::ReadWrite => Pte::BIT_NX | Pte::BIT_RW,
                PteRestr::ExecOnly => 0,
            }
            | if ps == PageSize::Normal {
                0
            } else {
                Pte::BIT_PS
            };
        self.0.store(pteval, Ordering::Relaxed);
    }

    /// Rewrite a PTE for a PT frame with certain restrictions
    pub fn set_pt(&self, pa: Paddr) {
        let pteval = pa.0 | Pte::BIT_P | Pte::BIT_US | Pte::BIT_RW;
        self.0.store(pteval, Ordering::Relaxed);
    }

    /// Invalidate this PTE and invalidate only the local TLB
    pub fn invalidate(&self) {
        self.0.store(0, Ordering::Relaxed);
        asm::tlb_globalflush();
    }
}

/// Size of an x86 page
pub const PAGE_SIZE: usize = 4096;

/// Set of physical page sizes that can be configured in the page tables
///
/// Huge pages are not always available, but we just mandate them for ease of implementation
#[derive(Copy, Clone, Eq, PartialEq)]
pub enum PageSize {
    /// 4 KiB
    Normal,
    /// 2 MiB
    Super,
    /// 1 GiB
    Huge,
}

impl PageSize {
    /// Number of bits required to represent an offset into the page
    pub const fn bits(&self) -> u64 {
        match self {
            Self::Normal => 12,
            Self::Super => 21,
            Self::Huge => 30,
        }
    }

    /// Size of the page in octets
    pub const fn octets(&self) -> u64 {
        1 << self.bits()
    }

    /// A mask derived from [`Self::bits`]
    pub const fn mask(&self) -> u64 {
        self.octets() - 1
    }
}

#[cfg(test)]
mod x86_64_tests {
    use super::*;

    #[test]
    fn pte_access() {
        let pte = Pte((0x12341234 + PageSize::Huge.octets()).into());
        assert_eq!(pte.addr(), None);

        let pte = Pte((0x12341235 | Pte::BIT_NX).into());
        assert_eq!(pte.addr(), Some(0x12341000));
    }
}
