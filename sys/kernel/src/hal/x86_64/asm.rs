//! Assembly wrappers
//!
//! Includes all assembly wrapers in the system

use crate::hal::mm::this_cpu;

use super::proc::FixRegId;
use super::Paddr;

/// Legacy PIO port
///
/// Typically used to interact with legacy devices
pub struct Pio(pub u16);

#[cfg(feature = "std")]
pub mod mock {
    use std::cell::RefCell;
    use std::collections::VecDeque;

    #[thread_local]
    pub static PIO_QUEUE: RefCell<VecDeque<(u16, u8)>> = RefCell::new(VecDeque::new());
}

impl Pio {
    /// Sends an octet through a PIO port
    ///
    /// # Safety
    ///   The caller must ensure the port is safe
    pub unsafe fn outb(&self, val: u8) {
        #[cfg(feature = "std")]
        mock::PIO_QUEUE.borrow_mut().push_back((self.0, val));

        #[cfg(not(feature = "std"))]
        core::arch::asm!("out dx, al",
             in("dx") self.0,
             in("al") val,
             options(preserves_flags,
                     nomem,
                     nostack));
    }

    /// Receives an octet through a PIO port
    ///
    /// # Safety
    ///   The caller must ensure the port is safe to use
    pub unsafe fn inb(&self) -> u8 {
        let ret: u8;

        #[cfg(feature = "std")]
        {
            ret = 0;
        }

        #[cfg(not(feature = "std"))]
        core::arch::asm!("in al, dx",
             in("dx") self.0,
             out("al") ret,
             options(preserves_flags,
                     nomem,
                     nostack));
        ret
    }
}

/// Reload the GDTR register
///
/// Issues an `lgdt` instruction and reloads all the segments under the
/// assumption that the GDT is laid out to be compible with STAR/LSTAR
///
/// # Safety
///   The caller must ensure the GDTR is valid and points to a valid GDT
pub unsafe fn lgdt(gdtr: &super::gdt::Gdtr) {
    #[cfg(feature = "std")]
    let _ = gdtr;
    #[cfg(not(feature = "std"))]
    core::arch::asm!(
        "lgdt [{0}]",

        /* Load the TSS segment */
        "mov {1:x}, 0x28",
        "ltr {1:x}",

        /* Reload all data segments */
        "mov {1:x}, 0x10",
        "mov ds, {1:x}",
        "mov es, {1:x}",
        "mov ss, {1:x}",
        "mov fs, {1:x}",
        "mov gs, {1:x}",

        /* Reload CS via a far return */
        "mov {1}, 0x8",
        "push {1}",
        "lea {1}, [2f]",
        "push {1}",
        "retfq",
        "2:",
        in(reg) &gdtr.limit as *const u16,
        out(reg) _,
    );
}

/// Reload the IDTR register
///
/// Issues an `lidt` instruction
///
/// # Safety
///   The caller must ensure the IDTR is valid and points to a valid IDT
pub unsafe fn lidt(idtr: &super::idt::Idtr) {
    #[cfg(feature = "std")]
    let _ = idtr;
    #[cfg(not(feature = "std"))]
    core::arch::asm!( "lidt [{0}]", in(reg) &idtr.limit as *const u16);
}

/// Retrieve the physical address stored in cr3
///
/// Removes the bits in the lowest 12 bits
pub fn cr3() -> Paddr {
    #[cfg(not(feature = "std"))]
    unsafe {
        let ret: u64;
        core::arch::asm!( "mov {0}, cr3", out(reg) ret, options(nomem, nostack, preserves_flags));
        Paddr(ret & !super::PageSize::Normal.mask())
    }

    #[cfg(feature = "std")]
    Paddr(0)
}

/// Retrieve the linear address stored in cr2
pub fn cr2() -> u64 {
    #[cfg(not(feature = "std"))]
    unsafe {
        let ret: u64;
        core::arch::asm!( "mov {0}, cr2", out(reg) ret, options(nomem, nostack, preserves_flags));
        ret
    }

    #[cfg(feature = "std")]
    0
}

/// Override cr3 with a new set of page tables
unsafe fn set_cr3(pa: Paddr) {
    #[cfg(not(feature = "std"))]
    core::arch::asm!( "mov cr3, {}", in(reg) pa.0, options(nomem, nostack, preserves_flags));
    #[cfg(feature = "std")]
    let _ = pa;
}

/// Perform a CPU-local TLB flush via cr3 reload
pub fn tlb_localflush() {
    unsafe {
        set_cr3(cr3());
    }
}

/// Perform a TLB shootdown
///
/// INVLPGB+TLBSYNC
pub fn tlb_globalflush() {
    // TODO: Send IPIs, Or do INVLPGB+TLBSYNC
    tlb_localflush();
}

/// Returns to userspace through an iret
pub fn ret_to_userspace() -> ! {
    unsafe {
        let this = this_cpu();
        // We bypass locking because if it's there it's already locked
        //
        // TODO: Needs better explanation, but I don't have time now.
        let r = &this.cur.get().unwrap().get().arch.fixreg;
        set_cr3(Paddr(r[FixRegId::Cr3].get()));
        core::arch::asm!(
            "mov rsp, {0}",
            "pop rbp",
            "mov [rsp - 8], {1}", // Save per-CPU kernel stack
            "pop rax",
            "pop rdi",
            "pop rsi",
            "pop rcx",
            "pop rdx",
            "pop r8",
            "pop r9",
            "pop r10",
            "pop r11",
            "pop r12",
            "pop r13",
            "pop r14",
            "pop r15",
            "pop rbx",
            "add rsp, 8",  // FIXME: wrfsbase
            "add rsp, 8",  // FIXME: wrgsbase
            "add rsp, 16", // Skip IsrNum and ErrorCode
            "iretq",
            in(reg) &r[FixRegId::Rbp],
            in(reg) this,
            options(preserves_flags, noreturn),
        );
    }
}
