//! x86_64-specifics of processes
//!
//! Includes registers and any other process-specific state

use crate::{hal::mm::FULL_KERN_PML4_PADDR, mapping::ptab::PtabId};
use core::{cell::Cell, ops::Index, sync::atomic::Ordering};
use libsys::{base::WordCtl, ocap::CAPINV_WORD64_COUNT};

/// Indices into a [`FixRegs`]
#[repr(C)]
#[derive(Copy, Clone)]
pub enum FixRegId {
    #[doc(hidden)]
    Cr3,
    #[doc(hidden)]
    Cr2,
    #[doc(hidden)]
    Rbp,
    #[doc(hidden)]
    Rax,
    #[doc(hidden)]
    Rdi,
    #[doc(hidden)]
    Rsi,
    #[doc(hidden)]
    Rcx,
    #[doc(hidden)]
    Rdx,
    #[doc(hidden)]
    R8,
    #[doc(hidden)]
    R9,
    #[doc(hidden)]
    R10,
    #[doc(hidden)]
    R11,
    #[doc(hidden)]
    R12,
    #[doc(hidden)]
    R13,
    #[doc(hidden)]
    R14,
    #[doc(hidden)]
    R15,
    #[doc(hidden)]
    Rbx,
    #[doc(hidden)]
    Fsbase,
    #[doc(hidden)]
    Gsbase,
    #[doc(hidden)]
    IsrNum,
    #[doc(hidden)]
    ErrorCode,
    #[doc(hidden)]
    Rip,
    #[doc(hidden)]
    Cs,
    #[doc(hidden)]
    Rflags,
    #[doc(hidden)]
    Rsp,
    #[doc(hidden)]
    Ss,
}

/// Array of every fixed-point register (general and special pupose) in a process.
#[repr(C, align(16))]
pub struct FixRegs([Cell<u64>; FixRegId::Ss as usize + 1]);

impl Index<FixRegId> for FixRegs {
    type Output = Cell<u64>;

    fn index(&self, index: FixRegId) -> &Self::Output {
        &self.0[index as usize]
    }
}

/// Number of General Purpose Registers in a CPU
#[repr(C)]
pub struct ArchProcess {
    /// Array of fixed point registers. Includes general and special purpose registers
    pub fixreg: FixRegs,
    /// Root page table
    pub pml4: Cell<Option<PtabId>>,
}

/// In-regfile representation of the invocation parameters.
#[repr(C)]
pub struct SyscallBlock {
    /// Main invocation word, contains opcode, invocation type and caps involved.
    pub wctl: Cell<WordCtl>,
    /// Message payload.
    pub data: [Cell<u64>; 4],
}

impl ArchProcess {
    /// Set the RIP register to a set value
    pub fn set_ip(&self, ip: u64) {
        self.fixreg[FixRegId::Rip].set(ip);
    }

    /// Restarts IP to re-execute the syscall instruction
    ///
    /// This is 2 octets both in the "int $33" case and
    /// in the "syscall" case
    pub fn dec_ip(&self) {
        let ip = self.fixreg[FixRegId::Rip].get();
        self.fixreg[FixRegId::Rip].set(ip - 2);
    }

    /// Set the cr3 register to a set value
    pub fn set_rootpg(&self, pml4: Option<PtabId>) {
        self.pml4.set(pml4);
        if let Some(ptabid) = pml4 {
            self.fixreg[FixRegId::Cr3].set(ptabid.paddr().0);
        } else {
            self.fixreg[FixRegId::Cr3].set(FULL_KERN_PML4_PADDR.load(Ordering::Relaxed));
        }
    }

    /// Initialise the x86_64-specific area of a process object.
    pub fn init(&self) {
        // We need the bottom of the fixreg array to be 16-octet aligned
        // so can directly use it as a stack in the interrupt path.
        debug_assert!((core::ptr::addr_of!(self.fixreg[FixRegId::Ss]) as u64 + 8) & 0xF == 0);

        self.set_rootpg(None);
        self.fixreg[FixRegId::Cs].set(0x1B);
        self.fixreg[FixRegId::Rflags].set(0x202);
        self.fixreg[FixRegId::Ss].set(0x23);
    }

    /// Get a reference to the register bank
    pub fn syscall_block(&self) -> &SyscallBlock {
        let start = FixRegId::Rax as usize;
        let end = start + CAPINV_WORD64_COUNT;
        let base = &self.fixreg.0[start..end];
        unsafe { &*core::mem::transmute::<*const Cell<u64>, *const SyscallBlock>(base.as_ptr()) }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use libsys::{base::InvType, CapSlot};

    #[test]
    fn miri_agrees_with_syscall_block() {
        let p: ArchProcess = unsafe { core::mem::zeroed() };
        let sysblk = p.syscall_block();
        let wctl = WordCtl::new(InvType::Call, CapSlot(1), 1);
        sysblk.wctl.set(wctl);
        assert_eq!(sysblk.wctl.get().0, p.fixreg[FixRegId::Rax].get());
        assert_eq!(sysblk.wctl.get().0, wctl.0);
    }
}
