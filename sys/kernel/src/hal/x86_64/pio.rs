//! Interface for userspace access to PIO ports
use libsys::ocap::Cap;

use super::{asm::ret_to_userspace, proc::SyscallBlock};

/// `IoCtl` cap invocation handler
pub fn invoke_io(cap: Cap, sysblk: &SyscallBlock) -> ! {
    let wctl = sysblk.wctl.get();

    /* PIO arguments */
    let pio = super::asm::Pio(sysblk.data[0].get() as u16);

    /* Valid ranges of ports */
    let start = cap.payload as u16;
    let end = (cap.payload >> 16) as u16;

    if u64::from(pio.0) != sysblk.data[0].get() {
        todo!("invoke_io bad port port={:#x}", pio.0);
    }

    if pio.0 < start || pio.0 > end {
        todo!(
            "invoke_io restricted port. {start:#x} <= {:#x} <= {end:#x}",
            pio.0
        );
    }

    match wctl.cmd() {
        0 /* read */ =>  sysblk.data[1].set(unsafe { pio.inb().into() }),
        1 /* write */ => {
            let datum = sysblk.data[1].get() as u8;
            if u64::from(datum) != sysblk.data[1].get() {
                todo!("invoke_io bad datum");
            }
            unsafe { pio.outb(datum) }
        },
        x => todo!("invoke_io bad op ({x})"),
    }

    ret_to_userspace();
}
