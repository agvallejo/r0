//! Page Fault handler
//!
//! Foooooo

use libsys::ocap::{CapType, CAP_RESTR_NX, CAP_RESTR_RO};

use crate::hal::PageSize;
use crate::hal::{proc::FixRegId, Paddr, Pte};
use crate::mapping::dep::dep_remember;
use crate::mapping::ptab::{PtabRef, PteRestr};
use crate::mapping::rmap::{rm_remember_leaf, rm_remember_pt, rm_remember_toplevel};
use crate::ocap::cap::CapWalk;
use crate::ocap::page::{PageId, PageRef};
use crate::{global::HeapRef, log};

use super::mm::{this_cpu, PtabLvl, PAGING_NUM_LVLS};

/// Canary value for a PTE. Used to ensure atomicity in writing PTEs
///
/// See the main loop inside [`pagefault_handler`] for more details.
const PTE_CANARY: Paddr = Paddr(Pte::BIT_RW);

/// Validate memory access, creating the required mappings if needed and
/// recording enough bookeeping to undo it should it be required.
pub fn pagefault_handler() {
    let cur = this_cpu().cur.get().unwrap();
    let cur_id = cur.objid;
    let mut addr = super::asm::cr2();
    log::PageFault(cur_id, addr as usize).debug();

    // FIXME: Remove magic number
    if addr >= (256 << 39) {
        todo!("fault_delivery[{}]: addr={addr:#x} out of range", cur_id.0);
    }

    let cur = cur.get();

    let Some(walk) = CapWalk::new(&cur.addr_space, addr) else {
        todo!(
            "fault_delivery[{}]: addr={addr:#x} translation error",
            cur_id.0
        );
    };

    let leaf = unsafe { (*walk.leaf).get() };
    if leaf.typ() != CapType::Page {
        todo!(
            "fault_delivery[{}]: addr={addr:#x} translation error",
            cur_id.0
        );
    }

    let errcode = cur.arch.fixreg[FixRegId::ErrorCode].get();

    let write_error = errcode & 2 != 0;
    if write_error {
        // write violation
        if walk.restr & CAP_RESTR_RO != 0 {
            todo!(
                "fault_delivery[{}]: addr={addr:#x} illegal write\n\twalk={walk:?}",
                cur_id.0
            );
        }
    }

    let exec_error = errcode & 16 != 0;
    if exec_error && walk.restr & CAP_RESTR_NX != 0 {
        todo!("fault_delivery[{}]: addr={addr:#x} illegal exec", cur_id.0);
    }

    let leaf_mem = &PageRef::new(PageId(leaf.oid_hi)).get().mem;

    let mut cur_producer = if walk.step.is_empty() {
        // Leaf is the page, and that's the single producer
        leaf_mem
    } else {
        // Otherwise, pick the first step
        &walk.step[0].target.get().mem
    };

    let mut ptab = match cur.arch.pml4.get() {
        Some(id) => PtabRef::new(id),
        None => {
            let ptab = cur_producer.produce(PtabLvl::Pml4 as usize, 0);
            cur.arch.set_rootpg(Some(ptab.id));
            rm_remember_toplevel(ptab.id, cur_id);
            ptab
        }
    };

    // At this point we may have allocated a new top-level page table, but no
    // PTEs have been set or replaced yet within the user address space. There's
    // a very pernicious race to deal with moving forward. In particular, we
    // _don't want_ to set a PTE until all dependencies are recorded so we can
    // preserve the invariant that all dependencies of a PTE (revmap and depend
    // entries) exist for every valid PTE.
    //
    // TBD: The previous invariant probably needs rewording to account for
    //      existing translations in the TLB.
    //
    // The race is another CPU wiping a dependency (dep or rm) we have written
    // before we've set the PTE itself. That would lead to a dangling
    // unaccounted dependency. The way we cope with it is by installing a canary
    // value beforehand in the PTE, then setting all our dependencies, and then
    // setting the final value through atomic CAS. If we fail setting it it
    // means someone wiped a dependency under our back.
    //
    // It's very, very unlikely. But very, very important to get right.
    //
    // There's no such race for the top-level page table because we have an
    // implicit lock on the process that caused this pagefault.
    let mut prod_idx = 0;
    let mut step_lvl = 0;
    for pt_lvl in (PtabLvl::Pdpt as usize)..PAGING_NUM_LVLS {
        let bits_left = 12 + 9 * (PAGING_NUM_LVLS - pt_lvl); // TODO: Cleanup
        let i_pte = (addr >> bits_left) as u16;

        if step_lvl < walk.step.len() {
            let step = &walk.step[step_lvl];
            let mem = &step.target.get().mem;
            let slot_bitsz = mem.slot_bitsz.get();
            // Not every GPT is a producer. In particular, a GPT is the producer
            // in a walk if it decodes at least one bit not already, decoded by
            // the previous product of the previous producer.
            //
            // We must _produce_ on producers only. For non-producers we want to
            // record the dependency in a depend entry.
            if (slot_bitsz as usize) < bits_left {
                prod_idx = step.slot >> (bits_left as u8 - slot_bitsz);
                cur_producer = mem;
            } else {
                dep_remember(step.target.objid, ptab.id, step.slot as u16, i_pte);
                step_lvl += 1;
            }
        }

        let old = ptab;
        ptab = cur_producer.produce(pt_lvl, prod_idx.try_into().unwrap());

        rm_remember_pt(old.id, ptab.id, i_pte);
        // FIXME: Integrate in rm, so the rm entry is inserted atomically
        //        with writing the PTE
        old.get_frame().0[i_pte as usize].set_pt(ptab.id.paddr());

        addr &= (1 << bits_left) - 1;
    }

    let i_pte = (addr >> 12) as u16;
    assert!(i_pte < 512);
    let leaf = PageRef::new(PageId(leaf.oid_hi));
    rm_remember_leaf(leaf.objid, ptab.id, i_pte);
    // FIXME: Integrate in rm, so the rm entry is inserted atomically
    //        with writing the PTE
    ptab.get_frame().0[i_pte as usize].set_user_leaf(
        leaf.get().paddr.get(),
        PageSize::Normal,
        if exec_error {
            PteRestr::ExecOnly
        } else if write_error {
            PteRestr::ReadWrite
        } else {
            PteRestr::ReadOnly
        },
    );
}
