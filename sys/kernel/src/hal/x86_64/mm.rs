//! Arch-specific memory management
use core::sync::atomic::{AtomicU64, Ordering};

use crate::hal::{
    Paddr, PageSize, KERN_END, KERN_START, __LD_DATA_END, __LD_DATA_START, __LD_RODATA_END,
    __LD_RODATA_START, __LD_TEXT_END, __LD_TEXT_START,
};
use crate::mapping::ptab::{self, PageTabBundle, PtabRef, PteRestr};
use crate::percpu::{CpuId, PerCpu};
use crate::phys;

use libsys::PAGE_SIZE;

/// Enumeration of the different page table levels
/// that may be found in the x86 hierarchy
#[derive(Copy, Clone)]
pub enum PtabLvl {
    #[allow(clippy::missing_docs_in_private_items)]
    Pml4,
    #[allow(clippy::missing_docs_in_private_items)]
    Pdpt,
    #[allow(clippy::missing_docs_in_private_items)]
    Pd,
    #[allow(clippy::missing_docs_in_private_items)]
    Pt,
}

/// Number of levels in the paging hierarchy
pub const PAGING_NUM_LVLS: usize = 4;

/// Amount of virtual address space allocated for each cache object vector
///
/// The vast majority of it won't be allocated at all, but it logically
/// belongs to the object type it was reserved for.
pub const OBJ_VRANGE: usize = 16 << 30;

/// Start address of the kernel heap
///
/// Internally it's split in a set of virtual ranges, one per object type.
/// Each range is only partially allocated.
pub const DIRECTMAP_START: usize = usize::MAX << 47;

/// Start address of the kernel heap
///
/// Internally it's split in a set of virtual ranges, one per object type.
/// Each range is only partially allocated.
pub const DIRECTMAP_END: usize = DIRECTMAP_START + (2 << 30);

/// Start address of the kernel heap
///
/// Internally it's split in a set of virtual ranges, one per object type.
/// Each range is only partially allocated.
///
/// # Testing
///   On unit-tests this number is actually positive to avoid overlapping
///   the running kernel.
pub const HEAP_START: usize = if cfg!(feature = "std") {
    512 << 30
} else {
    0usize.wrapping_sub(512 << 30)
};

/// Start address of the vector of processes
pub const PROC_START: usize = HEAP_START;
/// End address of the vector of processes
pub const PROC_END: usize = PROC_START + OBJ_VRANGE;

/// Start address of the vector of GPTs
pub const GPT_START: usize = PROC_END;
/// End address of the vector of GPTs
pub const GPT_END: usize = GPT_START + OBJ_VRANGE;

/// Start address of the vector of page frame headers
pub const PAGE_START: usize = GPT_END;
/// End address of the vector of page frame headers
pub const PAGE_END: usize = PAGE_START + OBJ_VRANGE;

/// Start address of the vector of page tables
pub const PTAB_LVL0_START: usize = PAGE_END;
/// End address of the vector of page tables
pub const PTAB_LVL0_END: usize = PTAB_LVL0_START + OBJ_VRANGE;

/// Start address of the vector of page tables
pub const PTAB_LVL1_START: usize = PTAB_LVL0_END;
/// End address of the vector of page tables
pub const PTAB_LVL1_END: usize = PTAB_LVL1_START + OBJ_VRANGE;

/// Start address of the vector of page tables
pub const PTAB_LVL2_START: usize = PTAB_LVL1_END;
/// End address of the vector of page tables
pub const PTAB_LVL2_END: usize = PTAB_LVL2_START + OBJ_VRANGE;

/// Start address of the vector of page tables
pub const PTAB_LVL3_START: usize = PTAB_LVL2_END;
/// End address of the vector of page tables
pub const PTAB_LVL3_END: usize = PTAB_LVL3_START + OBJ_VRANGE;

/// Start address of the vector of page tables
pub const DEP_START: usize = PTAB_LVL3_END;
/// End address of the vector of page tables
pub const DEP_END: usize = DEP_START + OBJ_VRANGE;

/// Start address of the vector of reverse map entries
pub const RMAP_START: usize = DEP_END;
/// End address of the vector of reverse map entries
pub const RMAP_END: usize = RMAP_START + OBJ_VRANGE;

/// Start address of the vector of Object Table entries
pub const OTE_START: usize = RMAP_END;
/// End address of the vector of Object Table entries
pub const OTE_END: usize = OTE_START + OBJ_VRANGE;

/// End address of the kernel heap
pub const HEAP_END: usize = OTE_END;
/// Size of the kernel heap
pub const HEAP_SZ: usize = HEAP_END - HEAP_START;
/// Number of object types in the heap
pub const HEAP_NUMTYPES: usize = HEAP_SZ / OBJ_VRANGE;

#[doc(hidden)]
const _: () = assert!(HEAP_SZ <= (510 << 30) && (HEAP_SZ % (1 << 30)) == 0);
#[doc(hidden)]
const _: () = assert!(HEAP_START % (512 << 30) == 0);

/// Size of each per-CPU stack in octets (last page is a guard).
pub const PERCPU_STACK_SIZE: usize = 64 << 10;

/// Returns a reference to the percpu structure of an arbitrary CPU
pub const fn get_cpu(cpu: CpuId) -> &'static PerCpu {
    // At offset 2MiB from the kernel start there's an array of 2MiB
    // regions, 1 per CPU. The first page is always a guard, and the
    // remainder of the 64KiB aligned area is the percpu stack.
    let base = 0usize.wrapping_sub(2 << 30);
    unsafe { &*((base + (1 + cpu.0 as usize) * (2 << 20) + PERCPU_STACK_SIZE) as *const _) }
}

/// Returns a reference to the percpu structure of the current CPU
pub fn this_cpu() -> &'static PerCpu {
    let stacksize: u64 = PERCPU_STACK_SIZE.try_into().unwrap();
    let rsp: u64;
    unsafe {
        core::arch::asm!("mov {}, rsp", out(reg) rsp);
        &*(((rsp + stacksize) & !(stacksize - 1)) as *const _)
    }
}

/// A PML4 that includes a full the directmap and every per-CPU resource.
#[cfg_attr(feature = "std", thread_local)]
pub static FULL_KERN_PML4: ptab::Frame = unsafe { core::mem::zeroed() };

/// The physical address of [`FULL_KERN_PML4`]
#[cfg_attr(feature = "std", thread_local)]
pub static FULL_KERN_PML4_PADDR: AtomicU64 = AtomicU64::new(0);

/// A kernel PDPT that includes every per-CPU resource.
#[cfg_attr(feature = "std", thread_local)]
static FULL_KERN_PDPT: ptab::Frame = unsafe { core::mem::zeroed() };

/// Set of PDs that hold the kernel heap.
///
/// This should be every PD that the heap could possibly need.
#[cfg_attr(feature = "std", thread_local)]
static HEAP_PD: [ptab::Frame; HEAP_SZ >> 30] = unsafe { core::mem::zeroed() };

/// A kernel PDPT that includes every per-CPU resource.
#[cfg_attr(feature = "std", thread_local)]
static DIRECTMAP_PDPT: [ptab::Frame; 2] = unsafe { core::mem::zeroed() };

/// The kernel PD that includes the kernel image.
#[cfg_attr(feature = "std", thread_local)]
static KERN_PD: ptab::Frame = unsafe { core::mem::zeroed() };

/// The kernel PT that includes the kernel image.
#[cfg_attr(feature = "std", thread_local)]
static KERN_PT: ptab::Frame = unsafe { core::mem::zeroed() };

/// Initialise the kernel image area of the address space.
unsafe fn init_kimg() {
    let kern_start = phys::kpa(&KERN_START).0;
    let kern_end = phys::kpa(&KERN_END).0;
    // The kernel image must fit in a single superpage
    assert!((kern_end - kern_start) <= (2 << 20));

    // rodata
    let start = phys::kpa(&__LD_RODATA_START).0;
    let end = phys::kpa(&__LD_RODATA_END).0;
    let base = (start - kern_start) as usize / PAGE_SIZE;

    for (i, pa) in (start..end).step_by(PAGE_SIZE).enumerate() {
        KERN_PT.0[base + i].set_leaf(Paddr(pa), PageSize::Normal, PteRestr::ReadOnly);
    }

    // text
    let start = phys::kpa(&__LD_TEXT_START).0;
    let end = phys::kpa(&__LD_TEXT_END).0;
    let base = (start - kern_start) as usize / PAGE_SIZE;

    for (i, pa) in (start..end).step_by(PAGE_SIZE).enumerate() {
        KERN_PT.0[base + i].set_leaf(Paddr(pa), PageSize::Normal, PteRestr::ExecOnly);
    }

    // data
    let start = phys::kpa(&__LD_DATA_START).0;
    let end = phys::kpa(&__LD_DATA_END).0;
    let base = (start - kern_start) as usize / PAGE_SIZE;

    for (i, pa) in (start..end).step_by(PAGE_SIZE).enumerate() {
        KERN_PT.0[base + i].set_leaf(Paddr(pa), PageSize::Normal, PteRestr::ReadWrite);
    }

    let pt_pa = phys::kpa(&KERN_PT);
    KERN_PD.0[0].set_pt(pt_pa);
    let pd_pa = phys::kpa(&KERN_PD);
    FULL_KERN_PDPT.0[510].set_pt(pd_pa);
    let pdpt_pa = phys::kpa(&FULL_KERN_PDPT);
    FULL_KERN_PML4.0[511].set_pt(pdpt_pa);
}

/// Initialise the per-CPU areas of the address space.
unsafe fn init_percpu() {
    // Each CPU gets 2 MiB of virtual range
    for cpu in 0..crate::percpu::nr_cpus() {
        let pt_pa = phys::alloc(PageSize::Normal).unwrap();
        // # Safety
        // The page comes straight from the allocator, so it's safe and
        // it can be casted because Limines identity map is still there.
        let pt = unsafe { &*(pt_pa.0 as *const ptab::Frame) };

        // Set up a self-mapping for transmap management
        pt.0[511].set_leaf(pt_pa, PageSize::Normal, PteRestr::ReadWrite);

        // percpu rw area (last page global percpu)
        //
        // Starts at 1 to provide a guard page
        for i in 1..(1 + PERCPU_STACK_SIZE / PAGE_SIZE) {
            let ps = PageSize::Normal;
            let pa = phys::alloc(ps).unwrap();
            pt.0[i].set_leaf(pa, ps, PteRestr::ReadWrite);
        }

        // Skip the kernel image!
        KERN_PD.0[1 + cpu].set_pt(pt_pa);
    }
}

/// Initialise the directmap area of the address space.
unsafe fn init_directmap() {
    let mut pa = 0;
    for (i, pdpt) in DIRECTMAP_PDPT.iter().enumerate() {
        for pte in pdpt.0.iter() {
            pte.set_leaf(Paddr(pa), PageSize::Huge, PteRestr::ReadWrite);
            pa += 1 << 30;
        }
        FULL_KERN_PML4.0[256 + i].set_pt(phys::kpa(pdpt));
    }
}

/// Initialise a basic heap to bootstrap sysimg. After the primordial
/// processes start up they will manage the kernel heap moving pages around
unsafe fn init_heap() {
    for (i, pd) in HEAP_PD.iter().enumerate() {
        FULL_KERN_PDPT.0[i].set_pt(phys::kpa(pd));

        // Only prepopulate the first superpage of each object type
        if i % (OBJ_VRANGE >> 30) != 0 {
            continue;
        }

        let ps = PageSize::Super;
        let pa = phys::alloc(ps).unwrap();

        // Safe because there's still an identity map in place
        core::slice::from_raw_parts_mut(pa.0 as *mut u8, PAGE_SIZE)
            .iter_mut()
            .for_each(|x| *x = 0);
        pd.0[0].set_leaf(pa, ps, PteRestr::ReadWrite);
    }
}

/// Calculates the physical address of a bundle
pub fn bundle_get_paddr(bundle: &PageTabBundle) -> Paddr {
    let addr = core::ptr::addr_of!(*bundle) as u64;

    // We extract from the address the chunk that would be consumed by the
    // PDPT. This allows us to
    let pd = &HEAP_PD[((addr >> 30) & 0x1ff) as usize];

    Paddr(pd.0[0].addr().unwrap())
}

/// Initialise the bootstrap page tables.
pub unsafe fn init() {
    init_kimg();
    init_percpu();
    init_directmap();
    init_heap();

    FULL_KERN_PML4_PADDR.store(phys::kpa(&FULL_KERN_PML4).0, Ordering::Relaxed);
}

/// Given a top-level page table, attaches the kernel PDPT to it.
pub fn map_kernel(ptab: PtabRef) {
    assert_eq!(ptab.id.lvl(), 0);
    let kernel_pte = FULL_KERN_PML4.0[511].0.load(Ordering::Relaxed);
    ptab.get_frame().0[511]
        .0
        .store(kernel_pte, Ordering::Relaxed);
}
