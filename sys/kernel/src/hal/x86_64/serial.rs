//! Serial port support
//!
//! Module to communicate with the local UART. In principle we only care
//! about using it as a means of IO on early QEMU environemnts, but it can
//! easily be made to work on a physical UART as well.

use super::asm;

/// COM1. Used for testing on QEMU
const PORT: u16 = 0x3F8;

/// Initialise the serial driver
///
/// Configure baud rate, interrupts, FIFOs, etc
pub fn init() {
    unsafe {
        asm::Pio(PORT + 1).outb(0);
    }

    /* This is only really useful on QEMU. No need to configure anything
     * else (at least not until r0 gets hooked to an actual machine)
     */
}

/// Sends an octet over the serial line
///
/// # Warning
///
/// This function ignores errors and overruns. It is acceptable because in
/// the context of emulated serial ports those overruns cannot happen.
pub fn put(c: u8) {
    unsafe {
        asm::Pio(PORT).outb(c);
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn send_char_succeeds() {
        init();
        assert_eq!(
            asm::mock::PIO_QUEUE.borrow_mut().pop_front().unwrap(),
            (0x3F9, 0)
        );

        put(127);
        assert_eq!(
            asm::mock::PIO_QUEUE.borrow_mut().pop_front().unwrap(),
            (0x3F8, 127)
        );
    }
}
