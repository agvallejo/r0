//! Hardware Abstraction Layer
//!
//! Everything arch-specific

/// 64-bit PC
///
/// Covers both amd64 and ia32e
#[cfg(target_arch = "x86_64")]
pub mod x86_64;
#[cfg(target_arch = "x86_64")]
pub use x86_64::*;
