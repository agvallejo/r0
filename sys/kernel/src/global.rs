//! Global state handling
//!
//! This module deals with kernel heap initialization, access and manipulation
use core::cell::SyncUnsafeCell;

use crate::hal::mm::PAGING_NUM_LVLS;
use crate::mapping::dep::{self, DependBundle};
use crate::mapping::ptab::{self, PageTabBundle};
use crate::mapping::rmap::{self, RevMapBundle};
use crate::ocap::ote::{self, OteBundle};
use crate::ocap::{self, gpt, page, proc};

/// Encapsulation of the kernel heap. Holds every slice of cached objects and book-keeping of the
/// backing store used for each.
#[repr(C)]
pub struct ObjectCache<'a> {
    /// Global slice of processes
    pub proc: &'a [proc::Process],
    /// Global slice of GPTs
    pub gpt: &'a [gpt::Gpt],
    /// Global slice of pages
    pub page: &'a [page::Page],

    /// Global slice of mapping bundles
    pub ptab: [&'a [PageTabBundle]; PAGING_NUM_LVLS],
    /// Global slice of reverse map bundles
    pub rmap: &'a [RevMapBundle],
    /// Global slice of depend table bundles
    pub dep: &'a [DependBundle],

    /// Global slice of object table bundle
    pub ote: &'a [OteBundle],

    /// Number of superpages used to hold processes in the kernel heap.
    pub proc_nr_superpages: u32,
    /// Number of superpages used to hold GPTs in the kernel heap.
    pub gpt_nr_superpages: u32,
    /// Number of superpages used to hold page headers in the kernel heap.
    pub page_nr_superpages: u32,
}

/// Singleton of the kernel heap. It's [`SyncUnsafeCell`] so that a CPU may rendezvous the others
/// and modify it. This allows the typical case to bypass a useless RW-lock.
pub static OBJCACHE: SyncUnsafeCell<ObjectCache> = SyncUnsafeCell::new(ObjectCache {
    proc: &[],
    gpt: &[],
    page: &[],

    ptab: [&[]; PAGING_NUM_LVLS],
    rmap: &[],
    dep: &[],

    ote: &[],

    proc_nr_superpages: 1,
    gpt_nr_superpages: 1,
    page_nr_superpages: 1,
});

/// Initialise the kernel heap
pub unsafe fn init() {
    // Initialise OTEs first, as they are needed for the objects
    ote::init();

    proc::init();
    gpt::init();
    page::init();

    ptab::init();
    rmap::init();
    dep::init();
}

/// Sentinel value used where null pointers would normally go
///
/// Note that zero is a valid index, so we use `-1` as the sentinel instead
pub const SENTINEL: u32 = u32::MAX;

/// Contract of a generic 32bit index into a heap array.
pub trait HeapId: Copy + Clone {}

/// Trait bound to the [`ObjRef`] type. This allows :wq
pub trait HeapRef
where
    Self: Sized,
{
    /// Type of index into the array of objects of this type
    type ObjId;

    /// Type of each element of the array
    type ObjType;

    /// Base address of the array of objects of this type
    const BASE: *mut Self::ObjType;

    /// Creates a new reference to the heap from its index in the array
    fn new(id: Self::ObjId) -> Self;

    /// Finds an OID in the global array of this type
    ///
    /// `Err(())` if the OID is not loaded
    fn lookup(oid: u64) -> Result<Self, ocap::Error>;
}

/// An in-stack representation of an object reference
///
/// Object _pointers_ in their in-memory form are merely indices into the
/// per-object arrays, and the actual objects don't contain their own index
/// to save space. This is the solution to avoid taking locks more than
/// we should. The convention is to return this after locking an object so
/// the caller has access to both the pointer and the id.
#[derive(Debug)]
pub struct ObjRef<ID: HeapId, OBJ> {
    /// Index of the object in the per-objtype array
    pub objid: ID,
    /// Actual pointer to the inner type. _NOT_ the lock
    pub objref: *const OBJ,
}

impl<ID: HeapId, OBJ> Copy for ObjRef<ID, OBJ> {}
impl<ID: HeapId, OBJ> Clone for ObjRef<ID, OBJ> {
    fn clone(&self) -> Self {
        *self
    }
}

impl<ID: HeapId, OBJ> ObjRef<ID, OBJ> {
    /// Turns the internal pointer into a reference. This is safe because
    /// the object is guaranteed to exist (or the Ref wouldn't exist).
    pub fn get<'a>(&self) -> &'a OBJ {
        unsafe { &*self.objref }
    }
}
