//! Initial System Image
//!
//! Parses the initial system image (sysimg) passed as a module in order to
//! configure the global vectors.
use core::ops::Deref;

use libsys::sysimg::{SerialisedGpt, SerialisedProcess, SysImgHeader, SYSIMG_MAGIC};
use libsys::PAGE_SIZE;

use crate::global::HeapRef;
use crate::ocap::gpt::GptRef;
use crate::ocap::page::PageRef;
use crate::ocap::proc::ProcRef;
use crate::ocap::{gpt::GptId, page::PageId, proc::ProcId};
use crate::{log, sched};

/// Limine request for the initial system image
static SYSIMG: limine::ModuleRequest = limine::ModuleRequest::new(0);

/// Parses the initial system image and preloads the global vectors
pub unsafe fn init() {
    let modlist = SYSIMG.get_response().get().unwrap();

    // We merely expect the sysimg as a module
    assert_eq!(modlist.module_count, 1);

    let file = modlist.modules()[0].deref();
    assert!(file.length > core::mem::size_of::<SysImgHeader>() as u64);

    let sysimg = unsafe {
        let p = file.base.as_ptr().unwrap();
        &*core::mem::transmute::<*const u8, *const SysImgHeader>(p)
    };
    assert_eq!(sysimg.magic, SYSIMG_MAGIC);
    assert_eq!(sysimg.total_octets, file.length);

    let (sysimg_procvec, sysimg_gptvec, sysimg_pagevec) = unsafe {
        let p = file.base.as_ptr().unwrap();
        (
            core::slice::from_raw_parts(
                core::mem::transmute::<*const u8, *const SerialisedProcess>(
                    p.wrapping_add(sysimg.proc_offset as usize),
                ),
                sysimg.proc_len as usize,
            ),
            core::slice::from_raw_parts(
                core::mem::transmute::<*const u8, *const SerialisedGpt>(
                    p.wrapping_add(sysimg.gpt_offset as usize),
                ),
                sysimg.gpt_len as usize,
            ),
            core::slice::from_raw_parts(
                core::mem::transmute::<*const u8, *const [u8; PAGE_SIZE]>(
                    p.wrapping_add(sysimg.page_offset as usize),
                ),
                sysimg.page_len as usize,
            ),
        )
    };

    for (i, sysimg_proc) in sysimg_procvec.iter().enumerate() {
        let procid = ProcId(i.try_into().unwrap());
        let proc = ProcRef::new(procid).get();
        proc.init_from_serialised(1 + i as u64, sysimg_proc);
        sched::init_push(proc);
    }
    log::SysImgLoad("proc", sysimg_procvec.len()).info();

    for (i, sysimg_gpt) in sysimg_gptvec.iter().enumerate() {
        let gptid = GptId(i.try_into().unwrap());
        let gpt = GptRef::new(gptid);
        gpt.get().init_from_serialised(1 + i as u64, sysimg_gpt);
    }
    log::SysImgLoad("gpt", sysimg_gptvec.len()).info();

    for (i, sysimg_page) in sysimg_pagevec.iter().enumerate() {
        let pageid = PageId(i.try_into().unwrap());
        let page = PageRef::new(pageid);
        page.get().init_from_serialised(1 + i as u64, sysimg_page);
    }
    log::SysImgLoad("page", sysimg_pagevec.len()).info();
}
