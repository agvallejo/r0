//! Synchronisation primitives
//!
//! The concurrency story here is identical to that of Coyotos. A
//! _lockword_ is a combination of a _transaction ID_ and a CPU index.
//! When a CPU wants to grab a lock, it must insert its lockword into the
//! lockword of the lock. A lock is said to be taken if its lockword
//! matches any of the lockwords of the CPUs. Note that, because the CPU
//! index is part of the lockword it's possible to know who has which locks.
//!
//! Furthermore, it's possible for a CPU to release all its locks by simply
//! bumping its own lockword, or release a single one, by decreasing the
//! transaction ID part of of the lockword in the mutex.
//!
//! We say we're _gang-releasing_ the locks when we release them all at once.

use core::sync::atomic::{AtomicU64, Ordering};

use crate::hal::mm::{get_cpu, this_cpu};
use crate::percpu::CpuId;

/// This locking primitive is present in both [`LwMutex`] and [`crate::percpu::PerCpu`]
/// structures. The convention is that if the lockword in a [`LwMutex`] matches
/// the lockword in a [`crate::percpu::PerCpu`] structure, then the lock is taken.
#[derive(Copy, Clone, Debug)]
pub struct LockWord(pub u64);

impl LockWord {
    /// Synthesises a valid lockword for a CPU
    pub const fn new(cpu: CpuId) -> Self {
        Self((cpu.0 as u64) << 48)
    }

    /// Extracts the CPU ID from the lockword
    pub fn cpu_id(&self) -> CpuId {
        CpuId((self.0 >> 48) as u16)
    }

    /// Extracts the Transaction ID from the lockword
    pub fn transaction_id(&self) -> u32 {
        self.0 as u32
    }

    /// Bumps the lockword to gang-release every lock
    ///
    /// Note that this is just a copy of the lockword, it must still be
    /// updated in the lockword in [`crate::percpu::PerCpu`]
    pub fn inc_transaction_id(&mut self) -> &mut Self {
        self.0 += 1;
        self.0 &= !(1 << 32);
        self
    }

    /// Bumps the lockword to gang-release every lock
    ///
    /// Note that this is just a copy of the lockword, it must still be
    /// updated in the lockword in [`crate::percpu::PerCpu`]
    pub fn dec_transaction_id(&mut self) -> &mut Self {
        if self.transaction_id() == 0 {
            self.0 |= u64::from(u32::MAX);
        } else {
            self.0 -= 1;
        }
        self
    }
}

/// Used to ensure mutual exclusion on shared resources
#[repr(C)]
#[derive(Debug)]
pub struct LwMutex(AtomicU64);

/// Potential errors out of [`LwMutex::tryonce_lock`]
enum Error {
    /// Found unlocked mutex, but some other CPU grabbed it first.
    ///
    /// Caller meant to retry
    LostRace,

    /// Found a locked mutex, and the caller has priority.
    ///
    /// Caller meant to retry
    AlreadyLocked,

    /// Found a locked mutex, and the other CPU has priority
    ///
    /// Caller meant to abort the current transaction and start from scratch
    Deferred,
}

impl LwMutex {
    /// Creates a new unlocked mutex. Usable for global state.
    pub const fn new() -> Self {
        Self(AtomicU64::new(LockWord::new(CpuId(0)).0))
    }

    /// Try to grab this lock. On failure, the CPU is expected to abort the
    /// current transaction and restart from scratch. Trying again is
    /// forbidden and causes deadlocks.
    pub fn try_lock(&self) -> Result<(), ()> {
        loop {
            match self.tryonce_lock() {
                Ok(x) => return Ok(x),
                Err(Error::Deferred) => return Err(()),
                Err(_) => continue,
            }
        }
    }

    /// Attempt to grab the lock just once
    fn tryonce_lock(&self) -> Result<(), Error> {
        let lw_mutex = self.0.load(Ordering::Relaxed);

        let cpu = get_cpu(LockWord(lw_mutex).cpu_id());
        let lw_cpu = cpu.lockword.load(Ordering::Relaxed);

        let this = this_cpu();
        let lw_this = this.lockword.load(Ordering::Relaxed);

        if lw_mutex != lw_cpu
            && self
                .0
                .compare_exchange_weak(lw_mutex, lw_this, Ordering::Acquire, Ordering::Relaxed)
                .is_err()
        {
            // Was unlocked, but lost the race to lock it
            return Err(Error::LostRace);
        }

        if lw_mutex != lw_this {
            // Locked, and not by us!

            // Deadlock avoidance. Get out of the way if someone told us to
            let lw_defer = this.defer_lockword.load(Ordering::Relaxed);
            if lw_this == lw_defer {
                return Err(Error::Deferred);
            }

            // Tell the other CPU to bugger off if we think we're more important
            if lw_this < lw_cpu {
                cpu.defer_lockword.store(lw_cpu, Ordering::Relaxed);
            }

            return Err(Error::AlreadyLocked);
        }

        Ok(())
    }
}
