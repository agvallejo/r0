//! Page Table procedures !
//! This module deals with page table manipulations. Note that it's imperative for trees of page
//! tables to have strictly less permissions than those of the capabilities trees that created
//! them.
use core::cell::Cell;
use core::mem::size_of;

use crate::global::OBJCACHE;
use crate::hal::mm::{bundle_get_paddr, OBJ_VRANGE, PAGING_NUM_LVLS, PTAB_LVL0_START};
use crate::hal::{Paddr, PageSize, Pte};
use crate::log;
use crate::ocap::ObjType;

use libsys::PAGE_SIZE;
use spin::mutex::SpinMutex;

use super::rmap::rm_forget_pt;

/// A LRU list head. There's one per page table level.
#[derive(Debug)]
#[repr(C, align(64))]
pub struct PtabLruHead {
    /// Most recently used. SENTINEL iff empty.
    front: PtabId,
    /// Least recently used. SENTINEL iff empty.
    back: PtabId,
}

impl PtabLruHead {
    /// Grab the least recently used page table, invalidate all references to
    /// it, dettach it from its producer and return it after moving it to the
    /// front of the age queue.
    pub fn alloc(&mut self) -> PtabRef {
        let ptab = PtabRef::new(self.back);
        rm_forget_pt(ptab.id);
        ptab.dettach_producer();

        // We'll never have a single page table in the chain
        assert_ne!(self.front, self.back);

        let hdr = ptab.get_header();

        self.back = hdr.lru_front.get();
        PtabRef::new(self.back)
            .get_header()
            .lru_back
            .set(PtabId(SENTINEL));

        hdr.lru_back.set(self.front);
        hdr.lru_front.set(PtabId(SENTINEL));

        PtabRef::new(self.front).get_header().lru_front.set(ptab.id);
        self.front = ptab.id;

        ptab
    }
}

#[doc(hidden)]
const EMPTY_LRU: PtabLruHead = PtabLruHead {
    front: PtabId(SENTINEL),
    back: PtabId(SENTINEL),
};

/// Page table manipulation lock. All page table headers, page table age links,
/// and product chains are guarded by this lock.
pub static PTAB_LRU: SpinMutex<[PtabLruHead; PAGING_NUM_LVLS]> =
    SpinMutex::new([EMPTY_LRU; PAGING_NUM_LVLS]);

/// Unique identifier for a page table in the pool of products.
#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub struct PtabId(pub u32);

/// A stack tracker for a page table. Contains pointers to the page table
/// header, the page table frame and its associated Id. This is pretty much
/// the cost of making every static pointer 32 bits.
#[derive(Copy, Clone)]
pub struct PtabRef {
    /// Pointer to the frame of this page table.
    header: *const PageTabHeader,
    /// Pointer to the header of this page table.
    frame: *const Frame,
    /// Heap Id of this page table.
    pub id: PtabId,
}

impl PtabRef {
    /// Create a ref out of an Id. The Id must be valid or the function panics.
    pub fn new(id: PtabId) -> Self {
        assert_ne!(id, PtabId(SENTINEL));

        let bundle = unsafe { &(*OBJCACHE.get()).ptab[id.lvl()][id.bundle_id()] };
        Self {
            header: core::ptr::addr_of!(bundle.hdr[id.subbundle_id()]),
            frame: core::ptr::addr_of!(bundle.frame[id.subbundle_id()]),
            id,
        }
    }

    /// Create a new [`PtabRef`] object from an Id. None if the Id was a
    /// [`SENTINEL`]
    pub fn try_new(id: PtabId) -> Option<Self> {
        if id == PtabId(SENTINEL) {
            return None;
        }
        Some(PtabRef::new(id))
    }

    /// Pushes this page table onto the LRU cache
    pub fn lru_pushback(&self) {
        let hdr = self.get_header();
        let lvl = self.id.lvl();

        let lru = &mut PTAB_LRU.lock()[lvl];

        let front = hdr.lru_front.get();
        let back = hdr.lru_back.get();
        assert_eq!(front, PtabId(SENTINEL));
        assert_eq!(back, PtabId(SENTINEL));

        hdr.lru_front.set(lru.back);
        if lru.back == PtabId(SENTINEL) {
            lru.front = self.id;
        } else {
            PtabRef::new(lru.back).get_header().lru_back.set(self.id);
        }
        lru.back = self.id;
    }

    /// Returns the next element in the product chain
    /// of which this page table is an element
    pub fn next_product(&self) -> Option<Self> {
        PtabRef::try_new(self.get_header().product_list.get())
    }

    /// Remove this product from the product chain of its producer
    pub fn dettach_producer(&self) {
        if self.get_header().producer_id.get() != SENTINEL {
            todo!("not_done");
        }
    }

    /// Fetch the associated page table header.
    pub fn get_header(&self) -> &PageTabHeader {
        unsafe { &*self.header }
    }

    /// Fetch the frame associated with this page table.
    pub fn get_frame(&self) -> &Frame {
        unsafe { &*self.frame }
    }
}

impl PtabId {
    /// Number of bits used for indexing inside a single bundle
    const SUBBUNDLE_BITS: usize = 9;
    /// Number of bits used for determining the paging level
    const LVL_BITS: usize = 2;

    /// Synthesise a new [`Self`] from _bundle_id_ + _subbundle_id_
    fn new(lvl: u32, bundle_id: u32, subbundle_id: u32) -> Self {
        let partial_id = (bundle_id << Self::SUBBUNDLE_BITS) | subbundle_id;
        let ret = Self((partial_id << Self::LVL_BITS) | lvl);
        assert_eq!(ret.bundle_id(), bundle_id as usize);
        assert_eq!(ret.subbundle_id(), subbundle_id as usize);
        ret
    }

    /// Which page table level this ID refers to
    pub fn lvl(self) -> usize {
        self.0 as usize & ((1 << Self::LVL_BITS) - 1)
    }

    /// Which bundle contains the page table of this index
    fn bundle_id(self) -> usize {
        self.0 as usize >> (Self::SUBBUNDLE_BITS + Self::LVL_BITS)
    }

    /// Which item inside the bundle contains the page table of this index
    fn subbundle_id(self) -> usize {
        (self.0 as usize >> Self::LVL_BITS) & ((1 << Self::SUBBUNDLE_BITS) - 1)
    }

    /// Find the physical address of this page table
    pub fn paddr(self) -> Paddr {
        let cache = unsafe { &*OBJCACHE.get() };
        let base = cache.ptab[self.lvl()][self.bundle_id()].paddr;
        Paddr(base.0 + (self.subbundle_id() * PAGE_SIZE) as u64)
    }
}

/// Arch-semiagnostic definition of a page table
///
/// This isn't quite true, but it _is_ true in most useful architectures like x86_64, aarch64 and
/// riscv.
#[repr(C, align(4096))]
pub struct Frame(pub [Pte; PAGE_SIZE / core::mem::size_of::<Pte>()]);

/// Arch-independent definition of PTE restrictions.
#[derive(Copy, Clone)]
pub enum PteRestr {
    /// Reachable pages cannot be written or executed
    ReadOnly,
    /// Reachable pages can be written, but not executed
    ReadWrite,
    /// Reachable pages cannot be written, but they can be executed
    ExecOnly,
}

/// A piece of metadata to track a page table.
#[derive(Debug)]
#[repr(C)]
pub struct PageTabHeader {
    /// Front element in the LRU chain this page table belongs to
    pub lru_front: Cell<PtabId>,
    /// Back element in the LRU chain this page table belongs to
    pub lru_back: Cell<PtabId>,
    /// Link for the product chain. A singly linked list
    /// of products starting from the producer of Which
    /// this product is an element.
    pub product_list: Cell<PtabId>,
    /// Id of the producer.
    pub producer_id: Cell<u32>,
    /// Object type tag (proc vs gpt vs page).
    pub producer_type: Cell<ObjType>,
    /// Level of the page table in the hierarchy.
    lvl: u8,
    /// Product index.
    pub prod_idx: Cell<u8>,
    /// Padding.
    _pad: [u8; 5],
}

/// Number of headers per bundle.
const ITEMS_PER_BUNDLE: usize =
    PageSize::Super.octets() as usize / (PAGE_SIZE + size_of::<PageTabHeader>());

/// Arch-semiagnostic definition of a page table.
#[repr(C)]
pub struct PageTabBundle {
    /// Page table frames
    frame: [Frame; ITEMS_PER_BUNDLE],
    /// Page table frame headers
    hdr: [PageTabHeader; ITEMS_PER_BUNDLE],
    /// Physical address of the bundle. It's all physically contiguous
    paddr: Paddr,
    /// Tail padding of the bundle to make an even superpage
    _tailpad: [u64; 3],
}

#[doc(hidden)]
const _: [u8; PageSize::Super.octets() as usize] = [0; size_of::<PageTabBundle>()];

/// TODO: Make an argument here why we don't need a lock... (and hope
/// that's the case). In _principle_ I suspect we don't need it.
unsafe impl Sync for PageTabBundle {}

/// Sentinel value for linked lists. We can't use zero, because that's a valid index.
pub const SENTINEL: u32 = u32::MAX;

impl PageTabBundle {
    /// Initialise this bundle of products
    pub unsafe fn init(&mut self, lvl: u8) {
        self.paddr = bundle_get_paddr(self);

        // Initialise the freelist
        for hdr in &mut self.hdr {
            *hdr = PageTabHeader {
                lru_front: Cell::new(PtabId(SENTINEL)),
                lru_back: Cell::new(PtabId(SENTINEL)),
                product_list: Cell::new(PtabId(SENTINEL)),
                producer_id: Cell::new(u32::MAX),
                producer_type: Cell::new(ObjType::Process),
                lvl,
                prod_idx: Cell::new(u8::MAX),
                _pad: [0; 5],
            };
        }

        // Wipe the frames 1 at a time, or we
        // will blow the stack on debug builds
        for frame in self.frame.iter_mut() {
            *frame = core::mem::zeroed();
        }
    }
}

/// Initialises the page table objects in the mapping subsystem
pub unsafe fn init() {
    let cache = OBJCACHE.get();

    for lvl in 0..PAGING_NUM_LVLS {
        let start = PTAB_LVL0_START + lvl * OBJ_VRANGE;
        let bundles = core::slice::from_raw_parts_mut(start as *mut PageTabBundle, 1);
        for b in bundles.iter_mut() {
            b.init(lvl as u8);
        }

        (*cache).ptab[lvl] = bundles;

        for i in 0..ITEMS_PER_BUNDLE {
            PtabRef::new(PtabId::new(lvl as u32, 0, i as u32)).lru_pushback();
        }

        log::HeapVectorAdded {
            name: "ptab{lvl}",
            nitems: ITEMS_PER_BUNDLE,
            start,
            end: start + PageSize::Super.octets() as usize,
            max: start + OBJ_VRANGE,
        }
        .info();
    }

    assert!(PtabId(u32::MAX).subbundle_id() >= ITEMS_PER_BUNDLE - 1);
}
