//! Mapping model
//!
//! This module tries to hide the complexities of performing shadow paging between an architectural
//! tree of page tables and a tree of capabilities.

// Book-keeping structures
pub mod dep;
pub mod rmap;

// Mapping byproducts. Typically page tables.
pub mod ptab;
