//! Depend Table
//!
//! This module describes the book-keeping between non-producing GPTs and
//! the PTEs they crossed during page walks. The idea is to be able to
//! invalidate any active mappings when GPTs are destroyed, or their active
//! slots are overwritten.

use core::{cell::Cell, mem::size_of};

use crate::global::{OBJCACHE, SENTINEL};
use crate::hal::mm::{DEP_END, DEP_START};
use crate::hal::PageSize;
use crate::log;
use crate::mapping::ptab::PtabId;
use crate::ocap::gpt::GptId;

use spin::mutex::SpinMutex;

use super::ptab::PtabRef;

/// A depend table entry, representing the relationship between a GPT slot and a
/// page table PTE. The idea is that if that slot were to change, this PTE
/// would need invalidating first.
#[repr(C)]
#[derive(Copy, Clone)]
pub struct DependEntry {
    /// A GPT with a slot with an active mapping
    ///
    /// # Note
    ///   `GptId(SENTINEL)` means the entry is free
    gpt: GptId,
    /// Page table clobbered due to a traversal through the previous GPT
    ptab: PtabId,
    /// Slot index inside the GPT
    gpt_slot: u16,
    /// PTE this depend entry refers to.
    ptab_slot: u16,
}

#[doc(hidden)]
const _: [u8; 12] = [0; size_of::<DependEntry>()];

/// Global spinlock to guard the depend table
///
/// This is just one for simplicity, but it'll be several after we start
/// hashing.
static DEPTAB_SPINLOCK: SpinMutex<()> = SpinMutex::new(());

/// Number of depend table entries in each bundle
const ITEMS_PER_BUNDLE: usize = PageSize::Super.octets() as usize / size_of::<DependEntry>();

/// A _bundle_ of depend table entries. This is simply as many dep entries
/// as we can fit in a superpage
#[repr(C)]
pub struct DependBundle {
    /// Linear array of depend table entries.
    ///
    /// In time, this array will have to be split into some fixed-number of
    /// hashes so traversals are sped up. But that can wait until later.
    tab: [Cell<DependEntry>; ITEMS_PER_BUNDLE],
    /// Number of free depend table entries in this bundle
    nr_in_use: Cell<u64>,
}

/// This is guarded by [`DEPTAB_SPINLOCK`]. In time, it'll become
/// an array of spinlocks (one per strip hash in the bundle).
unsafe impl Sync for DependBundle {}

#[doc(hidden)]
const _: [u8; PageSize::Super.octets() as usize] = [0; size_of::<DependBundle>()];

/// Record that `gpt_slot` in `gpt` is the culprit of `ptab_slot` of `ptab`
/// being clobbered. This can be undone through `dep_forget_slot`, or
/// `dep_forget_gpt` to remove every relationship with that GPT.
pub fn dep_remember(gpt: GptId, ptab: PtabId, gpt_slot: u16, ptab_slot: u16) {
    let dep = unsafe { &(*OBJCACHE.get()).dep };
    let _lock = DEPTAB_SPINLOCK.lock();

    // Is this relationship already recorded?
    for bundle in dep.iter().filter(|b| b.nr_in_use.get() > 0) {
        for e in bundle.tab.iter() {
            let e = e.get();
            if e.gpt.0 == gpt.0
                && e.ptab.0 == ptab.0
                && e.gpt_slot == gpt_slot
                && e.ptab_slot == ptab_slot
            {
                // Yup
                return;
            }
        }
    }

    // Find a free entry in the table
    //
    // TODO: We probably want a lock-free freelist instead
    let Some(bundle) = dep
        .iter()
        .find(|b| (b.nr_in_use.get() as usize) < ITEMS_PER_BUNDLE)
    else {
        todo!("pt_stealing_not_done");
    };

    let entry = bundle
        .tab
        .iter()
        .find(|e| e.get().gpt.0 == SENTINEL)
        .unwrap();

    entry.set(DependEntry {
        gpt,
        ptab,
        gpt_slot,
        ptab_slot,
    });
    bundle.nr_in_use.set(bundle.nr_in_use.get() + 1);
}

/// Wipe all PTEs pointing to this page table
pub fn dep_forget_gpt(gpt: GptId) {
    let dep = unsafe { &(*OBJCACHE.get()).dep };
    let _lock = DEPTAB_SPINLOCK.lock();

    for bundle in dep.iter().filter(|b| b.nr_in_use.get() > 0) {
        for e in bundle.tab.iter() {
            {
                let e = e.get();
                if e.gpt.0 != gpt.0 {
                    continue;
                }
                let ptabref = PtabRef::new(e.ptab);
                ptabref.get_frame().0[e.ptab_slot as usize].invalidate();
            }
            e.set(DependEntry {
                gpt: GptId(SENTINEL),
                ptab: PtabId(u32::MAX),
                gpt_slot: u16::MAX,
                ptab_slot: u16::MAX,
            });
            bundle.nr_in_use.set(bundle.nr_in_use.get() - 1);
        }
    }
}

/// Forget about all PTEs created as a result of traversing a GPT slot
pub fn dep_forget_slot(gpt: GptId, gpt_slot: u16) {
    let dep = unsafe { &(*OBJCACHE.get()).dep };
    let _lock = DEPTAB_SPINLOCK.lock();

    for bundle in dep.iter().filter(|b| b.nr_in_use.get() > 0) {
        for e in bundle.tab.iter() {
            {
                let e = e.get();
                if e.gpt.0 != gpt.0 || e.gpt_slot != gpt_slot {
                    continue;
                }
                let ptabref = PtabRef::new(e.ptab);
                ptabref.get_frame().0[e.ptab_slot as usize].invalidate();
            }
            e.set(DependEntry {
                gpt: GptId(SENTINEL),
                ptab: PtabId(u32::MAX),
                gpt_slot: u16::MAX,
                ptab_slot: u16::MAX,
            });
            bundle.nr_in_use.set(bundle.nr_in_use.get() - 1);
        }
    }
}

/// Initialises the Depend Table
pub unsafe fn init() {
    let cache = OBJCACHE.get();
    let bundles = core::slice::from_raw_parts_mut(DEP_START as *mut DependBundle, 1);
    for b in bundles.iter_mut() {
        b.nr_in_use.set(0);
        for e in b.tab.iter_mut() {
            e.set(DependEntry {
                gpt: GptId(SENTINEL),
                ptab: PtabId(SENTINEL),
                gpt_slot: 0,
                ptab_slot: 0,
            });
        }
    }
    (*cache).dep = bundles;

    log::HeapVectorAdded {
        name: "dep ",
        nitems: ITEMS_PER_BUNDLE,
        start: DEP_START,
        end: DEP_START + PageSize::Super.octets() as usize,
        max: DEP_END,
    }
    .info();
}
