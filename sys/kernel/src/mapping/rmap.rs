//! Reverse Map
//!
//! This module describes an inverted page table. It tracks every single
//! PTE in the system so an page table can be dettached from the memory
//! subsystem on invalidation/reallocation.

use core::{cell::Cell, mem::size_of};

use crate::global::OBJCACHE;
use crate::hal::mm::{RMAP_END, RMAP_START};
use crate::hal::PageSize;
use crate::log;
use crate::mapping::ptab::PtabId;
use crate::ocap::page::PageId;
use crate::ocap::proc::ProcId;

use super::ptab::PtabRef;

use spin::mutex::SpinMutex;

#[doc(hidden)]
struct Pointee(u32);

#[doc(hidden)]
struct Pointer(u32);

/// Discriminant for a [`RevMapEntry`]
#[repr(u16)]
#[derive(Copy, Clone, Eq, PartialEq)]
enum RevMapEntryCase {
    /// Other cases; PTEs binding page tables
    Free,
    /// A binding between a top-level page table (e.g: PML4) and the next
    TopLevel,
    /// A binding between a page table and a page
    Leaf,
    /// Other cases; PTEs binding page tables
    Mid,
}

/// A reverse map entry, representing a single PTE relationship. Everytime
/// a PT is bound to another, one of these is allocated and recorded.
#[repr(C)]
#[derive(Copy, Clone)]
pub struct RevMapEntry {
    /// * If [`Self::case`] is [`RevMapEntryCase::Leaf`], then this is a [`PtabId`]
    /// * If [`Self::case`] is [`RevMapEntryCase::TopLevel`], then this is a `ProcId`
    /// * If [`Self::case`] is [`RevMapEntryCase::Mid`], then this is a [`PtabId`]
    pointer: u32,
    /// * If [`Self::case`] is [`RevMapEntryCase::Leaf`], then this is a `PageId`.
    /// * If [`Self::case`] is [`RevMapEntryCase::TopLevel`], then this is a [`PtabId`]
    /// * If [`Self::case`] is [`RevMapEntryCase::Mid`], then this is a [`PtabId`]
    pointee: u32,
    /// If [`Self::case`] is [`RevMapEntryCase::TopLevel`], then this zero.
    /// Else (if allocated), this is the affectd PTE.
    slot: u16,
    /// Discriminator for the revmap entry. See [`RevMapEntryCase`]
    case: RevMapEntryCase,
}

#[doc(hidden)]
const _: [u8; 12] = [0; size_of::<RevMapEntry>()];

/// Global spinlock to guard the reverse map
///
/// This is just one for simplicity, but it'll be several after we start
/// hashing.
static REVMAP_SPINLOCK: SpinMutex<()> = SpinMutex::new(());

/// Number of reverse map entries in each bundle
const ITEMS_PER_BUNDLE: usize = PageSize::Super.octets() as usize / size_of::<RevMapEntry>();

/// A _bundle_ of reverse map entries. This is simply as many rmap entries
/// as we can fit in a superpage
#[repr(C)]
pub struct RevMapBundle {
    /// Linear array of reverse map entries.
    ///
    /// In time, this array will have to be split into some fixed-number of
    /// hashes so traversals are sped up. But that can wait until later.
    tab: [Cell<RevMapEntry>; ITEMS_PER_BUNDLE],
    /// Number of free reverse map entries in this bundle
    nr_in_use: Cell<u64>,
}

/// This is guarded by [`REVMAP_SPINLOCK`]. In time, it'll become
/// an array of spinlocks (one per strip hash in the bundle).
unsafe impl Sync for RevMapBundle {}

#[doc(hidden)]
const _: [u8; PageSize::Super.octets() as usize] = [0; size_of::<RevMapBundle>()];

/// Record a PTE binding a process to its top-level page table
fn rm_remember(pointee: Pointee, pointer: Pointer, case: RevMapEntryCase, slot: u16) {
    let rmap = unsafe { &(*OBJCACHE.get()).rmap };
    let _lock = REVMAP_SPINLOCK.lock();

    // Is this relationship already recorded?
    for bundle in rmap.iter().filter(|b| b.nr_in_use.get() > 0) {
        for e in bundle.tab.iter() {
            let e = e.get();
            if e.case == case && e.pointer == pointer.0 && e.pointee == pointee.0 && e.slot == slot
            {
                // Yup
                return;
            }
        }
    }

    // Find a free entry in the table
    //
    // TODO: We probably want a lock-free freelist instead
    let Some(bundle) = rmap
        .iter()
        .find(|b| (b.nr_in_use.get() as usize) < ITEMS_PER_BUNDLE)
    else {
        todo!("pt_stealing_not_done");
    };

    let entry = bundle
        .tab
        .iter()
        .find(|e| e.get().case == RevMapEntryCase::Free)
        .unwrap();

    entry.set(RevMapEntry {
        case,
        slot,
        pointer: pointer.0,
        pointee: pointee.0,
    });
    bundle.nr_in_use.set(bundle.nr_in_use.get() + 1);
}

/// Record a PTE binding two page tables
pub fn rm_remember_pt(pointee: PtabId, pointer: PtabId, slot: u16) {
    rm_remember(
        Pointee(pointee.0),
        Pointer(pointer.0),
        RevMapEntryCase::Mid,
        slot,
    );
}

/// Record a PTE binding a page with a page table
pub fn rm_remember_leaf(pointee: PageId, pointer: PtabId, slot: u16) {
    rm_remember(
        Pointee(pointee.0),
        Pointer(pointer.0),
        RevMapEntryCase::Leaf,
        slot,
    );
}

/// Record a PTE binding between a process and its toplevel page table
pub fn rm_remember_toplevel(pointee: PtabId, pointer: ProcId) {
    rm_remember(
        Pointee(pointee.0),
        Pointer(pointer.0),
        RevMapEntryCase::TopLevel,
        0,
    );
}

/// Wipe all PTEs pointing to this page table
pub fn rm_forget_pt(pointee: PtabId) {
    let rmap = unsafe { &(*OBJCACHE.get()).rmap };
    let _lock = REVMAP_SPINLOCK.lock();

    for bundle in rmap.iter().filter(|b| b.nr_in_use.get() > 0) {
        for e in bundle.tab.iter() {
            {
                let e = e.get();
                if e.pointee != pointee.0 {
                    continue;
                }
                use RevMapEntryCase as Case;
                match e.case {
                    Case::Free | Case::Leaf => unreachable!(),
                    Case::Mid => {
                        let ptabref = PtabRef::new(PtabId(e.pointer));
                        let ptab = ptabref.get_frame();
                        ptab.0[e.slot as usize].invalidate();
                    }
                    Case::TopLevel => {
                        // wipe cr3 in the process structure
                        todo!("not done");
                    }
                }
            }
            e.set(RevMapEntry {
                pointee: 0,
                pointer: 0,
                slot: 0,
                case: RevMapEntryCase::Free,
            });
            bundle.nr_in_use.set(bundle.nr_in_use.get() - 1);
        }
    }
}

/// Wipe all PTEs pointing to this page
pub fn rm_forget_leaf(pointee: PageId) {
    let rmap = unsafe { &(*OBJCACHE.get()).rmap };
    let _lock = REVMAP_SPINLOCK.lock();

    for bundle in rmap.iter().filter(|b| b.nr_in_use.get() > 0) {
        for e in bundle.tab.iter() {
            {
                let e = e.get();
                if e.pointee != pointee.0 || e.case != RevMapEntryCase::Leaf {
                    continue;
                }
                let ptabref = PtabRef::new(PtabId(e.pointer));
                let ptab = ptabref.get_frame();
                ptab.0[e.slot as usize].invalidate();
            }
            e.set(RevMapEntry {
                pointee: 0,
                pointer: 0,
                slot: 0,
                case: RevMapEntryCase::Free,
            });
            bundle.nr_in_use.set(bundle.nr_in_use.get() - 1);
        }
    }
}

/// Initialises the reverse map
pub unsafe fn init() {
    let cache = OBJCACHE.get();
    let bundles = core::slice::from_raw_parts_mut(RMAP_START as *mut RevMapBundle, 1);
    for b in bundles.iter_mut() {
        b.nr_in_use.set(0);
        for e in b.tab.iter_mut() {
            *e = core::mem::zeroed();
        }
    }
    (*cache).rmap = bundles;

    log::HeapVectorAdded {
        name: "rmap",
        nitems: ITEMS_PER_BUNDLE,
        start: RMAP_START,
        end: RMAP_START + PageSize::Super.octets() as usize,
        max: RMAP_END,
    }
    .info();
}
