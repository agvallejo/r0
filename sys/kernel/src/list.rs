//! Linked list manipulation
//!
//! Several reasons drive the desire _not_ to use these.
//!   * They consume 8 octets per pointer
//!   * Rust doesn't play ball with linked structures very well
//!
//! They are pretty important for making process queues though

use core::cell::Cell;
use core::ptr::{addr_of, null};

/// A `link` in circular doubly linked lists
///
/// These links are used as anchors on every item contained in the list.
/// Additionally, the lists themselves have `heads` that don't b
///
/// Used for `anchors` on the list head
/// and on each item linked together.
///
/// _MUST_ be covered by a mutex.
pub struct Dlist {
    /// Pointer to the next element in the list
    next: Cell<*const Dlist>,
    /// Pointer to the previous element in the list
    prev: Cell<*const Dlist>,
}

/// Safe as long as `Dlist` is always covered by a mutex.
unsafe impl Send for Dlist {}

impl Dlist {
    /// Creates a new unbound double link
    pub unsafe fn init(&self) {
        self.next.set(null());
        self.prev.set(null());
    }

    /// True iff this link is disconnected from any another
    pub fn is_empty(&self) -> bool {
        self.next.get() == self.prev.get()
    }

    /// Unbind this element from its linked list
    ///
    /// # Safety
    /// Caller must be ensure the item is in a linked list
    pub unsafe fn unbind(&self) {
        assert!(!self.next.get().is_null());

        let prev = self.prev.get();
        let next = self.next.get();

        (*prev).next.set(next);
        (*next).prev.set(prev);

        self.prev.set(null());
        self.next.set(null());
    }
}

/// The head of circular doubly linked list
///
/// See [`Dlist`] for the definition of each individual `link`.
pub struct Dhead(Dlist);

impl Dhead {
    /// Creates a new empty list head
    pub fn init(&self) {
        if self.0.next.get().is_null() {
            self.0.next.set(addr_of!(self.0));
            self.0.prev.set(addr_of!(self.0));
        }
    }

    /// True iff there's no items in the list
    pub fn is_empty(&self) -> bool {
        self.0.next.get() == addr_of!(self.0)
    }

    /// Returns the item at the front of the linked list without unbinding it
    pub fn peekfront(&self) -> Option<&Dlist> {
        if self.is_empty() {
            return None;
        }
        Some(unsafe { &*self.0.next.get() })
    }

    /// Pushes `other` on the front of the linked list
    pub unsafe fn pushfront(&self, other: &Dlist) {
        let next = self.0.next.get();
        assert_ne!(next, null());

        other.next.set(next);
        other.prev.set(addr_of!(self.0));

        (*next).prev.set(addr_of!(*other));
        self.0.next.set(addr_of!(*other));
    }

    /// Unbinds all items in the `other` linked list
    /// and rebinds them into `self`, at the front of the list.
    pub unsafe fn mergefront(&self, other: &Self) {
        if other.is_empty() {
            return;
        }

        let src_prev = other.0.prev.get();
        let src_next = other.0.next.get();
        let dst_next = self.0.next.get();

        (*src_prev).next.set(dst_next);
        (*src_next).prev.set(addr_of!(other.0));

        (*dst_next).prev.set(src_prev);
        self.0.next.set(src_next);

        other.init();
    }

    /// Returns the item at the back of the linked list without unbinding it
    pub fn peekback(&self) -> Option<&Dlist> {
        if self.is_empty() {
            return None;
        }
        Some(unsafe { &*self.0.prev.get() })
    }

    /// Pushes `other` on the back of the linked list
    pub unsafe fn pushback(&self, other: &Dlist) {
        let prev = self.0.prev.get();
        assert_ne!(prev, null());
        assert!(other.is_empty());

        other.next.set(addr_of!(self.0));
        other.prev.set(prev);

        (*prev).next.set(addr_of!(*other));
        self.0.prev.set(addr_of!(*other));
    }

    /// Unbinds all items in the `other` linked list
    /// and rebinds them into `self`, at the back of the list.
    pub unsafe fn mergeback(&self, other: &Self) {
        if other.is_empty() {
            return;
        }

        let src_prev = other.0.prev.get();
        let src_next = other.0.next.get();
        let dst_prev = self.0.prev.get();

        (*src_next).prev.set(dst_prev);
        (*src_prev).next.set(addr_of!(self.0));

        (*dst_prev).next.set(src_next);
        self.0.prev.set(src_prev);

        other.init();
    }
}
