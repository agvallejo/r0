buildtype := "debug"
arch := "x86_64"
lldb := "n"
gfx := "n"

target := if arch == "x86_64" { "x86_64-unknown-none" } else { error("bad arch: " + arch) }
cargo_args := if buildtype == "release" { " --release" } else if buildtype == "debug" { "" } else { error("bad buildtype (release|debug): " + buildtype) }

kernel_manifest := "--manifest-path sys/kernel/Cargo.toml"
cargo_kernel_args := kernel_manifest + cargo_args
build_rustflags_kernel := "-C link-arg=-Tsrc/hal/"+arch+"/linker.ld -C relocation-model=static"

mksys_manifest := "--manifest-path sys/mksys/Cargo.toml"
cargo_mksys_args := mksys_manifest + cargo_args

usr_manifest := "--manifest-path sys/usr/Cargo.toml"
cargo_usr_args := usr_manifest + cargo_args
build_rustflags_usr := "-C link-arg=-T../libsys/src/hal/"+arch+"/linker.ld -C relocation-model=static"
usr_target_dir := "sys/usr/target/" + target + "/" + buildtype + "/"

libsys_manifest := "--manifest-path sys/libsys/Cargo.toml"
cargo_libsys_args := libsys_manifest + cargo_args

clippy_args := "-D clippy::all -D warnings -D missing_docs -D clippy::missing_docs_in_private_items"
gfx_flags := if gfx == "n" { "-vga none -nographic -monitor none" } else { "" }
qemu_lldb := if lldb == "n" { "" } else { "-s -S" }

limine_dir := "sys/limine/"
mksys_dir := "sys/mksys/"
virtdisk_dir := "sys/target/disk/"
ovmf_code := "/usr/share/OVMF/OVMF_CODE.fd"
ovmf_vars := "/usr/share/OVMF/OVMF_VARS.fd"

kernel_bin := "sys/kernel/target/" + target + "/" + buildtype + "/r0"

# Checkout the limine binary repository
limine:
    #!/usr/bin/env sh
    if [ ! -d "{{limine_dir}}" ]; then
        git clone https://github.com/limine-bootloader/limine.git --branch=v7.x-binary --depth=1 {{limine_dir}};
    fi

# Build everything
build: limine
    cargo rustc {{cargo_kernel_args}} --target {{target}} -- {{build_rustflags_kernel}}
    RUSTFLAGS="{{build_rustflags_usr}}" cargo build {{cargo_usr_args}} --target {{target}}
    mkdir -p {{virtdisk_dir}}
    cargo run {{cargo_mksys_args}} --bin rxtx -- "{{virtdisk_dir}}" "{{usr_target_dir}}"

# Test everything
test:
    cargo test {{cargo_kernel_args}} --features std
    MIRIFLAGS="-Zmiri-permissive-provenance -Zmiri-ignore-leaks" cargo miri test {{cargo_kernel_args}} --features std

# Retrieve test coverage
cov:
    cargo tarpaulin {{cargo_kernel_args}} --features std

# Runs the kani model-checker
kani:
    cargo kani {{cargo_kernel_args}} --features std

# Build the documentation website
doc mkdocs_args='':
    RUSTDOCFLAGS="-D warnings" cargo doc --target-dir docs/ref-manuals/libsys {{cargo_libsys_args}}
    RUSTDOCFLAGS="-D warnings" cargo doc --target-dir docs/ref-manuals/kernel {{cargo_kernel_args}}
    RUSTDOCFLAGS="-D warnings" cargo doc --target-dir docs/ref-manuals/mksys {{cargo_mksys_args}} --bins
    mkdocs build {{mkdocs_args}}

# Wipe the target directory
clean:
    cargo clean {{cargo_libsys_args}}
    cargo clean {{cargo_kernel_args}}
    cargo clean {{cargo_mksys_args}}
    cargo clean {{cargo_usr_args}}
    rm -rf docs/ref-manuals/*

# Wipe _all_ artifacts 
distclean: clean
    rm -rf {{limine_dir}}
    rm -rf {{virtdisk_dir}}

# Reformat all the Rust sources
fmt:
    cargo fmt {{libsys_manifest}} --all
    cargo fmt {{kernel_manifest}} --all
    cargo fmt {{usr_manifest}} --all
    cargo fmt {{mksys_manifest}} --all

# Ensure all Rust sources are properly formatted
checkfmt:
    cargo fmt {{libsys_manifest}} --all --check
    cargo fmt {{kernel_manifest}} --all --check
    cargo fmt {{usr_manifest}} --all --check
    cargo fmt {{mksys_manifest}} --all --check

# Run clippy on the Rust code
clippy:
    cargo clippy {{cargo_libsys_args}} --all-targets -- {{clippy_args}}
    cargo clippy {{cargo_kernel_args}} --target {{target}} -- {{clippy_args}}
    cargo clippy {{cargo_usr_args}} --target {{target}} -- {{clippy_args}}
    cargo clippy {{cargo_mksys_args}} -- {{clippy_args}}

# Checks the paper trail. Licenses and security advisories
papers:
    cargo deny {{cargo_libsys_args}} check
    cargo deny {{cargo_kernel_args}} --target {{target}} check
    cargo deny {{cargo_usr_args}} --target {{target}} check
    cargo deny {{cargo_mksys_args}} check

# Run build on qemu
qemu: build
    mkdir -p {{virtdisk_dir}}EFI/BOOT
    cp {{kernel_bin}} {{virtdisk_dir}}
    cp {{limine_dir}}BOOTX64.EFI {{virtdisk_dir}}EFI/BOOT
    cp extra/limine.cfg {{virtdisk_dir}}
    qemu-system-x86_64 \
      -m 4G \
      -smp 4 \
      {{gfx_flags}} \
      -drive if=pflash,format=raw,unit=0,readonly=on,file={{ovmf_code}} \
      -drive if=pflash,format=raw,unit=1,snapshot=on,file={{ovmf_vars}} \
      -drive file=fat:rw:{{virtdisk_dir}},format=raw \
      -net none \
      -serial stdio \
      {{qemu_lldb}} \
      -device isa-debug-exit || \
      RC=$?; if [ "$RC" != 3 ]; then exit $RC; else exit 0; fi


# Run LLDB (assumes QEMU is blocked waiting)
lldb:
    rust-lldb -o "target create {{kernel_bin}}" \
         -o "gdb-remote localhost:1234" \
         -o "command script import extra/lldb_r0.py" \
         -o "command script add -f lldb_r0.kcache_dump_all kcache_dump_all" \
         -o "command script add -f lldb_r0.kcache_dump_active kcache_dump_active" \
         -o "breakpoint set --name pagefault_handler" \
         -o "c"
