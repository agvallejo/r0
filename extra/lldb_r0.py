#!/usr/bin/env python3

import lldb

def objvec(target, objtype: str):
    return target.FindFirstGlobalVariable("OBJCACHE")\
                 .GetChildMemberWithName("value")\
                 .GetChildMemberWithName("value")\
                 .GetChildMemberWithName(objtype)

def kcache_dump_all(debugger, command, result, internal_dict):
    target = debugger.GetSelectedTarget()

    print("proc_tot_count=", objvec(target, "proc").GetNumChildren())
    print("gpt_tot_count=", objvec(target, "gpt").GetNumChildren())
    print("pages_tot_count=", objvec(target, "page").GetNumChildren())

def kcache_dump_active(debugger, command, result, internal_dict):
    target = debugger.GetSelectedTarget()

    for i in range(objvec(target, "proc").GetNumChildren()):
        proc = objvec(target, "proc").GetChildAtIndex(i)
        oid = proc.EvaluateExpression("hdr.oid.value.value").GetValue()
        if oid == 0:
            break
        print(f"ProcId({i})")
        print("  Oid=",   oid)
        print("  OteId=", proc.EvaluateExpression("hdr.ote.value.value").GetChildAtIndex(0).GetValue())
